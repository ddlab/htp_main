function masterTable = conn( o )
    ls = @(x,y) linspace(x,y);

 

EEG = o.EEG;

    % specify some time-frequency parameters
    freqs2use  = logspace(log10(1),log10(80),20); % 4-30 Hz in 15 steps
  %  times2save = -2000:10:0;
  %  timewindow = linspace(1.5,3,length(freqs2use)); % number of cycles on either end of the center point (1.5 means a total of 3 cycles))
   % baselinetm = [-400 -200];
    
    % wavelet and FFT parameters
    time          = -1:1/EEG.srate:1;
    half_wavelet  = (length(time)-1)/2;
    num_cycles    = logspace(log10(4),log10(8),length(freqs2use));
    n_wavelet     = length(time);
    n_data        = EEG.pnts*EEG.trials;
    n_convolution = n_wavelet+n_data-1;
    
    dbwpli_mat = NaN(EEG.nbchan,EEG.nbchan, 6);    
    
    % data FFTs
    
   % fft2(reshape(EEG.data(:,:,:),1, n_data), 128, n_convolution)
    
    gpuon = 1;
     if gpuon == 0
              data_fft = zeros(EEG.nbchan, n_convolution);

     else
         
        %EEG.data = gpuArray(single(EEG.data));
        EEG.data = gpuArray(double(EEG.data));
        data_fft = zeros(EEG.nbchan, n_convolution, 'gpuArray');


     end
     

     for i = 1 : EEG.nbchan
         
     data_fft(i, :) = fft(reshape(EEG.data(i,:,:),1, n_data),n_convolution);

     end
     
combos = combnk({EEG.chanlocs(:).labels}',2);

%D = gpuDevice;

tic
for chancombo = 1 : 100 %length(combos)
    fprintf('%d\n', chancombo);
    % select channel based on label
    channel1 = combos{chancombo,1};
    channel2 = combos{chancombo,2};
    
  %  if mod(chancombo, 10) == 0
  %      D.AvailableMemory
  %  end

    
    % time in indices
    %times2saveidx = dsearchn(EEG.times',times2save');
    times2saveidx = 1 : length(EEG.times);
   % baselineidxF  = dsearchn(EEG.times',baselinetm');  % for the full temporal resolution data (thanks to Daniel Roberts for finding/reporting this bug here!)
   % baselineidx   = dsearchn(times2save',baselinetm'); % for the temporally downsampled data
    
    chanidx = zeros(1,2); % always initialize!
    chanidx(1) = find(strcmpi(channel1,{EEG.chanlocs.labels}));
    chanidx(2) = find(strcmpi(channel2,{EEG.chanlocs.labels}));
%     
%     data_fft1 = fft(reshape(EEG.data(chanidx(1),:,:),1,n_data),n_convolution);
%     data_fft2 = fft(reshape(EEG.data(chanidx(2),:,:),1,n_data),n_convolution);
%     

data_fft1 = data_fft(chanidx(1), :);
data_fft_all = data_fft(1:end, :);

data_fft2 = data_fft(chanidx(2), :);

    if gpuon == 0
    % initialize
    ispc    = zeros(length(freqs2use),EEG.pnts);
    pli     = zeros(length(freqs2use),EEG.pnts);
    wpli    = zeros(length(freqs2use),EEG.pnts);
    dwpli   = zeros(length(freqs2use),EEG.pnts);
    %dwpli_t = zeros(length(freqs2use),length(times2save));
    %ispc_t  = zeros(length(freqs2use),length(times2save));
    else
    % gpuArray
    ispc    = zeros(length(freqs2use),EEG.pnts, 'gpuArray');
    pli     = zeros(length(freqs2use),EEG.pnts, 'gpuArray');
    wpli    = zeros(length(freqs2use),EEG.pnts, 'gpuArray');
    dwpli   = zeros(length(freqs2use),EEG.pnts, 'gpuArray');
   % dwpli_t = zeros(length(freqs2use),length(times2save), 'gpuArray');
   % ispc_t  = zeros(length(freqs2use),length(times2save), 'gpuArray');
      %  wavelet_fft = zeros(1, n_convolution, 'gpuArray');

    %convolution_result_fft = zeros(1, n_data, 'gpuArray');
    
    end
    
    for fi=1:length(freqs2use)
        
        % create wavelet and take FFT
        s = num_cycles(fi)/(2*pi*freqs2use(fi));
        
                s_all = num_cycles ./(2*pi*freqs2use);

        
        wavelet_fft = fft( exp(2*1i*pi*freqs2use(fi).*time) .* exp(-time.^2./(2*(s^2))) ,n_convolution);
        wavelet_fft_all = fft( exp(2*1i*pi*freqs2use' .* repmat(time, length(freqs2use),1)) .* repmat(exp(-time.^2./(2*(s^2))),  length(freqs2use),1) ,n_convolution)';

        
        % phase angles from channel 1 via convolution
        convolution_result_fft = ifft(wavelet_fft.*data_fft1,n_convolution);
        convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
        
        convolution_result_fft_all = ifft(wavelet_fft_all.* data_fft_all,n_convolution);
        convolution_result_fft_all = convolution_result_fft(half_wavelet+1:end-half_wavelet);
        
        
        
        
        sig1 = reshape(convolution_result_fft,EEG.pnts,EEG.trials);
        
        % phase angles from channel 2 via convolution
        convolution_result_fft = ifft(wavelet_fft.*data_fft2,n_convolution);
        convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
        sig2 = reshape(convolution_result_fft,EEG.pnts,EEG.trials);
        
        % cross-spectral density
        cdd = sig1 .* conj(sig2);
        
        % ISPC
%         ispc(fi,:) = abs(mean(exp(1i*angle(cdd)),2)); % note: equivalent to ispc(fi,:) = abs(mean(exp(1i*(angle(sig1)-angle(sig2))),2));
              ispc(fi,:) = abs(sum(exp(1i*angle(cdd)),2) / length(angle(cdd))); % note: equivalent to ispc(fi,:) = abs(mean(exp(1i*(angle(sig1)-angle(sig2))),2));
  
        
        % take imaginary part of signal only
        cdi = imag(cdd);
        
        % phase-lag index
%         pli(fi,:)  = abs(mean(sign(imag(cdd)),2));
                pli(fi,:)  = abs(sum(sign(imag(cdd)),2) / length(cdd));

        % weighted phase-lag index (eq. 8 in Vink et al. NeuroImage 2011)
       % wpli(fi,:) = abs( mean( abs(cdi).*sign(cdi) ,2) )./mean(abs(cdi),2);
                wpli(fi,:) = abs( sum( abs(cdi).*sign(cdi) ,2) / length(cdi) )./ (sum(abs(cdi),2) / length(cdi));

        % debiased weighted phase-lag index (shortcut, as implemented in fieldtrip)
        imagsum      = sum(cdi,2);
        imagsumW     = sum(abs(cdi),2);
        debiasfactor = sum(cdi.^2,2);
        dwpli(fi,:)  = (imagsum.^2 - debiasfactor)./(imagsumW.^2 - debiasfactor);
        
        
    end

    connTable = table(freqs2use', mean(pli,2), mean(wpli,2), mean(dwpli,2), 'VariableNames', {'Freq','pli', 'wpli', 'dwPLI'});
    
    
    bandLabel = { 'delta', 'theta', 'alpha1', 'alpha2', 'beta', 'gamma' };
    bandRange = { ls(1,3), ls(4,7), ls(8,10), ls(10,13), ls(13,30), ls(30,80) };
    

    % create results array by band
    for i = 1 : length(bandLabel)
        
        band(i).label = bandLabel(i);
        band(i).range = bandRange{i};
        band(i).freq2use = unique(dsearchn(freqs2use', band(i).range'));
        band(i).tablecol = mean(table2array(connTable(band(i).freq2use',2:end)));
        
    end
    
     % band = gather(band);
    %  connTable = gather(connTable);
      
      % pretty display of results
      t1 = table([band(:).label]', 'VariableNames', {'Band'});
      %t2 = array2table(...
      %    round(...
      %    reshape( [band.tablecol], length(band), size(connTable(:, 2:end),2)) ...
      %    ,3));
      t2 = array2table(...
          reshape( [band.tablecol], length(band), size(connTable(:, 2:end),2)) ...
          );
    
    t2.Properties.VariableNames = {'pli', 'wpli', 'dwPLI'};
    %disp([channel1 ' ' channel2]);

    resTable = [t1 t2];
    
masterTable(chancombo).chan1 =  combos{chancombo,1};
masterTable(chancombo).chan2 =  combos{chancombo,2};
masterTable(chancombo).chan1idx =  chanidx(1);
masterTable(chancombo).chan2idx =  chanidx(2);


masterTable(chancombo).resTable =  [t1 t2];

test = [band(:).tablecol];
test = reshape(test, 6,3);
test = gather(test(:,3));

% dbwpli_mat(chanidx(1), chanidx(2), :) = 
    
end

toc
end
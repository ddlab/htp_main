EEG2 = EEG;


buttonArr = {'DIN1', 'DIN2', 'DIN3', 'DIN4' };

for j = 1:length(buttonArr)

    clear trigIdx;
    
    trigIdx = find(strcmp({EEG2.event.type}, buttonArr(j)));
    
    for i = 1 : length(trigIdx)
        
        EEG2.event(trigIdx(i)).type = 'BUTTON';
        
    end
    
end

trialArr = {'DIN6', 'DIN7'};

for j = 1:length(trialArr)

    clear trigIdx;
    
    trigIdx = find(strcmp({EEG2.event.type}, trialArr(j)));
    
    for i = 1 : length(trigIdx)
        
        EEG2.event(trigIdx(i)).type = 'START';
        
    end
    
end


    trigIdx = find(strcmp({EEG2.event.type}, 'DIN9'));
    
    for i = 1 : length(trigIdx)
        
        EEG2.event(trigIdx(i)).type = 'FEEDBACK9';
        
    end


    trigIdx = find(strcmp({EEG2.event.type}, 'DI11'));
    
    for i = 1 : length(trigIdx)
        
        EEG2.event(trigIdx(i)).type = 'FEEDBACK11';
        
    end

    trigIdx = find(strcmp({EEG2.event.type}, 'DIN8'));
    
    for i = 1 : length(trigIdx)
        
        EEG2.event(trigIdx(i)).type = 'BLANK';
        
    end

    
    
    trigIdx = find(strcmp({EEG2.event.type}, 'DI10'));
    
    for i = 1 : length(trigIdx)
        
        EEG2.event(trigIdx(i)).type = 'DISCARD';
        
    end
    
 find(strcmp({EEG2.event.type}, 'BUTTON'))
 find(strcmp({EEG2.event.type}, 'START'))
 find(strcmp({EEG2.event.type}, 'FEEDBACK9'))
 find(strcmp({EEG2.event.type}, 'FEEDBACK11'))
 
 EEGBACKUP = EEG;
 
 EEG = EEG2;
 
 eeglab redraw;
# High Throughput Pipeline for EEG Analysis (HTP) 

Object oriented wrapper for EEG data from import to group analysis. 
Underlying structure allows customization to use various toolkits.
Current source relies primarily on EEGLAB, FieldTrip, and custom analysis routines.
Routines for importing EGI 32 or 128 channel RAW data is implemented.

## Getting Started

Stage template scripts are located under experiments/htp 

+ PREPROCESSING
	+htp_stage1 - raw import, channel assignment, filter, resample
 	+htp_stage2 - epoching; manual channel, artifact, and epoch cleaning
 	+htp_stage3 - ICA (parallel binary)
 	+htp_stage4 - non-neural component identification

+ ANALYSIS
	+htp_stage5 - individual analysis (level1)
	+htp_stage6 - group level analysis and statistics

### Object Oriented Framework

+ eegDataClass
+ restEegDataClass

For combining batches, example scripts are available in the "combiner" directory

BINICA - Packaged with Windows Binary of Extended Infomax ICA Routines
(https://sccn.ucsd.edu/wiki/Binica)

Please install Intel math Kernel Library and adjust system paths as below:
(https://software.intel.com/en-us/mkl)

+ If you get a "no weights found" message, please update your system paths for the Intel MKL library as follows:
\[location of program files]\IntelSWTools\compilers_and_libraries_2018.5.274\windows\mkl
\[location of program files]\IntelSWTools\compilers_and_libraries_2018.5.274\windows\redist\intel64_win\mkl

### Prerequisites

Multiplatform (tested on Windows 10 and Mac OS X Sierra)
MATLAB (tested on 2017a and later)
  Signal Processing Toolkit (required)
  Parallel Computing Toolkit (optional)
EEGLAB 14.2b (may work on previous versions)
FieldTrip Toolbox or FieldTrip Light Plugin for EEGLAB
ICView Plugin
ViewProps Plugin
DIPFIT2 (usually installed by default)

### Installing

1. Download source code
2. Create a data directory with the following structure:

+ ROOT
  + Data Directory Name
     + S00_RAW
	    + SUBFOLDER (Arbitrary)
			+ RAW data files go here
		+ SUBFOLDER
			+ RAW data files go here

3. Edit htp_config.m to configure hostname, eeglab directory, and  data directory.
4. Switch MATLAB working directory to main source folder.
5. Run setPipelinePath.m to configure local paths
6. Run Stage scripts consecutively    

## Deployment


## Built With

* [MATLAB](http://www.mathworks.com)
* [EEGLAB](https://sccn.ucsd.edu/eeglab/index.php) 
* [FieldTrip Toolbox] (http://www.fieldtriptoolbox.org/) 

## Contributing

Please read WIKI https://bitbucket.org/ddlab/htp_main/wiki/Home

## Authors

* **Ernest Pedapati** - *Initial work*

See also the list of [contributors](https://bitbucket.org/ddlab/htp_main/wiki/Home who participated in this project.

## Acknowledgments
* 
* Ellen Russo (University of Cincinnati)



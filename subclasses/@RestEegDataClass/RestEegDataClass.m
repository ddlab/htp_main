classdef RestEegDataClass < eegDataClass & restingAnalysisModule
    % MeaDataClass - subclass
    properties
        
        % inherited properties from eegDataClass
        
        
    end
    
    methods
        % object constructor
        function o = RestEegDataClass(  )
            
            o@eegDataClass( );  % superclass constructor
            %obj@erpModule( type );
            
            %o.net_filter = '*.raw';
            
            
            % initiate specific experimental protocols
            o.classInit;
            
        end
        
        function o = setElectrodeSystem( o, chaninfo )
           
            % chaninfo is passed from stage 1 and defined in htp_config
            o.net_name      = chaninfo.net_name;
            o.net_file      = chaninfo.net_file;
            o.net_filter    = chaninfo.net_filter;
            o.net_regions   = chaninfo.net_regions;
            
        end
        
        function o = classInit( o )
            
            % Study Information
            o.study_id        = 1;
            o.study_title     = 'Rest';
            o.study_desc      = 'Rest';
            % Processing Parameters
            o.proc_sRate1             = 500;
            o.proc_clearEpochs        = 1;            % remove epochs following raw import
            o.proc_deleteMissingChans = 1;     % remove raw channels without locations
            o.proc_contEpochLength    = 2;     % duration in seconds
            o.proc_contEpochLimits    = [-1 1];
            
            % ERP variables
            % study specific properties here
            o.includeInStudyList = true;
            o.includeInStudyType = 'eeg';
            
        end
 
        
        function o = changeStudyTitle( o, string )
            
            o.study_title     = string;
            
        end
        
        function o = getMeaData( o )
           try
                    
               datafile =  o.filename.raw;
               subfolder = o.subj_subfolder;
               folder = o.pathdb.raw;
               
               edfFile = fullfile(folder, subfolder, datafile);
               
               EEG = pop_biosig( edfFile );
               load('chanfiles/meachanlocs.mat', 'chanlocs');
               EEG.chanlocs = chanlocs;
               clear chanlocs;
               
                    EEG             = eeg_checkset( EEG );
                    EEG = pop_select( EEG,'channel',{'17' '16' '15' '14' '19' '18' '13' '12' '21' '20' '11' ...
                        '10' '24' '23' '22' '9' '8' '7' '27' '26' '25' '6' '5' '4' '30' '29' '28' '3' '2' '1'});
                    swCHANNEL = 0;
                    swRESAMPLE  = 0;
                    EEG.filename = datafile;
                    EEG.chaninfo.filename = 'meachanlocs.mat';
                    
                    o.EEG = EEG;
           catch
               
           end
            
        end
        
        function o = getRawData ( o )
            
            % Revised 1/5/2019 for cross species import
            
            % correct recording system is defined in setElectrodeSystem
            net_name = o.net_name;

            switch net_name
                
                case 'EGI32'
                    
                    o.getRawEGI;
            
                case 'EGI128'
                    
                    o.getRawEGI;
                    
                case 'MEA30'
                    
                    o.getRawMEA;
                    
            end
                  
        end
        
        function o = getRawEGI( o )
           
            try
                
                o.EEG = pop_readegi(fullfile(o.pathdb.raw, o.subj_subfolder, o.filename.raw));
                o.EEG = eeg_checkset( o.EEG );
                
            catch
                cprintf('red', '\n\nERROR: Check if data files are located in "Main Folder"\\S00_RAW\\"Subfolder"\n\n');
                return;
                
            end
            
            % load 3d locations from template
            o.EEG = readegilocs2(o.EEG,o.net_file);
            
            o.net_nbchan_orig = o.EEG.nbchan;
            o.proc_sRate_raw = o.EEG.srate;
            o.proc_xmax_raw = o.EEG.xmax;
            
        end
        
        function o = getRawMEA( o )
        
            try
                
                datafile =  o.filename.raw;
                subfolder = o.subj_subfolder;
                folder = o.pathdb.raw;
                
                edfFile = fullfile(folder, subfolder, datafile);
                
                EEG = pop_biosig( edfFile );
                EEG = pop_select( EEG, 'nochannel', [2,32,33]);
                
                load(o.net_file, 'chanlocs');
                chanlocs(31) = [];
                EEG.chanlocs = chanlocs;
                EEG = eeg_checkset( EEG );
                
                % clear chanlocs;
                
                %EEG = pop_select( EEG,'channel',{'17' '16' '15' '14' '19' '18' '13' '12' '21' '20' '11' ...
                %    '10' '24' '23' '22' '9' '8' '7' '27' '26' '25' '6' '5' '4' '30' '29' '28' '3' '2' '1'});
                
                % based on the revised NN remap provided by Carrie Jonak
                % (Channel remap.jpg)
                
                %EEG = pop_select( EEG,'channel',{'1','3','4','5','6','7','8','9','10', ...
%                     '11','12','13','14','15','16','17','18','19','20','21','22','23','24', ...
%                     '25','26','27','28','29','30','31'});
%                     
%                 
%                 '17' '16' '15' '14' '19' '18' '13' '12' '21' '20' '11' ...
%                     '10' '24' '23' '22' '9' '8' '7' '27' '26' '25' '6' '5' '4' '30' '29' '28' '3' '2' '1'});
%                 
%                 
                swCHANNEL = 0;
                swRESAMPLE  = 0;
                EEG.filename = datafile;
                EEG.chaninfo.filename = 'meachanlocs.mat';
                
                o.EEG = EEG;
            catch
                
            end
            

        end
            
%             % import raw data into eeglab format
%             % main import function for human data
%             
%             
% 
%             
%             %chanLocsTemplate = loadbvef(o.net_file);
%             %o.EEG = assignChanLocs( o.EEG, chanLocsTemplate, ...
%             %    o.proc_deleteMissingChans );
%             
%             % create backup of original data
%             obj.EEG_raw = obj.EEG;
%             
%             % -------- LOG -----------
%             % original channel number prior to cleaning
%             obj.net_nbchan_orig = obj.EEG.nbchan;
%             obj.proc_sRate_raw = obj.EEG.srate;
%             obj.proc_xmax_raw = obj.EEG.xmax;
%             
%             if strcmp(obj.study_type,'chirp')
%                 
%                 if  obj.EEG.pnts == obj.events_pnts
%                     
%                     disp('EEG ERROR: Event DIG file is not same length as MEA Datafile');
%                     disp(['Data file: ' obj.filename.raw]);
%                     disp(['Event file: ' obj.filename.eventdata]);
%                     
%                     obj.EEG.event = obj.events_event;
%                     obj.EEG.uurevent = obj.events_urevent;
%                     
%                 end
%                 
%                 
%             end
            
%         end
        
        %         function obj = getChanEvents( obj )
        %
        %             % identify raw basename
        %             [sub,basefile,~] = fileparts(obj.filename.raw);
        %
        %             % add 'dig.edf'
        %             eventfile = fullfile(sub, [basefile ' dig.edf']);
        %
        %             % load data
        %             EEG = pop_biosig(eventfile, 'channels',3,'importevent','off','importannot','off');
        %
        %             % extract events
        %             EEG = pop_chanevent(EEG, 1,'edge','leading','edgelen',5, 'delchan', 'off');
        %
        %             % store data size
        %             obj.events_pnts = EEG.pnts;
        %
        %             % store events in temp structure
        %             obj.events_event = EEG.event;
        %             obj.events_urevent = EEG.urevent;
        %             % print basics
        %             uniqueEvents = unique({EEG.event(:).type});
        %             disp(['Unique Events:' uniqueEvents]);
        %             % return
        %
        %         end
        
        function obj = filtData2( obj )
            
            % High Pass
            filtConfig = filt_InitParam();
            filtConfig.locutoff = 0.5;
            obj.proc_filt_lowcutoff = filtConfig.locutoff ;
            
            filtConfig.filtorder = 6600;
            [obj.EEG, com, b] = filt_eegfiltnew(obj.EEG, filtConfig);
            
        end
        
        function o = filt_highpass( o, hz )
            
            try
            EEG = eeg_checkset( o.EEG );
            
            filt_order = round(3 * ( EEG.srate / hz ));
            
            % changed per matlab warning message
            filt_order = 3300;
            % back up original data
            o.EEG_prefilt = EEG;
            
            % High Pass
            filtConfig = filt_InitParam();
            filtConfig.locutoff = hz;
            
            filtConfig.filtorder = filt_order;
            [EEG, com, b] = filt_eegfiltnew(EEG, filtConfig);
            
            o.EEG = EEG;
            
            o.proc_filt_lowcutoff = hz;
            
            catch
                
            end
            
        end
        
        function o = filt_lowpass( o, hz )
            
            try
                EEG = eeg_checkset( o.EEG );
                
                %filt_order = round(8 * ( EEG.srate / hz ));
                filt_order = 1650;
                % back up original data
                o.EEG_prefilt = EEG;
                
                % High Pass
                filtConfig = filt_InitParam();
                filtConfig.hicutoff = hz;
                
                filtConfig.filtorder = filt_order;
                [EEG, com, b] = filt_eegfiltnew(EEG, filtConfig);
                
                o.EEG = EEG;
                
                o.proc_filt_highcutoff = hz;
                
            catch
                
            end
            
        end
        
        
        function o = filt_notch( o, hz1, hz2, filt_order )
            
            try
                EEG = eeg_checkset( o.EEG );
                
                %filt_order = 3300;
                
                % Notch
                filtConfig = filt_InitParam();
                              
                [EEG, com, b] = pop_eegfiltnew(EEG, hz1, hz2, filt_order, 1, [], 0);
                
                EEG  = eeg_checkset( EEG );
                
                o.EEG = EEG;
                
                o.proc_filt_bandlow = hz1;
                o.proc_filt_bandhigh = hz2;

               
                
            catch
                
            end
            
        end
        
        function o = filt_for_ica( o, filtbound )
            
            plots = 1;
            % enter filter bounds as a matrix, i.e. [2 80] or 2 hz to 80 hz
            o.proc_filt_bandlow = filtbound(1);              % filter settings
            o.proc_filt_bandhigh = filtbound(2);              % filter settings
            
            EEG = o.EEG;
            
            nyquist = EEG.srate / 2; % specify Nyquist Frequency
            
            % transition width
            trans_width = 0.15; % fraction of 1, thus 20%.
            
            % filter order
            filt_order = round(3 * ( EEG.srate / filtbound(1) ));
            
            % frequency vector (as fraction of Nyquist
            ffrequencies  = [ 0 (1-trans_width)*filtbound(1) filtbound (1+trans_width)*filtbound(2) nyquist ]/nyquist;
            
            % shape of filter (must be the same number of elements as frequency vector
            idealresponse = [ 0 0 1 1 0 0 ];
            
            % get filter weights
            filterweights = firls(filt_order,ffrequencies,idealresponse);
            
            if plots == 1
                % plot for visual inspection
                figure(1), clf
                subplot(211)
                plot(ffrequencies*nyquist,idealresponse,'k--o','markerface','m')
                set(gca,'ylim',[-.1 1.1],'xlim',[-2 nyquist+2])
                xlabel('Frequencies (Hz)'), ylabel('Response amplitude')
                
                subplot(212)
                plot((0:filt_order)*(1000/EEG.srate),filterweights)
                xlabel('Time (ms)'), ylabel('Amplitude')
            end
            
            
            % apply filter to data
            filtered_data = zeros(EEG.nbchan,EEG.pnts);
            for chani=1:EEG.nbchan
                filtered_data(chani,:) = filtfilt(filterweights,1,double(EEG.data(chani,:,1)));
            end
            
            if plots == 1
                
                figure(2), clf
                plot(EEG.times,squeeze(EEG.data(47,:,1)))
                hold on
                plot(EEG.times,squeeze(filtered_data(chani,:)),'r','linew',2)
                xlabel('Time (ms)'), ylabel('Voltage (\muV)')
                legend({'raw data';'filtered'})
                
            end
            
            o.EEG = EEG;
            
            
        end
        
        function obj = filtData( obj )
            
            obj.EEG  = eeg_checkset( obj.EEG );
            
            % back up original data
            obj.EEG_prefilt = obj.EEG;
            
            % High Pass
            filtConfig = filt_InitParam();
            filtConfig.locutoff = 0.5;
            obj.proc_filt_lowcutoff = filtConfig.locutoff ;
            
            filtConfig.filtorder = 6600;
            [obj.EEG, com, b] = filt_eegfiltnew(obj.EEG, filtConfig);
            
            % Low Pass
            filtConfig = filt_InitParam();
            filtConfig.hicutoff = 120;
            obj.proc_filt_highcutoff = filtConfig.hicutoff ;
            
            filtConfig.filtorder = [];
            [obj.EEG, com, b] = filt_eegfiltnew(obj.EEG, filtConfig);
            
            % Notch
            filtConfig = filt_InitParam();
            
            filtbandlow = 58;
            filtbandhigh = 62;
            
            [obj.EEG, com, b] = pop_eegfiltnew(obj.EEG, filtbandlow, filtbandhigh, 3300, 1, [], 0);
            
            obj.EEG  = eeg_checkset( obj.EEG );
            
            
            obj.proc_filt_bandlow = filtbandlow;              % filter settings
            obj.proc_filt_bandhigh = filtbandhigh;              % filter settings
            
            
        end
        
        function obj = resampleData( obj )
            
            srate = obj.proc_sRate1;
            
            obj.EEG             = pop_resample( obj.EEG, srate);
            obj.EEG             = eeg_checkset( obj.EEG );
            
        end
        
        function obj = setResampleRate( obj, srate )
           
            obj.proc_sRate1 = srate;
            
        end
        
        function obj = interpolateData( obj )
            
            obj.EEG = pop_interp(obj.EEG, obj.EEG_prechan.chanlocs, 'spherical');
            
        end
        
        function obj = cleanData( obj )
            
            % backup
            obj.EEG_prechan = obj.EEG;
            %    EEG = clean_rawdata(EEG, 5, -1, 0.85, 4, 20, 0.25);
            param = cleanRawDataInit();
            param.arg_flatline  = 5;
            param.arg_highpass  = -1;
            param.arg_channel   = 0.85;
            param.arg_noisy     = 4;
            param.arg_burst     = 20;
            param.arg_window    = 0.25;
            
            obj = cleanRawData( obj, param );
            
            % revise channel number for log
            obj.net_nbchan_post = obj.EEG.nbchan;
            
            % calculate the independent rank
            if isfield(obj.EEG.etc, 'clean_channel_mask')
                obj.proc_dataRank = min([rank(double(obj.EEG.data')) sum(obj.EEG.etc.clean_channel_mask)]);
            else
                obj.proc_dataRank = rank(double(obj.EEG.data'));
            end
            
            
        end
        
        function obj = manualChannelClean( obj )
            
            
        end
        
        
        function obj = averageRefData( obj )
            
            % Step 9: Re-reference the data to average
            obj.EEG.nbchan = obj.EEG.nbchan+1;
            obj.EEG.data(end+1,:) = zeros(1, obj.EEG.pnts);
            obj.EEG.chanlocs(1,obj.EEG.nbchan).labels = 'initialReference';
            obj.EEG = pop_reref(obj.EEG, []);
            obj.EEG = pop_select( obj.EEG,'nochannel',{'initialReference'});
            
            % calculate the independent rank
            if isfield(obj.EEG.etc, 'clean_channel_mask')
                obj.proc_dataRank = min([rank(double(obj.EEG.data')) sum(obj.EEG.etc.clean_channel_mask)]);
            else
                obj.proc_dataRank = rank(double(obj.EEG.data'));
            end
            
            obj.EEG  = eeg_checkset( obj.EEG );
        end
        
        function obj = epochData( obj )
            % old function
            switch obj.study_type
                case 'rest'
                    
                    arg_recurrence = obj.proc_contEpochLength;
                    arg_limits = obj.proc_contEpochLimits;
                    obj.EEG = eeg_regepochs(obj.EEG,'recurrence',arg_recurrence,'limits',arg_limits,'rmbase',NaN);
                    obj.EEG  = eeg_checkset( obj.EEG );
                    
                case 'chirp'
                    s={obj.EEG.event.type};
                    eventTypes=unique(s,'sorted');
                    obj.EEG = pop_epoch( obj.EEG, eventTypes,  obj.proc_contEpochLimits );
                    %EEG = pop_epoch( EEG, All_STIM, vEPOCHERP, 'newname', 'Epochs', 'epochinfo', 'yes');
                    obj.EEG                         = eeg_checkset( obj.EEG );
                    
                    
            end
        end
        
        function obj = outputRow ( obj, stage )
            
            
            
            % function generates excel/csv outputs for each data object
            % can customize
            
            % >> stage 1
            % proc_state
            % subj_basename
            % subj_subfolder
            % study_title
            % filenameimport
            
            % generate ALL property array
            propArr = properties(obj);
            
            
            
            
            % Assign proper stage
            % I - imported
            % C - post-manual clean
            % P - Post ICA
            % C - Components identified
            % R - Ready for Analysis
            
            if strcmp(stage,'import') || strcmp(stage,'import_trim'), obj.proc_state = 'Import'; end
            if strcmp(stage,'preica'), obj.proc_state = 'PreICA'; end
            if strcmp(stage, 'error'), if ~strcmp('Error', obj.proc_state(1:5)) obj.proc_state = ['Error-' obj.proc_state]; end; end
            
            %if strcmp(stage, 'postica'),obj.proc_state = 'PostICA';end
            
            
            % create Array of all properties in DataClass
            % general properties
            propArr = properties(obj);
            % custom properties
            customArr = {};
            if strcmp(stage,'import_trim')
                obj.filename.import = [obj.EEG.setname '.set'];
            end
            
            % structure specific
            fnArr = fields(obj.filename);
            pathArr = fields(obj.pathdb);
            eegArr = fields(obj.EEG);
            fnArr = cellfun(@(x) strcat('filename.', x), fnArr, 'UniformOutput', false);
            pathArr  = cellfun(@(x) strcat('pathdb.', x), pathArr, 'UniformOutput', false);
            eegArr  = cellfun(@(x) strcat('EEG.', x), eegArr, 'UniformOutput', false);
            propArr = [customArr; propArr; fnArr; pathArr; eegArr];
            % add numerical index
            propArr = [[num2cell(1:length(propArr))]' propArr];
            fidx = @(x) find(strcmp([propArr(:,2)], x));
            
            
            %             if strcmp(stage,'import') || strcmp(stage,'import_trim')
            %
            %                 % first column state
            %                 obj.proc_state = 'I';
            %
            %             end
            % original sample rate
            
            % new sample rate
            
            % original duration of recording
            
            % original channel number
            
            selColumns = [ ...
                fidx('proc_timetag') ...
                fidx('study_user') ...
                fidx('proc_state') ...
                fidx('subj_basename') ...
                fidx('subj_id') ...
                fidx('subj_subfolder') ...
                fidx('study_type') ...
                fidx('study_title') ...
                fidx('net_nbchan_orig') ...
                fidx('net_nbchan_post') ...
                fidx('proc_badchans') ...
                fidx('proc_filt_bandlow') ...
                fidx('proc_filt_bandhigh') ...
                fidx('proc_filt_lowcutoff') ...
                fidx('proc_filt_highcutoff') ...
                fidx('proc_sRate_raw') ...
                fidx('proc_sRate1') ...
                fidx('proc_xmax_raw') ...
                fidx('proc_xmax_post') ...
                fidx('proc_xmax_percent') ...
                fidx('proc_xmax_epoch') ...
                fidx('epoch_length') ...
                fidx('epoch_limits') ...
                fidx('epoch_trials') ...
                fidx('epoch_badtrials') ...
                fidx('epoch_percent') ...
                fidx('epoch_badid') ...
                fidx('proc_removed_regions') ...
                fidx('proc_dataRank') ...
                fidx('proc_ipchans') ...
                fidx('proc_icaweights') ...
                fidx('proc_removeComps') ...
                fidx('filename.import') ...
                ];
            
            obj.log_subjHeader = propArr(selColumns,2)';
            
            tmpArr = {};
            
            
            
            
            % Fill selected columns with data
            tmpArr = {};
            
            for i = 1 : length(selColumns)
                rowidx = selColumns(i);
                
                
                
                if rowidx == 0
                    tmpArr{end+1} = ' ';
                else
                    str = ['obj.' propArr{selColumns(i),2}];
                    tmpArr{end+1} = num2str(eval(str));
                end
                
                
                if rowidx == fidx('proc_timetag')
                    tmpArr{i} = strcat('D',num2str(tmpArr{i}));
                    %tmpArr{i} = num2str(tmpArr{i});
                    
                end
                
            end
            
            obj.log_subjRow = tmpArr;
            
        end
        
        
        function obj = lapData( obj )
            
            obj = lap( obj );
        end
        
        function o = sigPowTable( o )
            
            % load dataset
            o.loadDataset('import');
            
            %% What are the limits for each power band?
            frq.maxfreq               = 80;
            
            frq.delta.title           = 'Delta';        frq.theta.title           = 'Theta';
            frq.delta.range           = [0 4];          frq.theta.range           = [4 8];
            
            frq.lo_alpha.title        = 'Low_Alpha';    frq.hi_alpha.title        = 'High_Alpha';
            frq.lo_alpha.range        = [8 10];         frq.hi_alpha.range        = [10 13];
            
            frq.beta.title            = 'Beta';         frq.gamma.title           = 'Gamma';
            frq.beta.range            = [13 30];        frq.gamma.range           = [30 frq.maxfreq];
            
            frq.total.title           = 'Total Range';
            frq.total.range           = [1  frq.maxfreq];
            
            frq.totalex.title         = 'Total Range (excluding gaps)';
            frq.totalex.range         = [frq.delta.range(1):frq.delta.range(end), ...
                frq.theta.range(1):frq.theta.range(end)];
            
            bandtitle = {'Delta','Theta', 'LowAlpha','HighAlpha','Beta','Gamma'};
            
            
            % Define freq. bands and result arrays
            maxfreq = frq.maxfreq;
            
            frq_delta    = [frq.delta.range(1)      frq.delta.range(2)];
            frq_theta    = [frq.theta.range(1)      frq.theta.range(2)];
            frq_lo_alpha = [frq.lo_alpha.range(1)   frq.lo_alpha.range(2)];
            frq_hi_alpha = [frq.hi_alpha.range(1)   frq.hi_alpha.range(2)];
            frq_beta     = [frq.beta.range(1)       frq.beta.range(2)];
            frq_gamma    = [frq.gamma.range(1)      frq.gamma.range(2)];
            frq_total    = [frq.delta.range(1)      maxfreq];
            frq_totalex  = frq.totalex.range;
            
            strdelta            = sprintf('%s %s', frq.delta.title, mat2str(frq_delta));
            strtheta            = sprintf('%s %s', frq.theta.title,mat2str(frq_theta));
            strloalpha          = sprintf('%s %s', frq.lo_alpha.title,mat2str(frq_lo_alpha));
            strhialpha          = sprintf('%s %s', frq.hi_alpha.title,mat2str(frq_hi_alpha));
            strbeta             = sprintf('%s %s', frq.beta.title,mat2str(frq_beta));
            strgamma            = sprintf('%s %s', frq.gamma.title,mat2str(frq_gamma));
            strtotal            = sprintf('%s %s', frq.total.title,mat2str(frq_total));
            strtotalex          = sprintf('%s %s', frq.total.title,mat2str(frq_totalex));
            
            strchanlabels = {strdelta, strtheta, strloalpha, strhialpha,strbeta,strgamma};
            
            
            % What is the results Excel template to use?
            %[~,subid,~] = fileparts(o.EEG.filename);
            subprefix = [o.subj_basename '_'];
            
            % define sampling rate
            srate        = o.EEG.srate;
            
            % define number of epochs
            nepochs      = length(o.EEG.data(1,1,:));
            
            % define number of channels
            nchans       = length(o.EEG.data(:,1,1));
            
            % define number of data points in epoch
            npnts        = length(o.EEG.data(1,:,1));
            
            % number of channels
            chanidx = linspace(1,nchans,nchans)';
            
            % define time (s) of data points in epoch
            time         = o.EEG.times;
            
            % define analysis frequencies
            hz              = linspace(0,srate/2,npnts/2);
            
            % data matrixes for pooled data to create averages
            mat_signal          = zeros(nepochs,npnts,nchans);
            mat_signal_dt       = zeros(nepochs,npnts,nchans);
            mat_signal_hn       = zeros(nepochs,npnts,nchans);
            mat_signalX         = zeros(nepochs,npnts,nchans);
            
            % data matrixes for freq data
            mat_amp             = zeros(nepochs,length(hz),nchans);
            mat_pow             = zeros(nepochs,length(hz),nchans);
            %mat_pow2            = zeros(nepochs, length(hz), nchans);
            
            % Optional tests
            mat_pow_nodt             = zeros(nepochs,length(hz),nchans);    % no detrend or window
            mat_pow_nohn             = zeros(nepochs,length(hz),nchans);    % no taper window (Hann)
            
            
            for chanid = 1:nchans
                for epochid = 1:nepochs
                    
                    % define data signal (per epoch)
                    signal      = o.EEG.data(chanid,:,epochid);
                    
                    % Detrend 2-second epochs for each channel
                    signal_dt   = detrend(signal);
                    
                    % Apply Hann window to detrended signal
                    signal_hn   = hann( length(signal) )' .* signal_dt ;
                    
                    %stitch_signal = [stitch_signal signal_hn];
                    
                    % Create Fourier co-efficients (0.5 Hz freq steps)
                    signalX         = fft( signal_hn, npnts ) ./ npnts;
                    
                    % Multiply positive-frequency coefficients by 2
                    posfrex  = 2:floor(npnts/2);
                    ampl = abs(signalX( 1:length(hz) ));
                    ampl(posfrex) = ampl(posfrex)*2;
                    
                    % Square amplitude to calculate Power
                    power = ampl .^ 2;
                    
                    % capture amplitude & power per epoch in matrix
                    mat_amp(epochid,:,chanid)             = ampl;
                    mat_pow(epochid,:,chanid)             = power;
                    
                    % calculate power band with matlab internal function
                    %mat_pow2(epochid,:,chanid)            = bandpower(signal_hn,256,[0 128]);
                    
                    % Consider effects of detrending and hann window
                    signalX_nodt            = fft( signal, npnts ) ./ npnts;
                    signalX_nohn            = fft( signal_dt, npnts ) ./ npnts;
                    
                    % power calculation without detrending or Hann window
                    power_nodt              = abs(signalX_nodt( 1:length(hz) ));
                    power_nodt(posfrex)     = power_nodt(posfrex)*2;
                    power_nodt              = power_nodt .^2;
                    mat_pow_nodt(epochid,:,chanid)             = power_nodt;
                    
                    % power calculation with detrending but NO Hann window
                    power_nohn              = abs(signalX_nohn( 1:length(hz) ));
                    power_nohn(posfrex)     = power_nohn(posfrex)*2;
                    power_nohn              = power_nohn .^2;
                    mat_pow_nohn(epochid,:,chanid) = power_nohn;
                    
                    
                    % matrix of raw amplitudes
                    % mat_signal(epochs, samples, channels)
                    mat_signal(epochid,:,chanid)          = signal;
                    mat_signal_dt(epochid,:,chanid)       = signal_dt;
                    mat_signal_hn(epochid,:,chanid)       = signal_hn;
                    mat_signalX(epochid,:,chanid)         = signalX;
                    
                end % epoch loop
            end % channel loop
            
            mean_signal             = zeros(nchans,size(mat_signal,2));
            mean_signal_dt          = zeros(nchans,size(mat_signal_dt,2));
            mean_signal_hn          = zeros(nchans,size(mat_signal_hn,2));
            
            mean_signalX            = zeros(nchans,size(mat_signalX,2));
            
            mean_amp                = zeros(nchans, length(hz)); %length(mat_amp));
            mean_pow                = zeros(nchans, length(hz)); %length(mat_pow));
            
            mean_pow_nodt          = zeros(nchans,size(mat_pow_nodt,2));
            mean_pow_nohn          = zeros(nchans,size(mat_pow_nohn,2));
            
            
            for chanid = 1:nchans
                
                mean_signal(chanid,:)                = squeeze(mean(mat_signal(:,:,chanid),1));
                mean_signal_dt(chanid,:)             = squeeze(mean(mat_signal_dt(:,:,chanid),1));
                mean_signal_hn(chanid,:)             = squeeze(mean(mat_signal_hn(:,:,chanid),1));
                
                mean_signalX(chanid,:)            = squeeze(mean(mat_signalX(:,:,chanid),1));
                mean_amp(chanid,:)                = squeeze(mean(mat_amp(:,:,chanid),1));
                mean_pow(chanid,:)                = squeeze(mean(mat_pow(:,:,chanid),1));
                
                mean_signal_nodt(chanid,:)             = squeeze(mean(mat_pow_nodt(:,:,chanid),1));
                mean_signal_nohn(chanid,:)             = squeeze(mean(mat_pow_nohn(:,:,chanid),1));
                
            end
            
            
            rel_mean_pow80      = zeros(nchans,maxfreq);
            
            % create rel power array
            for chanid = 1:nchans
                
                rel_mean_pow80(chanid,:) = mean_pow(chanid,[1:maxfreq]) / sum(mean_pow(chanid,[1:maxfreq]),2);
                
            end
            
            
            bandtitle = { frq.delta.title, frq.theta.title,  frq.lo_alpha.title, ...
                frq.hi_alpha.title, frq.beta.title, frq.gamma.title };
            
            % define result arrays
            res_delta    = zeros(nepochs,1,nchans);
            res_theta    = zeros(nepochs,1,nchans);
            res_lo_alpha = zeros(nepochs,1,nchans);
            res_hi_alpha = zeros(nepochs,1,nchans);
            res_beta     = zeros(nepochs,1,nchans);
            res_gamma    = zeros(nepochs,1,nchans);
            
            
            [ delta_sum, delta_avg ] = powband( frq_delta, mean_pow, hz, srate, npnts );
            [ theta_sum, theta_avg ] = powband( frq_theta, mean_pow, hz, srate, npnts );
            [ lo_alpha_sum, lo_alpha_avg ] = powband( frq_lo_alpha, mean_pow, hz, srate, npnts );
            [ hi_alpha_sum, hi_alpha_avg ] = powband( frq_hi_alpha, mean_pow, hz, srate, npnts );
            [ beta_sum, beta_avg ] = powband( frq_beta, mean_pow, hz, srate, npnts );
            [ gamma_sum, gamma_avg ] = powband( frq_gamma, mean_pow, hz, srate, npnts );
            [ total_sum_all, total_avg ] = powband( frq_total, mean_pow, hz, srate, npnts );
            
            % total sum of relative power based on true intervals
            total_sum = delta_sum + theta_sum + lo_alpha_sum + hi_alpha_sum + beta_sum + gamma_sum;
            
            % Calculate relative power for each of the channel channels:
            % (power at specific frequency) / (total power)
            [delta_rel]      = relpow( delta_sum, total_sum, nchans );
            [theta_rel]      = relpow( theta_sum, total_sum, nchans );
            [lo_alpha_rel]   = relpow( lo_alpha_sum, total_sum, nchans );
            [hi_alpha_rel]   = relpow( hi_alpha_sum, total_sum, nchans );
            [beta_rel]       = relpow( beta_sum, total_sum, nchans );
            [gamma_rel]      = relpow( gamma_sum, total_sum, nchans );
            
            
            % output result table to file
            % relative power
            rel_tb = table(chanidx,delta_rel,theta_rel, ...
                lo_alpha_rel,hi_alpha_rel,beta_rel,gamma_rel);
            
            % absolute power (sum)
            abs_tb = table(chanidx,delta_sum,theta_sum, ...
                lo_alpha_sum,hi_alpha_sum,beta_sum,gamma_sum);
            
            % absolute power (average)
            avg_tb = table(chanidx,delta_avg,theta_avg, ...
                lo_alpha_avg,hi_alpha_avg,beta_avg,gamma_avg);
            
            
            % write tables to file
            
            savefile = fullfile(o.pathdb.analysis,[subprefix '_s4_power.xls']);
            % copyfile( templatefile, savefile );
            
            writetable( abs_tb, savefile, 'sheet', 'abs_sum' );
            writetable( avg_tb, savefile, 'sheet', 'abs_avg' );
            writetable( rel_tb, savefile, 'sheet', 'abs_rel' );
            
            spsslong = zeros(nchans,9);
            
            savefile = fullfile(o.pathdb.analysis,[subprefix '_s4_pow_spss.csv']);
            fid = fopen( savefile, 'w' );
            
            fprintf(fid,'%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s\n', ...
                'Subject', 'Group', 'Timepoint', 'Channel', 'Rel_Pow', ...
                frq.delta.title, frq.theta.title, frq.lo_alpha.title, ...
                frq.hi_alpha.title, frq.beta.title, frq.gamma.title);
            
            for chanid = 1:nchans
                fprintf(fid,'%s, %s, %s, %d,%s,%f,%f,%f,%f,%f,%f\n',subprefix, '','', chanid,'relpow', ...
                    rel_tb{1,2}, rel_tb{1,3}, rel_tb{1,4}, ...
                    rel_tb{1,5}, rel_tb{1,6}, rel_tb{1,7});
                fprintf(fid,'%s,%s, %s, %d,%s,%f,%f,%f,%f,%f,%f\n',subprefix, '','', chanid,'abspow', ...
                    abs_tb{1,2}, abs_tb{1,3}, abs_tb{1,4}, ...
                    abs_tb{1,5}, abs_tb{1,6}, abs_tb{1,7});
                fprintf(fid,'%s,%s, %s,%d,%s,%f,%f,%f,%f,%f,%f\n',subprefix, '','', chanid,'avgpow', ...
                    avg_tb{1,2}, avg_tb{1,3}, avg_tb{1,4}, ...
                    avg_tb{1,5}, avg_tb{1,6}, avg_tb{1,7});
            end
            
            fclose( fid );
            
            titlestr.pow_topo_rel = 'test';
            titlestr.pow_topo_fix = 'test';
            titlestr.pow_plot = 'test';
            
            savefile = fullfile(o.pathdb.analysis,[subprefix '_s4_pow_topo_rel.png']);
            htp_rest_power_plots(rel_tb, titlestr, o.pathdb.analysis, o.EEG.chanlocs, savefile, strchanlabels, 'relative');
            
            savefile = fullfile(o.pathdb.analysis,[subprefix '_s4_pow_topo_fixed.png']);
            htp_rest_power_plots(rel_tb, titlestr, o.pathdb.analysis, o.EEG.chanlocs, savefile, strchanlabels, 'fixed');
            
            savefile = fullfile(o.pathdb.analysis,[subprefix '_s4_plot.png']);
            powplot(mean_pow(1:o.EEG.nbchan,:), hz, 'relpow', savefile);
            
            
            for i = 1:length(o.net_regions)
                
                savefile = fullfile(o.pathdb.analysis,[subprefix '_s' num2str(i) '_plot.png']);
                powplot(mean_pow(o.net_regions{i,3},:), hz, o.net_regions{i,1}, savefile);
                
            end
            
            % unload data
            o.removeData;
            
            
            %
            % savefile = fullfile([subprefix '_s4_pow_topo.png']);
            % plot_stitch(subprefix, savefile, o.pathdb.analysis);
            
            
            
        end
        
        function o = sig_ispc( o )
            % load data
            o.loadDataset('import');
            
            % define all possible unique pairs of channels
            o.sigCreateUniqueChanPairs;
            pairs = o.sigPairs;
            
            channel1 = num2str(pairs(1,1));
            channel2 = num2str(pairs(1,2));
            
            % specify some time-frequency parameters
            freqs2use  = logspace(log10(4),log10(30),15); % 4-30 Hz in 15 steps
            times2save = -400:10:1000;
            timewindow = linspace(1.5,3,length(freqs2use)); % number of cycles on either end of the center point (1.5 means a total of 3 cycles))
            baselinetm = [-400 -200];
            
            % wavelet and FFT parameters
            time          = -1:1/o.EEG.srate:1;
            half_wavelet  = (length(time)-1)/2;
            num_cycles    = logspace(log10(4),log10(8),length(freqs2use));
            n_wavelet     = length(time);
            n_data        = o.EEG.pnts*o.EEG.trials;
            n_convolution = n_wavelet+n_data-1;
            
            % time in indices
            times2saveidx = dsearchn(o.EEG.times',times2save');
            baselineidxF  = dsearchn(o.EEG.times',baselinetm');  % for the full temporal resolution data (thanks to Daniel Roberts for finding/reporting this bug here!)
            baselineidx   = dsearchn(times2save',baselinetm'); % for the temporally downsampled data
            
            chanidx = zeros(1,2); % always initialize!
            chanidx(1) = find(strcmpi(channel1,{o.EEG.chanlocs.labels}));
            chanidx(2) = find(strcmpi(channel2,{o.EEG.chanlocs.labels}));
            
            
            % data FFTs
            data_fft1 = fft(reshape(o.EEG.data(chanidx(1),:,:),1,n_data),n_convolution);
            data_fft2 = fft(reshape(o.EEG.data(chanidx(2),:,:),1,n_data),n_convolution);
            
            % initialize
            ispc    = zeros(length(freqs2use),o.EEG.pnts);
            pli     = zeros(length(freqs2use),o.EEG.pnts);
            wpli    = zeros(length(freqs2use),o.EEG.pnts);
            dwpli   = zeros(length(freqs2use),o.EEG.pnts);
            dwpli_t = zeros(length(freqs2use),length(times2save));
            ispc_t  = zeros(length(freqs2use),length(times2save));
            
            for fi=1:length(freqs2use)
                
                % create wavelet and take FFT
                s = num_cycles(fi)/(2*pi*freqs2use(fi));
                wavelet_fft = fft( exp(2*1i*pi*freqs2use(fi).*time) .* exp(-time.^2./(2*(s^2))) ,n_convolution);
                
                % phase angles from channel 1 via convolution
                convolution_result_fft = ifft(wavelet_fft.*data_fft1,n_convolution);
                convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
                sig1 = reshape(convolution_result_fft,o.EEG.pnts,o.EEG.trials);
                
                % phase angles from channel 2 via convolution
                convolution_result_fft = ifft(wavelet_fft.*data_fft2,n_convolution);
                convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
                sig2 = reshape(convolution_result_fft,o.EEG.pnts,o.EEG.trials);
                
                % cross-spectral density
                cdd = sig1 .* conj(sig2);
                
                % ISPC
                ispc(fi,:) = abs(mean(exp(1i*angle(cdd)),2)); % note: equivalent to ispc(fi,:) = abs(mean(exp(1i*(angle(sig1)-angle(sig2))),2));
                
                
                % take imaginary part of signal only
                cdi = imag(cdd);
                
                % phase-lag index
                pli(fi,:)  = abs(mean(sign(imag(cdd)),2));
                
                % weighted phase-lag index (eq. 8 in Vink et al. NeuroImage 2011)
                wpli(fi,:) = abs( mean( abs(cdi).*sign(cdi) ,2) )./mean(abs(cdi),2);
                
                % debiased weighted phase-lag index (shortcut, as implemented in fieldtrip)
                imagsum      = sum(cdi,2);
                imagsumW     = sum(abs(cdi),2);
                debiasfactor = sum(cdi.^2,2);
                dwpli(fi,:)  = (imagsum.^2 - debiasfactor)./(imagsumW.^2 - debiasfactor);
                
                % compute time window in indices for this frequency
                time_window_idx = round((1000/freqs2use(fi))*timewindow(fi)/(1000/o.EEG.srate));
                
                for ti=1:length(times2save)
                    imagsum        = sum(cdi(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:),1);
                    imagsumW       = sum(abs(cdi(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:)),1);
                    debiasfactor   = sum(cdi(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:).^2,1);
                    dwpli_t(fi,ti) = mean((imagsum.^2 - debiasfactor)./(imagsumW.^2 - debiasfactor));
                    
                    % compute phase synchronization
                    phasesynch     = abs(mean(exp(1i*angle(cdd(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:))),1));
                    ispc_t(fi,ti)  = mean(phasesynch);
                end
            end
            
            % baseline subtraction from all measures
            ispc    = bsxfun(@minus,ispc,mean(ispc(:,baselineidxF(1):baselineidxF(2)),2)); % not plotted in the book, but you can plot it for comparison with PLI
            ispc_t  = bsxfun(@minus,ispc_t,mean(ispc_t(:,baselineidx(1):baselineidx(2)),2));
            pli     = bsxfun(@minus,pli,mean(pli(:,baselineidxF(1):baselineidxF(2)),2));
            dwpli   = bsxfun(@minus,dwpli,mean(dwpli(:,baselineidxF(1):baselineidxF(2)),2));
            dwpli_t = bsxfun(@minus,dwpli_t,mean(dwpli_t(:,baselineidx(1):baselineidx(2)),2));
            
            tbl_dwpli_t = array2table(dwpli_t);
            
            figure
            subplot(221)
            contourf(times2save,freqs2use,pli(:,times2saveidx),40,'linecolor','none')
            set(gca,'clim',[-.3 .3],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('PLI over trials')
            
            subplot(222)
            contourf(times2save,freqs2use,dwpli(:,times2saveidx),40,'linecolor','none')
            set(gca,'clim',[-.2 .2],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('dWPLI over trials')
            
            subplot(223)
            contourf(times2save,freqs2use,ispc_t,40,'linecolor','none')
            set(gca,'clim',[-.1 .1],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('ICPS over time')
            
            subplot(224)
            contourf(times2save,freqs2use,dwpli_t,40,'linecolor','none')
            set(gca,'clim',[-.1 .1],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('dWPLI over time')
            
        end
        
        
        
    end
end


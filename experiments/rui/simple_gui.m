function simple_gui2(grp1, grp2)
% SIMPLE_GUI2 1) Select a data set and a upper frequency bound from the pop-up menus
% 2) click one of the plot-type push buttons to plot the selected data in the axes

% Note:
% load('resting_alpha_peak_shift.mat')
% simple_gui2(grp1, grp2)

%  Create and then hide the UI as it is being constructed.
f = figure('Visible','off','Position',[360,500,450,285]);

% Construct the components.
hplot    = uicontrol('Style','pushbutton','String','Plot', ...
            'Position',[325,220,70,25],'Callback',@plotbutton_Callback);
hstem = uicontrol('Style','pushbutton', 'String','Stem', ...
            'Position',[325,195,70,25],'Callback',@stembutton_Callback);
htext1  = uicontrol('Style','text','String','Select Data Group',...
           'Position',[325,170,120,15]);
hpopup1 = uicontrol('Style','popupmenu','String',{'group 1','group 2','both'},...
           'Position',[325,140,100,25], 'Callback',@popup_menu1_Callback);
htext2  = uicontrol('Style','text','String','Select Upper Bound',...
           'Position',[300,120,150,15]);
hpopup2 = uicontrol('Style','popupmenu', 'String',{'20Hz','40Hz','60Hz','80Hz'},...
           'Position',[325,90,100,25], 'Callback',@popup_menu2_Callback);
ha = axes('Units','pixels','Position',[50,60,230,185]);
hzoomon = uicontrol('Style', 'pushbutton', 'Callback', @zoomon_Callback, ...
                         'String', 'Zoom On', 'Position', [325 60 85 25]);
hzoomoff = uicontrol('Style', 'pushbutton', 'Callback', @zoomoff_Callback, ... 
                          'String', 'Zoom Off', 'Position', [325 35 85 25]);
align([hplot,hstem,htext1,hpopup2,htext2,hpopup2,hzoomon,hzoomoff],'Center','None');



% Initialize the UI.
% Change units to normalized so components resize automatically.
f.Units = 'normalized';
ha.Units = 'normalized';
hplot.Units = 'normalized';
hstem.Units = 'normalized';
htext1.Units = 'normalized';
hpopup1.Units = 'normalized';
htext2.Units = 'normalized';
hpopup2.Units = 'normalized';
hzoomon.Units = 'normalized';
hzoomoff.Units = 'normalized';

% Generate the data to plot.
grp1_data = grp1;
grp2_data = grp2;

% Create a plot in the axes.
upper_bound =80;
current_data = grp1_data(1:(2*upper_bound+1), :);
plot(linspace(0,upper_bound,length(current_data)),current_data);
xlabel('Frequency (Hz)');ylabel('Relative Power')

% Assign the a name to appear in the window title.
f.Name = 'Simple GUI';
% Move the window to the center of the screen.
movegui(f,'center')
% Make the window visible.
f.Visible = 'on';

%  Pop-up menu callback. Read the pop-up menu Value property to
%  determine which item is currently displayed and make it the
%  current data. This callback automatically has access to 
%  current_data because this function is nested at a lower level.
   function popup_menu1_Callback(source,eventdata) 
      % Determine the selected data set.
      str = get(source, 'String');
      val = get(source,'Value');
      % Set current data to the selected data set.
      switch str{val}
      case 'group 1' % User selects Peaks.
         current_data = grp1;
      case 'group 2' % User selects Membrane.
         current_data = grp2;
      case 'both'
          current_data = [grp1 grp2];
      end
   end

   function popup_menu2_Callback(source,eventdata) 
      % Determine the selected data set.
      str = get(source, 'String');
      val = get(source,'Value');
      % Set current data to the selected data set.
      switch str{val}
      case '20Hz' % User selects Peaks.
         upper_bound = 20;
      case '40Hz' % User selects Membrane.
         upper_bound = 40;
      case '60Hz'
         upper_bound = 60;
      case '80Hz'
         upper_bound = 80;
      end
   end

  % Push button callbacks. Each callback plots current_data in the
  % specified plot type.

  function plotbutton_Callback(source,eventdata) 
  % Display surf plot of the currently selected data.
       data = current_data(1:(2*upper_bound+1), :);
       plot(linspace(0, upper_bound, size(data, 1)), data);
       xlabel('Frequency (Hz)');ylabel('Relative Power')
  end

  function stembutton_Callback(source,eventdata) 
  % Display contour plot of the currently selected data.
       data = current_data(1:(2*upper_bound+1), :);
       plot(linspace(0, upper_bound, size(data, 1)), data, '-o');
       xlabel('Frequency (Hz)');ylabel('Relative Power')
  end

  function zoomon_Callback(source,eventdata) 
       zoom on
  end

  function zoomoff_Callback(source,eventdata) 
        zoom off
  end

end
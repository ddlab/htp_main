

% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (11/2018)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% For U54p1 data set
% setup environment
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

% vectorization of subject array
for i = 1:length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
                        
            % power analysis
%             s.setFreqTable([.5,3.5;4,7.5;8,10;10.5,13;13.5,30;30.5,80]); % requires input
            s.setFreqTable();
            s.getPntsTable;
            s.generateStoreRoom;
            s.correlation_global_coupling;
%             s.bandAverageTrials;
%             s.generateNormalizedPowerSpectrum;
%             s.powerVisualization; % run under 1920*1440
%             s.surfaceLaplacian;
%             s.hilbert;
            % End Analysis methods
            
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end


%% Stage 6
% Amplitude coupling via rank correlation
nbgroups = 2;nbchan=15;
cum_zcorr = NaN(nbchan, 3, length(sub), nbgroups); % theta through upper alpha
% 15 channels mamually 
g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        temp1 = s.rest_corr_rho_theta;
        temp2 = s.rest_corr_rho_lalpha;
        temp3 = s.rest_corr_rho_ualpha;
        
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            % cumulative rel_power @ delta, theta, alpha1, alpha2, beta, gamma
            cum_zcorr(:, 1, i, gid) = temp1'; 
            cum_zcorr(:, 2, i, gid) = temp2'; 
            cum_zcorr(:, 3, i, gid) = temp3'; 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_zcorr(:, 1, i, gid) = temp1';
            cum_zcorr(:, 2, i, gid) = temp2'; 
            cum_zcorr(:, 3, i, gid) = temp3'; 
            g2 = g2+1;
            
        end
    end
end

% regrouping
cum_corr_g1 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(nbchan),randi(3),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_corr_g2 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(nbchan),randi(3),:,2)),2);

bandTable = ["\theta", "lower \alpha", "upper \alpha"];
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
figure
% (1) power Group 1
mean_corr_g1 = squeeze(mean(cum_corr_g1, 3));
recover_g1 = zeros(128,3);

for m=1:3
    recover_g1([65,70:71,73,75:77,81:85,90:91,94], m) = mean_corr_g1(:, m);
    subplot(4,3,m)
    topoplot(recover_g1(:,m), s.EEG.chanlocs)
    caxis([-.4, .4]);
    colorbar;
    title(bandTable(m),'FontSize', 8)
end
% (2) power Group 2
mean_corr_g2 = squeeze(mean(cum_corr_g2, 3));
recover_g2 = zeros(128,3);
for m=1:3
    recover_g2([65,70:71,73,75:77,81:85,90:91,94], m) = mean_corr_g2(:, m);
    subplot(4,3,3+m)
    topoplot(recover_g2(:,m), s.EEG.chanlocs)
    caxis([-.4, .4]);
    colorbar;
    title(bandTable(m),'FontSize', 8)
end
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_corr_g1, cum_corr_g2, chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(nbchan, 3);
t_orig_sig = NaN(nbchan, 3);
recover_g3 = zeros(128,3);
for m=1:3
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    recover_g3([65,70:71,73,75:77,81:85,90:91,94], m) = t_orig_sig(:, m);
    subplot(4,3,m+6);
    topoplot(recover_g3(:,m),s.EEG.chanlocs);
    caxis([0 6]);
    colorbar;
    title('FXS > HC');
end

% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_corr_g2,cum_corr_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(nbchan, 3);
t_orig_sig = NaN(nbchan, 3);
recover_g4 = zeros(128,3);
for m=1:3
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    recover_g4([65,70:71,73,75:77,81:85,90:91,94], m) = t_orig_sig(:, m);
    subplot(4,3,m+9);
    topoplot(recover_g4(:,m),s.EEG.chanlocs);
    caxis([0 6]);
    colorbar;
    title('FXS < HC');
end

suptitle('Correlation')


fxs=squeeze(mean(cum_corr_g1));
hc=squeeze(mean(cum_corr_g2));

figure;
scatter(fxs(1,:), ones(size(fxs(1,:))), 7.5, 'b');hold on
scatter(hc(1,:), 1.05*ones(size(hc(1,:))), 10, 'r');
scatter(fxs(2,:), 2*ones(size(fxs(2,:))), 7.5, 'b');
scatter(hc(2,:), 2.05*ones(size(hc(2,:))), 10, 'r');
scatter(fxs(3,:), 3*ones(size(fxs(3,:))), 7.5, 'b');
scatter(hc(3,:), 3.05*ones(size(hc(3,:))), 10, 'r');
ylim([0 4])
legend(['FXS (',num2str(g1),')'],['HC (',num2str(g2),')'])
yticks([1 2 3])
yticklabels({'\theta','lower \alpha','upper \alpha'})
xlabel('Normalized correlation')

figure;
bar([mean(fxs,2) mean(hc,2)]);
ylabel('mean of correlations')
legend('FXS','HC')
xticks([1 2 3])
xticklabels({'\theta band anterior','lower \alpha posterior','upper \alpha posterior'})
title('comparison to \gamma band subregion')
[h,p,ci,stats] = ttest2(fxs(1,:),hc(1,:)); % * pval=0.0227
[h,p,ci,stats] = ttest2(fxs(2,:),hc(2,:)); 
[h,p,ci,stats] = ttest2(fxs(3,:),hc(3,:)); % * pval=0.0022

%% gamma power difference region
cum_rel_pow = NaN(128, 6, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx_rel = s.rest_rel_power_band_average_trials;
        
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_rel_pow(:, :, i, gid) = pxx_rel'; 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_rel_pow(:, :, i, gid) = pxx_rel';
            g2 = g2+1;
            
        end
    end
end

% regrouping
cum_rel_power_g1 = cum_rel_pow(:,:,~isnan(...
    cum_rel_pow(randi(128),randi(6),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_rel_power_g2 = cum_rel_pow(:,:,~isnan(...
    cum_rel_pow(randi(128),randi(6),:,2)),2);

chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
nbband = 6;
bandTable = ["\delta", "\theta", "lower \alpha", ...
    "upper \alpha", "\beta", "\gamma"];

% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index1 = zeros(s.EEG.nbchan, nbband);
for m = 1:nbband
    index1(:,m) = logical(pval(:,m)<est_alpha);
end


%% Group alpha peak inspection
cum_rel_power = NaN(161,length(sub),2);
% cum_norm_power = NaN(161,length(sub),2);

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx_rel = s.rest_rel_power;
%         pxx_norm = s.rest_abs_norm_average_trials;
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel, 2)),2); 
%             cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel(:,:,...
%                 [65, 70:71, 73, 75:77, 81:85, 90:91, 94]), 2)),2); 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
%             cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel, 2)),2);
            cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel(:,:,...
                [65, 70:71, 73, 75:77, 81:85, 90:91, 94]), 2)),2);
            g2 = g2+1;
            
        end
    end
end
cum_rel_power_g1 = cum_rel_power(:,~isnan(cum_rel_power(randi(161),:,1)),1);
cum_rel_power_g2 = cum_rel_power(:,~isnan(cum_rel_power(randi(161),:,2)),2);
upper = 80;
grp1 = mean(cum_rel_power_g1, 2);
grp2 = mean(cum_rel_power_g2, 2);
plot(0:0.5:upper, linspace(0,upper,161)'.*grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
plot(0:0.5:upper, linspace(0,upper,161)'.*grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
legend({['Fragile X (',num2str(g1),')'],['Control (',num2str(g2),')']})
xlabel('Frequency (Hz)')
ylabel('F.* Relative Power')
% ylabel('Relative Power')

% data for alpha peak shift examination
% cum_norm_power_g1 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,1)),1);
% cum_norm_power_g2 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,2)),2);
% 
% % to be extended to GUI
% upper = 80;
% grp1 = mean(cum_norm_power_g1, 2);
% grp2 = mean(cum_norm_power_g2, 2);
% plot(0:0.5:upper, grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
% plot(0:0.5:upper, grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
% legend({['group1 (',num2str(g1),')'],['group2 (',num2str(g2),')']})
% xlabel('Frequency (Hz)')
% ylabel('Normalized Power')





% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (11/2018)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% For U54p1 data set
% Power, correlation analysis
% ref: connectivity 5 subjects took 894 sec
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 
[xls,txt,~] = xlsread('C:\data\U54P1\A00_ANALYSIS\U54_p1_Sex.xlsx');
tic
% vectorization of subject array
for i = 1:1%length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
            s.rest_subj_gender = txt(xls==str2double(strtok(s.subj_basename,...
            {'D','_'})));            
            % power analysis
%             s.setFreqTable([1,3; 4,7; 8,12; 13,30; 30,57; 63,100]); % requires input
%             s.setFreqTable([1,3; 4,7; 8,10; 10,12; 13,30; 30,110]);
            s.setFreqTable();
            s.getPntsTable;
%             s.generateStoreRoom;
%             s.correlation_global_coupling;
%             s.correlation_global_coupling2;
%             s.bandAverageTrials;
%             s.generateNormalizedPowerSpectrum;
%             s.powerVisualization; % run under 1920*1440    
%             s.surfaceLaplacian;
            s.hilbert;
            s.generateConnectivity;
            % End Analysis methods
            
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end
toc


%% Stage 6
% Amplitude coupling via rank correlation
nbgroups = 2;
% gamma_grp = [44,49:51,56,58:60,62:63,65:67,69:78,81:85,88:91,94:96,99:101,107,113,119];
gamma_grp = 1:128;
local_nbchan = length(gamma_grp);
cum_zcorr = NaN(local_nbchan, 3, length(sub), nbgroups); % theta through upper alpha
% 15 channels mamually 
g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
%         if strcmp(s.rest_subj_gender, 'Male')
            temp = s.rest_corr_rho_Jun;

            % frequencies*trials*chans
            if strcmp(s.subj_id, 'Group1')
                gid = 1;
                % cumulative rel_power @ theta* gamma, lower alpha* gamma
                %                        upper alpha* gamma
                cum_zcorr(:, :, i, gid) = temp'; 
                g1 = g1+1;

            elseif strcmp(s.subj_id, 'Group2')
                gid = 2;
                cum_zcorr(:, :, i, gid) = temp';
                g2 = g2+1;

            end
            
%         end
    end
end

nbchan = s.EEG.nbchan;
% regrouping
cum_corr_g1 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(local_nbchan),randi(2),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_corr_g2 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(local_nbchan),randi(2),:,2)),2);

bandTable = ["\theta - \gamma", "lower \alpha - \gamma", "upper \alpha - \gamma"];
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
figure
% (1) Correlation Group 1
mean_corr_g1 = squeeze(mean(cum_corr_g1, 3));
recover_g1 = zeros(nbchan,3);

for m=1:3
%     recover_g1(gamma_grp, m) = ones(size(mean_corr_g1(:,m)));
    recover_g1(gamma_grp, m) = mean_corr_g1(:, m);
    subplot(4,3,m)
    topoplot(recover_g1(:,m), s.EEG.chanlocs)
    caxis([-.3, .3]);
    colorbar;
    title(bandTable(m),'FontSize', 8)
    
end
% (2) Correlation Group 2
mean_corr_g2 = squeeze(mean(cum_corr_g2, 3));
recover_g2 = zeros(nbchan,3);
for m=1:3
    recover_g2(gamma_grp, m) = mean_corr_g2(:, m);
    subplot(4,3,3+m)
    topoplot(recover_g2(:,m), s.EEG.chanlocs)
    caxis([-.3, .3]);
    colorbar;
    title(bandTable(m),'FontSize', 8)
end
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_corr_g1, cum_corr_g2, chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(local_nbchan, 3);
t_orig_sig = NaN(local_nbchan, 3);
recover_g3 = zeros(nbchan,3);
for m=1:3
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    recover_g3(gamma_grp, m) = t_orig_sig(:, m);
    subplot(4,3,6+m);
    topoplot(recover_g3(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    colorbar;
    title('FXS > HC');
end

% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_corr_g2,cum_corr_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(local_nbchan, 3);
t_orig_sig = NaN(local_nbchan, 3);
recover_g4 = zeros(nbchan,3);
for m=1:3
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    recover_g4(gamma_grp, m) = t_orig_sig(:, m);
    subplot(4,3,9+m);
    topoplot(recover_g4(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    colorbar;
    title('FXS < HC');
end

suptitle('Normalized Correlation')
%

fxs=squeeze(mean(cum_corr_g1)); % 3*25 (male)
hc=squeeze(mean(cum_corr_g2)); % 3*26 

figure;
for i=1:3
    subplot(1,3,i)
    scatter(1*ones(size(fxs(i,:))), fxs(i,:), 8, 'LineWidth', .8, 'MarkerEdgeAlpha',.5)
    hold on
    scatter(2*ones(size(hc(i,:))), hc(i,:), 8, 'LineWidth', .8, 'MarkerEdgeAlpha',.5)
    hold off
    title(bandTable(i))
    xlim([0 3])
    xticks([1 2])
    xticklabels({'FXS','Control'})
    ylabel('Normalized correlation')
end
suptitle('Both')

data=NaN(3,2,46);
data(:,1,1:44)=fxs;
data(:,2,:)=hc;
figure
for i=1:3
    subplot(1,3,i);boxplot(squeeze(data(i,:,:))')
    xticks([1 2]);xticklabels({'FXS','Control'})
    title(bandTable(i))
end
suptitle('Both')


figure;
bar([mean(fxs,2) mean(hc,2)]);
ylabel('mean of correlations')
xticks([1 2 3])
xticklabels({'\theta  - \gamma', 'lower \alpha - \gamma', ...
            'upper \alpha - \gamma'})
title('Normalized correlation (Both)')
% [h,p,ci,stats] = ttest2(fxs(1,:),hc(1,:)); % pval=.0015
hold on;scatter(1,-.12,'*','k')
% [h,p,ci,stats] = ttest2(fxs(2,:),hc(2,:)); % pval=.0150
scatter(2,.06,'*','k')
% [h,p,ci,stats] = ttest2(fxs(3,:),hc(3,:)); % pval=.0010
scatter(3,.13,'*','k');hold off
legend('FXS','HC','significance','\alpha=.05')


%% gamma power difference region
nbgroups = 2;nbchan = s.EEG.nbchan;
cum_rel_pow = NaN(nbchan, 6, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        if strcmp(s.rest_subj_gender, 'Male')
            pxx_rel = s.rest_rel_power_band_average_trials;

            % frequencies*trials*chans
            if strcmp(s.subj_id, 'Group1')
                gid = 1;
                cum_rel_pow(:, :, i, gid) = pxx_rel'; 
                g1 = g1+1;

            elseif strcmp(s.subj_id, 'Group2')
                gid = 2;
                cum_rel_pow(:, :, i, gid) = pxx_rel';
                g2 = g2+1;
            end
        end
    end
end

% regrouping
cum_rel_power_g1 = cum_rel_pow(:,:,~isnan(...
    cum_rel_pow(randi(nbchan),randi(6),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_rel_power_g2 = cum_rel_pow(:,:,~isnan(...
    cum_rel_pow(randi(nbchan),randi(6),:,2)),2);

nbband = 6;
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1, cum_rel_power_g2, chan_hood, 5000, .05, 1, .05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index1 = zeros(s.EEG.nbchan, nbband);
for m = 1:nbband
    index1(:,m) = logical(pval(:,m)<est_alpha);
end

[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g2, cum_rel_power_g1, chan_hood, 5000, .05, 1, .05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index2 = zeros(s.EEG.nbchan, nbband);
for m = 1:nbband
    index2(:,m) = logical(pval(:,m)<est_alpha);
end


%% Stage 6
% relative power
nbgroups = 2;
nbchan = s.EEG.nbchan;
nbband = 6;
cum_theta = NaN(nbchan, nbband, length(sub), nbgroups); 
cum_norm_power_band = NaN(nbchan, nbband, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        if strcmp(s.rest_subj_gender, 'Female')
            pxx_rel = s.rest_rel_power_band_average_trials;

            % frequencies*trials*chans
            if strcmp(s.subj_id, 'Group1')
                gid = 1;
                % cumulative rel_power @ delta, theta, alpha, beta, gamma1,
                % gamma2
                cum_theta(:, :, i, gid) = pxx_rel'; 
                g1 = g1+1;

            elseif strcmp(s.subj_id, 'Group2')
                gid = 2;
                cum_theta(:, :, i, gid) = pxx_rel';
                g2 = g2+1;

            end
            
        end
    end
    
end

% regrouping
cum_rel_power_g1 = cum_theta(:,:,~isnan(...
    cum_theta(randi(nbchan),randi(nbband),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_rel_power_g2 = cum_theta(:,:,~isnan(...
    cum_theta(randi(nbchan),randi(nbband),:,2)),2);


chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
bandTable = ["\delta", "\theta", "lower \alpha", "upper \alpha", ...
                                            "\beta", "\gamma"];

figure('units','normalized','outerposition',[0 0 1 1]);
% (1) power Group 1
mean_power_g1 = mean(cum_rel_power_g1, 3);
for m = 1:nbband
    subplot(4,6,m)
    topoplot(mean_power_g1(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
    if m == 1
        caxis([.3, .48]);
    elseif m == 2
        caxis([.12, .24]);
    elseif m == 3
        caxis([.04, .13]);
    elseif m == 4
        caxis([.03, .09]);
    elseif m == 5
        caxis([.1, .18]);
    else
        caxis([0.1, .4]);
    end
    h=colorbar('SouthOutside');
    set(h, 'Position', [.135*(m-1)+.16 .73 .045 .015]);
    title(bandTable(m),'FontSize', 12)
end
dim = [.07 .64 .2 .18]; % x y w h
str = {['FXS (',num2str(g1),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (2) power Group 2
mean_power_g2 = mean(cum_rel_power_g2, 3);
for m = 1:nbband
    subplot(4,6,m+6)
    topoplot(mean_power_g2(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
    if m == 1
        caxis([.3, .48]);
    elseif m == 2
        caxis([.12, .24]);
    elseif m == 3
        caxis([.04, .13]);
    elseif m == 4
        caxis([.03, .09]);
    elseif m == 5
        caxis([.1, .18]);
    else
        caxis([0.1, .4]);
    end

%     h=colorbar('SouthOutside');
%     set(h, 'Position', [.135*(m-1)+.16 .52 .045 .015]);
    
end
dim = [.07 .44 .2 .18]; % x y w h
str = {['Control (',num2str(g2),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+12);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    if m == 6
        h=colorbar('SouthOutside');
        set(h, 'Position', [.135*(6-1)+.16 .31 .045 .015]);
        h.Label.String = 'T values';
%         h.Label.FontSize = 10;
    end
end
dim = [.05 .24 .2 .18]; % x y w h
str = {'FXS > Control'};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g2,cum_rel_power_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+18);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
%     colorbar
end
dim = [.05 .04 .2 .18]; % x y w h
str = {'FXS < Control'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);

axes( 'Position', [0, 0.95, 1, 0.05] ) ;
set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
text( 0.5, 0, 'Relative Power (Female)', 'FontSize', 14', 'FontWeight', 'Bold', ...
  'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;


%% Group alpha peak inspection
cum_rel_power = NaN(s.pntsTable(6,2),length(sub),2);

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx_rel = s.rest_rel_power_pmtm;
%         pxx_norm = s.rest_abs_norm_average_trials;
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel, 2)),2); 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel, 2)),2);
            g2 = g2+1;
            
        end
    end
end
cum_rel_power_g1 = cum_rel_power(:,~isnan(cum_rel_power(randi(s.pntsTable(6,2)),:,1)),1);
cum_rel_power_g2 = cum_rel_power(:,~isnan(cum_rel_power(randi(s.pntsTable(6,2)),:,2)),2);
upper = s.freqTable(6,2);
grp1 = mean(cum_rel_power_g1, 2);
grp2 = mean(cum_rel_power_g2, 2);
plot(0:0.5:upper, linspace(0,upper,s.pntsTable(6,2))'.*grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
plot(0:0.5:upper, linspace(0,upper,s.pntsTable(6,2))'.*grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
% plot(0:0.5:upper, grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
% plot(0:0.5:upper, grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
legend({['Fragile X (',num2str(g1),')'],['Control (',num2str(g2),')']})
xlabel('Frequency (Hz)')
ylabel('F.* Relative Power')
% ylabel('Relative Power')

% data for alpha peak shift examination
% cum_norm_power_g1 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,1)),1);
% cum_norm_power_g2 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,2)),2);
% 
% % to be extended to GUI
% upper = 80;
% grp1 = mean(cum_norm_power_g1, 2);
% grp2 = mean(cum_norm_power_g2, 2);
% plot(0:0.5:upper, grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
% plot(0:0.5:upper, grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
% legend({['group1 (',num2str(g1),')'],['group2 (',num2str(g2),')']})
% xlabel('Frequency (Hz)')
% ylabel('Normalized Power')





% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 4 Manual Component Removal
% FILE: htp_stage4.m
%
% NOTES:

% setup environment
clear all; close all;
global current_stage; current_stage = 4; global user;
cprintf('blue','\nStarting Stage 4: Post-ICA Component Identification');

try 
    htp_config; 
    clearWarnings; 
catch 
    disp('Error in Configuration Files'); 
end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postica'; stage_next = 'postcomps';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

for i = 1 : length(sub)
    
    s = sub(i);
    
    if ismember(i, objStageStatus) % processes only files in correct stage
        

        s.setUser( user ); 
        
        s.loadDataset( 'postica' );
        
        % Open component topoplot, 20 components
        pop_selectcomps(s.EEG, 1:20);
        
        h.tp = gcf;
        h.tp.Position = [100 50 700 460];
        
        p = uipanel(h.tp,'Title','Component Selection Tool',...
            'Position',[.40 .1 .55 .22]);
        
        strStatus = sprintf('(%d of %d): %s', i, length(sub), s.subj_basename);
        
        title = uicontrol(p,'Style','text',...
            'String', strStatus,...
            'FontSize', 10,...
            'Units', 'normalized', ...
            'HorizontalAlignment', 'left', ...
            'BackgroundColor', [1 1 1], 'ForegroundColor', [0 0 0], ...
            'Position',[0.05 0.7 0.9 0.30]);
        
        b1 = uicontrol(p,'Style','pushbutton','String','Save Components',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback',@b1_callback, ...
            'BackgroundColor', [0 1 0], ...
            'Position',[.6 0.05 .35 .25]);
        
        t1 = uicontrol(p,'Style','edit',...
            'tag', 'comp_entry', ...
            'String','',...
            'Max',1,'Min',0,...
            'Units', 'normalized', ...
            'Position',[0.6 0.34 0.35 0.30]);
        
        b2 = uicontrol(p,'Style','pushbutton','String','Other Components',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback', @b2_callback, ...
            'Position',[.05 0.05 .35 .25]);
        
        closebtn = findobj('tag', 'Set threhsolds', 'parent', h.tp);
        
        
              %  delete(t2)
                t2 = uicontrol(p,'Style','edit',...
                    'tag', 'comp_entry2', ...
                    'String','',...
                    'Max',1,'Min',0,...
                    'Units', 'normalized', ...
                    'Position',[0.05 0.34 0.35 0.30]);
        
        
        % Open component time series
        pop_eegplot( s.EEG, 0, 1, 1);
        h.ep = gcf;
        h.ep.Units = 'pixels';
        h.ep.Position = [100 550 700 450];
        
        g = h.ep.UserData;
        
        % adjust default spacing/zoom
        ESpacing = findobj('tag','ESpacing','parent', h.ep);   % ui handle
        ESpacing.String = 50;
        eegplot('draws',0);
        
        % adjust default number of epochs & components shown
        g.winlength = 10;
        g.dispchans = 10;
        
        if g.dispchans < 0 || g.dispchans > g.chans
            g.dispchans = g.chans;
        end
        
        set(h.ep, 'UserData', g);
        eegplot('updateslider', h.ep);
        eegplot('drawp',0);
        eegplot('scaleeye', [], h.ep);
        
        uiwait(h.ep);
        
        s.proc_state = 'postcomps';
        
        % save cleaned dataset into the preica directory
        s.storeDataset( s.EEG, pathdb.postcomps, s.subj_subfolder, s.filename.postcomps);
        
        
        % unload data & decrease memory footprint
        s.unloadDataset;
        
        s.outputRow('postcomps');
        
        
    else
         
        if ismember(i, objStageStatus_completed)
            s.unloadDataset;
            s.proc_state = 'postcomps';
            s.outputRow(stage_next);
            prev_files = prev_files + 1;
            
        else
            s.unloadDataset;
            s.outputRow('error');
            s.outputRow('error');
            skip_files = skip_files + 1;
        end
        
        
    end
    
    sub(i) = s;
    
end

cprintf('*blue','\nSummary', length(objStageStatus));
cprintf('*green','\nFiles Processed: %d\n', length(objStageStatus));
cprintf('*black','Previously Completed Files: %d\n', prev_files);
cprintf('*red','Skipped Files: %d\n', skip_files);
cprintf('*blue','Other Errors: %d\n', errorchk);

fname = createResultsCsv(sub, 'postcomps');



function b2_callback(src, event)

comps = findobj('tag', 'comp_entry2');


pop_prop( src.UserData.EEG, 0, str2num(comps.String), NaN, {'freqrange' [2 50] });


%pop_selectcomps(src.UserData.EEG);

end

function b1_callback(src, event)

comps = findobj('tag', 'comp_entry');

src.UserData.proc_removeComps = str2num(comps.String);

close all;
end



% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (01/2019)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% P2 dataset correlation analysis 
% ~428 seconds to run Stage 5
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 
[xls0,txt,~] = xlsread([basePath,'\A00_ANALYSIS\[update]EEG_Code_U54_DeID.xlsx']);
tic
% vectorization of subject array
for i = 1:length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
            s.rest_subj_gender = txt(xls0 == str2double(strtok(s.subj_basename,...
            {'D','_'})), 2);      
            s.regroup;
            % Amplitude coupling analysis
%             s.setFreqTable([1,3; 4,7; 8,12; 13,30; 30,57; 63,100]); % requires input
%             s.setFreqTable();
%             s.getPntsTable;
%             s.generateStoreRoom3;
            s.correlation_global_coupling;
%             s.correlation_global_coupling2;
            % End Analysis methods
            
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end
toc


%% Stage 6
% Amplitude coupling via rank correlation
% gender info oin line 111, 223-224
nbchan = s.EEG.nbchan;
% bandTable = ["\theta - \gamma", "lower \alpha - \gamma", "upper \alpha - \gamma"];
bandTable = ["\theta - lower \gamma", "\alpha - lower \gamma", "\beta - lower \gamma"];
% between subjects
for tmt=1:4 % treatment group input
    nbgroups = 2;
    [xls,~,~] = xlsread([s.pathdb.analysis,'\D',num2str(tmt,'%02d'),'.xlsx']);
    % extract numeric as the true matching criteria

    cum_zcorr = NaN(s.EEG.nbchan, 3, length(sub), nbgroups); 

    flabel = NaN(size(xls,1), nbgroups); % fake label
    g1=0; g2=0;
    for i = 1: length(sub)

        s = sub(i);

        if ismember(i, objStageStatus)
%             if strcmp(s.rest_subj_gender, 'Male')
                temp = s.rest_corr_rho_Jun;

                % frequencies*trials*chans
                if strcmp(s.subj_field, ['S01D', num2str(tmt, '%02d')])
                    gid = 1;
                    % cumulative rel_power @ delta, theta, alpha1, alpha2, beta, gamma
                    cum_zcorr(:, :, i, gid) = temp';
                    g1 = g1+1;
                    flabel(g1, gid) = str2double(strtok(s.subj_basename, {'D','_'}));

                elseif strcmp(s.subj_field, ['S02D', num2str(tmt, '%02d')])
                    gid = 2;
                    cum_zcorr(:, :, i, gid) = temp';
                    g2 = g2+1;
                    flabel(g2, gid) = str2double(strtok(s.subj_basename, {'D','_'}));
                end
%             end
        end
    end

    [C1,~,mlabel_grp1] = intersect(xls(:,1),flabel(:,1),'stable'); % matching label
    [C2,~,mlabel_grp2] = intersect(xls(:,3),flabel(:,2),'stable');
%     [C1 C2]

    % regrouping
    cum_corr_g1 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(nbchan),randi(2),:,1)),1); 
    % % ideally nbChan*(6)nbBand*nbSubj
    % % isnan returns an array the same size containing logical (vector index)
    cum_corr_g2 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(nbchan),randi(2),:,2)),2);

    new_g1 = cum_corr_g1(:,:,mlabel_grp1');sample_size1 = size(new_g1,3);
    new_g2 = cum_corr_g2(:,:,mlabel_grp2');sample_size2 = size(new_g2,3);

    chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
    figure
    % (1) Correlation Group 1
    mean_corr_g1 = squeeze(mean(cum_corr_g1, 3));
    % (2) Correlation Group 2
    mean_corr_g2 = squeeze(mean(cum_corr_g2, 3));
    
    for m=1:3
        subplot(4,3,m)
        topoplot(mean_corr_g1(:,m), s.EEG.chanlocs)
        
        lower_bound = min(min(mean_corr_g1(:,m)),min(mean_corr_g2(:,m)));
        upper_bound = max(max(mean_corr_g1(:,m)),max(mean_corr_g2(:,m)));
        caxis([lower_bound, upper_bound]);
        
        h=colorbar('SouthOutside');
        set(h, 'Position', [.28*(m-1)+.21 .7 .065 .013]);
        title(bandTable(m),'FontSize', 8)

        subplot(4,3,3+m)
        topoplot(mean_corr_g2(:,m), s.EEG.chanlocs)
        caxis([lower_bound, upper_bound]);        
    end
    
    if sample_size1 == sample_size2
        dim = [.07 .64 .2 .18]; % x y w h
        str = {['S01 ', '(',num2str(sample_size1),')']};
        annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                        'String',str,'FitBoxToText','on');
        dim = [.07 .435 .2 .18]; % x y w h
        str = {['S02 ', '(',num2str(sample_size2),')']};
        annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                        'String',str,'FitBoxToText','on');
    else
        cprintf('err', '\n\nERROR: Wrong number of samples.\n');
    end
    
    % (3) Group 1 > Group 2
    [pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
        cum_corr_g1, cum_corr_g2, chan_hood,5000,.05,1,.05,2,[],1); 
    % 6th argument: 0-unequal, 1-greater
    index = zeros(nbchan, 3);
    t_orig_sig = NaN(nbchan, 3);
    for m=1:3
        index(:,m) = logical(pval(:,m)<est_alpha);
        t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
        subplot(4,3,6+m);
        topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
        caxis([0 5]);
        if m == 3
            h=colorbar('SouthOutside');
            set(h, 'Position', [.28*(m-1)+.21 .28 .063 .013]);
%             h.Label.String = 'T values';
        end
        title(bandTable(m),'FontSize', 8)
    end
    dim = [.06 .23 .2 .18]; % x y w h
    str = {'S01>S02'};
    annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                        'String',str,'FitBoxToText','on');

    % (4) Group 1 < Group 2
    [pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
        cum_corr_g2,cum_corr_g1,chan_hood,5000,.05,1,.05,2,[],1); 
    % 6th argument: 0-unequal, 1-greater
    index = zeros(nbchan, 3);
    t_orig_sig = NaN(nbchan, 3);
    for m=1:3
        index(:,m) = logical(pval(:,m)<est_alpha);
        t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
        subplot(4,3,9+m);
        topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
        caxis([0 5]);
    end
    dim = [.06 .033 .2 .18]; % x y w h
    str = {'S01<S02'};
    annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                        'String',str,'FitBoxToText','on');

    suptitle(['D', num2str(tmt, '%02d'),' Normalized Correlation'])
    saveas(gcf,[s.pathdb.figs, 'D', num2str(tmt, '%02d'), '_paired_nCorrelation_Jun.png']);
%     suptitle(['D', num2str(tmt, '%02d'),' Normalized Correlation (male)'])
%     saveas(gcf,[s.pathdb.figs, '[male]D', num2str(tmt, '%02d'), '_paired_nCorrelation_Rui.png']);
    close
    
end
%%

fxs=squeeze(mean(cum_corr_g1)); % 3*25 (male)
hc=squeeze(mean(cum_corr_g2)); % 3*26 

figure;
for i=1:3
    subplot(1,3,i)
    scatter(1*ones(size(fxs(i,:))), fxs(i,:), 8, 'LineWidth', .8, 'MarkerEdgeAlpha',.5)
    hold on
    scatter(2*ones(size(hc(i,:))), hc(i,:), 8, 'LineWidth', .8, 'MarkerEdgeAlpha',.5)
    hold off
    title(bandTable(i))
    xlim([0 3])
    xticks([1 2])
    xticklabels({'FXS','Control'})
    ylabel('Normalized correlation')
end
suptitle('Both')

data=NaN(3,2,46);
data(:,1,1:44)=fxs;
data(:,2,:)=hc;
figure
for i=1:3
    subplot(1,3,i);boxplot(squeeze(data(i,:,:))')
    xticks([1 2]);xticklabels({'FXS','Control'})
    title(bandTable(i))
end
suptitle('Both')


figure;
bar([mean(fxs,2) mean(hc,2)]);
ylabel('mean of correlations')
xticks([1 2 3])
xticklabels({'\theta  - \gamma', 'lower \alpha - \gamma', ...
            'upper \alpha - \gamma'})
title('Normalized correlation (Both)')
% [h,p,ci,stats] = ttest2(fxs(1,:),hc(1,:)); % pval=.0015
hold on;scatter(1,-.12,'*','k')
% [h,p,ci,stats] = ttest2(fxs(2,:),hc(2,:)); % pval=.0150
scatter(2,.06,'*','k')
% [h,p,ci,stats] = ttest2(fxs(3,:),hc(3,:)); % pval=.0010
scatter(3,.13,'*','k');hold off
legend('FXS','HC','significance','\alpha=.05')



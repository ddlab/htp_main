

% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (11/2018)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% For U54p1 data set
% setup environment
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

% vectorization of subject array
for i = 1:length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
                        
            % power analysis
            s.setFreqTable([1,3; 4,7; 8,12; 13,30; 30,57; 63,100]); % requires input
%             s.setFreqTable();
            s.getPntsTable;
            s.generateStoreRoom;
%             s.correlation_global_coupling;
%             s.correlation_global_coupling2;
%             s.bandAverageTrials;
%             s.generateNormalizedPowerSpectrum;
%             s.powerVisualization; % run under 1920*1440    
%             s.surfaceLaplacian;
            s.hilbert;
            s.generateConnectivity;
            % End Analysis methods
            
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end


%% Stage 6
% Amplitude coupling via rank correlation
nbgroups = 2;
gamma_grp = [65:66,68:76,82:84,89:90,94];
nbchan = length(gamma_grp);
cum_zcorr = NaN(nbchan, 4, length(sub), nbgroups); % theta through upper alpha
% 15 channels mamually 
g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        temp = s.rest_corr_rho;
        
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            % cumulative rel_power @ theta*lower gamma, alpha*lower gamma
            %                        theta*upper gamma, alpha*upper gamma
            cum_zcorr(:, :, i, gid) = temp'; 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_zcorr(:, :, i, gid) = temp';
            g2 = g2+1;
            
        end
    end
end

% regrouping
cum_corr_g1 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(nbchan),randi(4),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_corr_g2 = cum_zcorr(:,:,~isnan(cum_zcorr(randi(nbchan),randi(4),:,2)),2);

bandTable = ["\theta - lower \gamma", "\alpha - lower \gamma", ...
            "\theta - upper \gamma", "\alpha - upper \gamma"];
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
figure
% (1) Correlation Group 1
mean_corr_g1 = squeeze(mean(cum_corr_g1, 3));
recover_g1 = zeros(128,3);

for m=1:4
    recover_g1(gamma_grp, m) = mean_corr_g1(:, m);
    subplot(4,4,m)
    topoplot(recover_g1(:,m), s.EEG.chanlocs)
    caxis([-.4, .4]);
    colorbar;
    title(bandTable(m),'FontSize', 8)
end
% (2) Correlation Group 2
mean_corr_g2 = squeeze(mean(cum_corr_g2, 3));
recover_g2 = zeros(128,3);
for m=1:4
    recover_g2(gamma_grp, m) = mean_corr_g2(:, m);
    subplot(4,4,4+m)
    topoplot(recover_g2(:,m), s.EEG.chanlocs)
    caxis([-.4, .4]);
    colorbar;
    title(bandTable(m),'FontSize', 8)
end
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_corr_g1, cum_corr_g2, chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(nbchan, 3);
t_orig_sig = NaN(nbchan, 3);
recover_g3 = zeros(128,3);
for m=1:4
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    recover_g3(gamma_grp, m) = t_orig_sig(:, m);
    subplot(4,4,m+8);
    topoplot(recover_g3(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    colorbar;
    title('FXS > HC');
end

% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_corr_g2,cum_corr_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(nbchan, 3);
t_orig_sig = NaN(nbchan, 3);
recover_g4 = zeros(128,3);
for m=1:4
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    recover_g4(gamma_grp, m) = t_orig_sig(:, m);
    subplot(4,4,m+12);
    topoplot(recover_g4(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    colorbar;
    title('FXS < HC');
end

suptitle('Correlation')


fxs=squeeze(mean(cum_corr_g1)); % 4*44
hc=squeeze(mean(cum_corr_g2));

figure;
for i=1:4
    subplot(2,2,i)
    scatter(1*ones(size(fxs(i,:))), fxs(i,:), 8, 'LineWidth', .8, 'MarkerEdgeAlpha',.5)
    hold on
    scatter(2*ones(size(hc(i,:))), hc(i,:), 8, 'LineWidth', .8, 'MarkerEdgeAlpha',.5)
    hold off
    title(bandTable(i))
    xlim([0 3])
    xticks([1 2])
    xticklabels({'FXS','Control'})
    ylabel('Normalized correlation')
end

data=NaN(4,2,46);
data(:,1,1:44)=fxs;
data(:,2,:)=hc;
figure
for i=1:4
    subplot(2,2,i);boxplot(squeeze(data(i,:,:))')
    xticks([1 2]);xticklabels({'FXS','Control'})
    title(bandTable(i))
end


figure;
bar([mean(fxs,2) mean(hc,2)]);
ylabel('mean of correlations')
xticks([1 2 3 4])
xticklabels({'\theta  - lower \gamma', '\alpha - lower \gamma', ...
            '\theta - upper \gamma', '\alpha - upper \gamma'})
title('Region to electrodes correlation')
% [h,p,ci,stats] = ttest2(fxs(1,:),hc(1,:)); 
% [h,p,ci,stats] = ttest2(fxs(2,:),hc(2,:)); % pval=.0316
hold on;scatter(2,-.035,'*','k')
% [h,p,ci,stats] = ttest2(fxs(3,:),hc(3,:));
% [h,p,ci,stats] = ttest2(fxs(4,:),hc(4,:)); % pval=.0123
scatter(4,-.05,'*','k');hold off
legend('FXS','HC','significance','\alpha=.05')


%% gamma power difference region
nbgroups = 2;
cum_rel_pow = NaN(128, 6, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx_rel = s.rest_rel_power_band_average_trials;
        
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_rel_pow(:, :, i, gid) = pxx_rel'; 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_rel_pow(:, :, i, gid) = pxx_rel';
            g2 = g2+1;
            
        end
    end
end

% regrouping
cum_rel_power_g1 = cum_rel_pow(:,:,~isnan(...
    cum_rel_pow(randi(128),randi(6),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_rel_power_g2 = cum_rel_pow(:,:,~isnan(...
    cum_rel_pow(randi(128),randi(6),:,2)),2);

nbband = 6;
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index1 = zeros(s.EEG.nbchan, nbband);
for m = 1:nbband
    index1(:,m) = logical(pval(:,m)<est_alpha);
end

[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g2,cum_rel_power_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index2 = zeros(s.EEG.nbchan, nbband);
for m = 1:nbband
    index2(:,m) = logical(pval(:,m)<est_alpha);
end


%% Stage 6
% relative power
nbgroups = 2;
nbchan = s.EEG.nbchan;
nbband = 6;
cum_theta = NaN(128, nbband, length(sub), nbgroups); 
cum_norm_power_band = NaN(nbchan, nbband, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx_rel = s.rest_rel_power_band_average_trials;
        
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            % cumulative rel_power @ delta, theta, alpha, beta, gamma1,
            % gamma2
            cum_theta(:, :, i, gid) = pxx_rel'; 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_theta(:, :, i, gid) = pxx_rel';
            g2 = g2+1;
            
        end
    end
end

% regrouping
cum_rel_power_g1 = cum_theta(:,:,~isnan(...
    cum_theta(randi(nbchan),randi(nbband),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
cum_rel_power_g2 = cum_theta(:,:,~isnan(...
    cum_theta(randi(nbchan),randi(nbband),:,2)),2);


chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
bandTable = ["\delta", "\theta", "\alpha", "\beta", "lower \gamma", ...
                                                    "upper \gamma"];

figure('units','normalized','outerposition',[0 0 1 1]);
% (1) power Group 1
mean_power_g1 = mean(cum_rel_power_g1, 3);
for m = 1:nbband
    subplot(4,6,m)
    topoplot(mean_power_g1(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
    if m == 1
        caxis([.25, .5]);
    elseif m == 2
        caxis([.1, .24]);
    elseif m == 3
        caxis([.08, .22]);
    elseif m == 4
        caxis([.1, .2]);
    elseif m == 5
        caxis([.06, .18]);
    else
        caxis([0, .2]);
    end
    h=colorbar('SouthOutside');
    set(h, 'Position', [.135*(m-1)+.16 .73 .045 .015]);
    title(bandTable(m),'FontSize', 12)
end
dim = [.07 .64 .2 .18]; % x y w h
str = {['FXS (',num2str(g1),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (2) power Group 2
mean_power_g2 = mean(cum_rel_power_g2, 3);
for m = 1:nbband
    subplot(4,6,m+6)
    topoplot(mean_power_g2(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
    if m == 1
            caxis([.25, .5]);
        elseif m == 2
            caxis([.1, .24]);
        elseif m == 3
            caxis([.08, .22]);
        elseif m == 4
            caxis([.1, .2]);
        elseif m == 5
            caxis([.06, .18]);
        else
            caxis([0, .2]);
    end
%     h=colorbar('SouthOutside');
%     set(h, 'Position', [.135*(m-1)+.16 .52 .045 .015]);
    
end
dim = [.07 .44 .2 .18]; % x y w h
str = {['Control (',num2str(g2),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+12);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    if m == 6
        h=colorbar('SouthOutside');
        set(h, 'Position', [.135*(6-1)+.16 .31 .045 .015]);
        h.Label.String = 'T values';
%         h.Label.FontSize = 10;
    end
end
dim = [.05 .24 .2 .18]; % x y w h
str = {'FXS > Control'};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g2,cum_rel_power_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+18);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
%     colorbar
end
dim = [.05 .04 .2 .18]; % x y w h
str = {'FXS < Control'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);

axes( 'Position', [0, 0.95, 1, 0.05] ) ;
set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
text( 0.5, 0, 'Relative Power', 'FontSize', 14', 'FontWeight', 'Bold', ...
  'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;


%% Group alpha peak inspection
cum_rel_power = NaN(161,length(sub),2);
% cum_norm_power = NaN(161,length(sub),2);

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx_rel = s.rest_rel_power;
%         pxx_norm = s.rest_abs_norm_average_trials;
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel, 2)),2); 
%             cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel(:,:,...
%                 [65, 70:71, 73, 75:77, 81:85, 90:91, 94]), 2)),2); 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
%             cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel, 2)),2);
            cum_rel_power(:, i, gid) = mean(squeeze(mean(pxx_rel(:,:,...
                [65, 70:71, 73, 75:77, 81:85, 90:91, 94]), 2)),2);
            g2 = g2+1;
            
        end
    end
end
cum_rel_power_g1 = cum_rel_power(:,~isnan(cum_rel_power(randi(161),:,1)),1);
cum_rel_power_g2 = cum_rel_power(:,~isnan(cum_rel_power(randi(161),:,2)),2);
upper = 80;
grp1 = mean(cum_rel_power_g1, 2);
grp2 = mean(cum_rel_power_g2, 2);
plot(0:0.5:upper, linspace(0,upper,161)'.*grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
plot(0:0.5:upper, linspace(0,upper,161)'.*grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
legend({['Fragile X (',num2str(g1),')'],['Control (',num2str(g2),')']})
xlabel('Frequency (Hz)')
ylabel('F.* Relative Power')
% ylabel('Relative Power')

% data for alpha peak shift examination
% cum_norm_power_g1 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,1)),1);
% cum_norm_power_g2 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,2)),2);
% 
% % to be extended to GUI
% upper = 80;
% grp1 = mean(cum_norm_power_g1, 2);
% grp2 = mean(cum_norm_power_g2, 2);
% plot(0:0.5:upper, grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
% plot(0:0.5:upper, grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
% legend({['group1 (',num2str(g1),')'],['group2 (',num2str(g2),')']})
% xlabel('Frequency (Hz)')
% ylabel('Normalized Power')



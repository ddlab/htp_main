
% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 1 Import Raw Files
% FILE: htp_stage1.m
%
% NOTES: Placed RAW files in S00_RAW in Subfolders

% Overview

% PREPROCESSING
% htp_stage1 - raw import, channel assignment, filter, resample
% htp_stage2 - epoching; manual channel, artifact, and epoch cleaning
% htp_stage3 - ICA (parallel binary)
% htp_stage4 - non-neural component identification

% ANALYSIS
% htp_stage5 - individual analysis (level1)
% htp_stage6 - group level analysis

% please set the number of channels used by your data files

% setup environment
cprintf('blue', '\nStarting Stage 1: Raw Data Import and Preprocessing...\n');
clear all; close all;

global current_stage, current_stage = 1;

% ------ STAGE 1 CONFIGURATION -----

s1.cfg.chans       =     'EGI128';  % currently EGI32 or EGI128
s1.cfg.lowcutoff   =     1;
s1.cfg.highcutoff  =     120;
s1.cfg.notch       =     [57 63 3300];  % band limits (low high) and filter order
s1.cfg.parfor      =     0; % turn on parallel processing (requires toolbox) 0 = off, 1 = on
s1.cfg.resample    =     256;
% -----------------------------------

% assign proper channel configuration (used all the way through stage 6)
switch s1.cfg.chans  
    case 'EGI32'
        s1.cfg.chan_number =    32; s1.cfg.chan_config =    'egi32'; % egi32 or egi128
    case 'EGI128'
        s1.cfg.chan_number =    128; s1.cfg.chan_config =    'egi128'; % egi32 or egi128
end

try
    htp_config;
    clearWarnings;
catch, disp('Error in Configuration Files');
end

if current_stage == 0, return; end

numberofchannels = s1.cfg.chan_number;

% Setup Environment
configObject = RestEegDataClass();
configObject.createPaths( basePath );
pathdb = configObject.pathdb;
filter = configObject.net_filter;

% Generate RAW data list
dirlist = getFileList( filter, pathdb.raw );
fnlist = dirlist.full';
subfolderlist = dirlist.subfolder';

% separate eventdata files - MEA specific
file_eventIdx = contains(fnlist,'dig.edf');
file_dataIdx = ~contains(fnlist,'dig.edf');

event_subfolderlist = subfolderlist(file_eventIdx);
event_fnlist = fnlist(file_eventIdx);
subfolderlist = subfolderlist(file_dataIdx);

if isempty(subfolderlist{1})
    cprintf('err', '\n\nERROR: Please place files in subfolders and restart.\n');
    return;
end

fnlist = fnlist(file_dataIdx);

% Create Data Objects Array
for i = 1:length(fnlist), sub(i) = RestEegDataClass(); end

for i = 1:length(fnlist), sub(i).updatePaths( basePath ); end

for i = 1:length(fnlist), sub(i).assignRawFile( subfolderlist{i}, fnlist{i} ); end

for i = 1:length(fnlist), sub(i).setUser( user ); end

for i = 1:length(fnlist), sub(i).changeStudyTitle( title_string ); end

for i = 1:length(fnlist), sub(i).createFileNames; end

for i = 1:length(fnlist), sub(i).setTotalChannelNumber( s1.cfg.chan_number ); end

for i = 1:length(fnlist), sub(i).setRecordingSystem( s1.cfg.chan_config ); end

for i = 1:length(fnlist), sub(i).getRawData; end

% set resample rate
for i = 1:length(fnlist), sub(i).setResampleRate( s1.cfg.resample ); end
tic
if s1.cfg.parfor == 1 % please note the assignment to 'sub' for pool to process correctly
    
    parfor i = 1:length(fnlist), s = sub(i); s.filtData; sub(i) = s; end
    parfor i = 1:length(fnlist), s = sub(i); s.resampleData;  sub(i) = s; end
    parfor i = 1:length(fnlist), s = sub(i); s.averageRefData;  sub(i) = s; end
    
else
    
    %for i = 1:length(fnlist), sub(i).filtData; end
    for i = 1:length(fnlist), sub(i).filt_highpass( s1.cfg.lowcutoff ); end
    for i = 1:length(fnlist), sub(i).filt_lowpass( s1.cfg.highcutoff ); end
    for i = 1:length(fnlist), sub(i).filt_notch( s1.cfg.notch(1), s1.cfg.notch(2), s1.cfg.notch(3) ); end
    
    
    % for i = 1:length(fnlist), sub(i).filt_for_ica([2 80]); end  %
    % alternative per Makato
    
    for i = 1:length(fnlist), sub(i).resampleData; end
    for i = 1:length(fnlist), sub(i).averageRefData; end
    
end
toc
for i = 1:length(fnlist), sub(i).storeDataset( sub(i).EEG, pathdb.import, sub(i).subj_subfolder, sub(i).filename.import ); end
for i = 1:length(fnlist), sub(i).unloadDataset; end
for i = 1:length(fnlist), sub(i).outputRow('import'); end

if strcmp(fnlist, 'No File')
    cprintf('err', '\n\nERROR: Missing files. Check folder and subfolders.\n');
    return;
end

fname = createResultsCsv(sub, 'import');


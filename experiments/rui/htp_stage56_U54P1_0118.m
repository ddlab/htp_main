% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (11/2018)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES:
% For U54p1 data set
% Power, correlation analysis
% ref: connectivity 5 subjects took 894 sec

% Stage File Initiation
clear all; close all;
global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0;
%[xls,txt,~] = xlsread('C:\data\U54P1\A00_ANALYSIS\U54_p1_Sex.xlsx');
tic
% vectorization of subject array
for i = 1:1 %length(sub)
    
    s = sub(i);
    
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
        
        % load postcomps dataset and place in EEG subject structure
        s.loadDataset( 'postcomps' );
        
        % dynamic component removal using CSV file
        s.compRemove;
        %             s.rest_subj_gender = txt(xls==str2double(strtok(s.subj_basename,...
        %             {'D','_'})));
        % power analysis
        %             s.setFreqTable([1,3; 4,7; 8,12; 13,30; 30,57; 63,100]); %
        %             change bandTable accordingly
        s.setFreqTable();
        s.getPntsTable;
        %             s.generateStoreRoom;
        %             s.correlation_global_coupling;
        %             s.correlation_global_coupling2;
        %             s.bandAverageTrials;
        %             s.generateNormalizedPowerSpectrum;
        %             s.powerVisualization; % run under 1920*1440
        %             s.surfaceLaplacian;
        if size(s.EEG.data,3)>=15
            tic;
            s.hilbert;
            
            s.generateConnectivity;
            
            toc;
        end
        % End Analysis methods
        
        s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
        
        if ismember(i, objStageStatus_completed)
            s.proc_state = 'level1';
            s.outputRow(stage_next);
            prev_files = prev_files + 1;
        else
            s.outputRow('error');
            skip_files = skip_files + 1;
        end
    end
    s.unloadDataset; % clear s.EEG
    disp(i)
    sub(i) = s;
    GetSize(s);
    i
end
toc


%% Stage 6
% Phase-based connectivity
nbgroups = 2;
nbchan = s.EEG.nbchan;
cum_dbwpli_band = NaN(128, 128, 6, length(sub), nbgroups);
g1=0; g2=0;

for i = 1:length(sub)
    
    s = sub(i);
    
    if ismember(i, objStageStatus)
        if ~isempty(s.rest_dbwpli) % 15-trial threshold purpose
            dbwpli = s.rest_dbwpli;
            % chan*chan*band
            if strcmp(s.subj_id, 'Group1')
                gid = 1;
                cum_dbwpli_band(:, :, :, i, gid) = dbwpli;
                g1 = g1+1;
                
            elseif strcmp(s.subj_id, 'Group2')
                gid = 2;
                cum_dbwpli_band(:, :, :, i, gid) = dbwpli;
                g2 = g2+1;
            end
        end
    end
    
end

% regrouping
cum_dbwpli_band_g1 = cum_dbwpli_band(:,:,:, ~isnan(cum_dbwpli_band(1,128,randi(6),:,1)),1);
cum_dbwpli_band_g2 = cum_dbwpli_band(:,:,:, ~isnan(cum_dbwpli_band(1,128,randi(6),:,2)),2);

bandTable = ["\delta", "\theta", "lower \alpha", "upper \alpha", "\beta", "\gamma"];
nbband = size(s.freqTable,1);nbband=6;
%% dbWPLI in Adjacency matrix

mean_dbwpli_g1 = squeeze(mean(cum_dbwpli_band_g1, 4)); % average over subjects
mean_dbwpli_g2 = squeeze(mean(cum_dbwpli_band_g2, 4));

figure('units','normalized','outerposition',[0 0 1 1]);
for i = 1:nbband
    subplot(2,6,i);
    imagesc(triu(mean_dbwpli_g1(:,:,i))+tril(mean_dbwpli_g1(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
    
    subplot(2,6,i+6);
    imagesc(triu(mean_dbwpli_g2(:,:,i))+tril(mean_dbwpli_g2(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end
dim = [.05 .64 .2 .18]; % x y w h
str = {['FXS (',num2str(g1),')']};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
    'String',str,'FitBoxToText','on');
dim = [.05 .12 .2 .18]; % x y w h
str = {['Control (',num2str(g2),')']};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
    'String',str,'FitBoxToText','on');
suptitle('dbWPLI')


%% dbWPLI vs Distance
distance_mat = NaN(128,128);
for i=1:(nbchan-1)
    for j=(i+1):nbchan
        distance_mat(i,j) = sqrt(((s.EEG.chanlocs(i).X-s.EEG.chanlocs(j).X)^2 ...
            +(s.EEG.chanlocs(i).Y-s.EEG.chanlocs(j).Y)^2 ...
            +(s.EEG.chanlocs(i).Z-s.EEG.chanlocs(j).Z)^2));
    end
end
rel_dist_mat = distance_mat/max(distance_mat(:));

figure('units','normalized','outerposition',[0 0 1 1]);
for m=1:nbband
    mean_dbwpli_band_g1 = mean_dbwpli_g1(:,:,m);
    mean_dbwpli_band_g2 = mean_dbwpli_g2(:,:,m);
    
    subplot(2,nbband,m)
    scatter(rel_dist_mat(:), mean_dbwpli_band_g1(:),10,'b')
    hold on
    P1 = polyfit(rel_dist_mat(~isnan(rel_dist_mat)), ...
        mean_dbwpli_band_g1(~isnan(mean_dbwpli_band_g1)),1);
    y1 = P1(1)*rel_dist_mat(~isnan(rel_dist_mat))+P1(2);
    plot(rel_dist_mat(~isnan(rel_dist_mat)), y1, 'k')
    hold off
    xlabel('relative distance')
    ylabel('dbWPLI')
    legend(['slope = ',num2str(P1(1))])
    title(bandTable(m))
    
    subplot(2,nbband,m+6)
    scatter(rel_dist_mat(:), mean_dbwpli_band_g2(:),10,'r')
    hold on
    P2 = polyfit(rel_dist_mat(~isnan(rel_dist_mat)), ...
        mean_dbwpli_band_g2(~isnan(mean_dbwpli_band_g2)),1);
    y2 = P2(1)*rel_dist_mat(~isnan(rel_dist_mat))+P2(2);
    plot(rel_dist_mat(~isnan(rel_dist_mat)), y2, 'k')
    hold off
    xlabel('relative distance')
    ylabel('dbWPLI')
    legend(['slope = ',num2str(P2(1))])
    title(bandTable(m))
end
dim = [.05 .54 .2 .18]; % x y w h
str = {['FXS (',num2str(g1),')']};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
    'String',str,'FitBoxToText','on');
dim = [.05 .12 .2 .18]; % x y w h
str = {['Control (',num2str(g2),')']};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
    'String',str,'FitBoxToText','on');
suptitle('average per group')


%% connection pattern
figure('units','normalized','outerposition',[0 0 1 1]);
ds.connectStrengthLimits=[0 1];
percentile = [49.5 50.5];

for m=1:nbband
    
    % upper triangle to vector
    oneband = mean_dbwpli_g1(:,:,m);
    bandvec = oneband(~isnan(oneband)); % a
    %     hist(bandvec)
    lower_bound = prctile(bandvec, percentile(1));
    upper_bound = prctile(bandvec, percentile(2));
    ind = bandvec >= lower_bound & bandvec <= upper_bound; % adjMat
    %     ds.connectStrength = bandvec(ind); % values maybe less important than
    %     the conectivity pattern
    
    % strength reflects the relative distance
    distance = rel_dist_mat(~isnan(rel_dist_mat));
    ds.connectStrength = distance(ind);
    
    % connectivity pattern
    CI= zeros(size(ds.connectStrength));
    RI= zeros(size(ds.connectStrength));
    index = 1;
    for ci = 2:nbchan
        for ri = 1:(ci-1)
            CI(index)=ci;
            RI(index)=ri;
            index=index+1;
        end
    end
    ds.chanPairs = [RI(ind) CI(ind)];
    
    subplot(2,nbband,m)
    colormap(flipud(hot));
    topoplot_connect_r(ds, s.EEG.chanlocs,.25);
    %     topoplot_connect(ds, s.EEG.chanlocs);
    %     cb = colorbar;
    %     set(cb, 'Position', [0.8 0.2 0.3 0.02])
    %     cb.Label.String = 'relative distance';
    title(bandTable(m))
    
    
    oneband = mean_dbwpli_g2(:,:,m);
    bandvec = oneband(~isnan(oneband)); % a
    %     hist(bandvec)
    lower_bound = prctile(bandvec, percentile(1));
    upper_bound = prctile(bandvec, percentile(2));
    ind = bandvec >= lower_bound & bandvec <= upper_bound; % adjMat
    %     ds.connectStrength = bandvec(ind); % values maybe less important than
    %     the conectivity pattern
    
    % strength reflects the relative distance
    distance = rel_dist_mat(~isnan(rel_dist_mat));
    ds.connectStrength = distance(ind);
    
    % connectivity pattern
    CI= zeros(size(ds.connectStrength));
    RI= zeros(size(ds.connectStrength));
    index = 1;
    for ci = 2:nbchan
        for ri = 1:(ci-1)
            CI(index)=ci;
            RI(index)=ri;
            index=index+1;
        end
    end
    ds.chanPairs = [RI(ind) CI(ind)];
    
    subplot(2,nbband,nbband+m)
    colormap(flipud(hot));
    topoplot_connect_r(ds, s.EEG.chanlocs,.25);
    %     topoplot_connect(ds, s.EEG.chanlocs);
    if m == 6
        cb = colorbar('SouthOutside');
        set(cb, 'Position', [.135*(m-1)+.16 .51 .045 .015]);
        cb.Label.String = 'relative distance';
    end
    title(bandTable(m))
end

dim = [.07 .62 .2 .18]; % x y w h
str = {['Group 1 ', '(',num2str(g1),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
    'String',str,'FitBoxToText','on', 'FontSize', 12);
dim = [.07 .14 .2 .18]; % x y w h
str = {['Group 2 ', '(',num2str(g2),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
    'String',str,'FitBoxToText','on', 'FontSize', 12);


%% Significance

mat1 = NaN(nbchan*(nbchan-1)/2,nbband,g1);
mat2 = NaN(nbchan*(nbchan-1)/2,nbband,g2);
for m=1:nbband
    cum_dbwpli_g1 = squeeze(cum_dbwpli_band_g1(:,:,m,:)); % upper triangle
    cum_dbwpli_g2 = squeeze(cum_dbwpli_band_g2(:,:,m,:)); % nbchan*nbchan*nsubj
    
    for i = 1:g1
        temp = cum_dbwpli_g1(:,:,i);
        mat1(:,m,i) = temp(~isnan(temp));
    end
    for i = 1:g2
        temp = cum_dbwpli_g2(:,:,i);
        mat2(:,m,i) = temp(~isnan(temp));
    end
end
% Jun's paper
diff = mat1(:,:,21) - mat2;
[pval, t_orig, tmx_ptile, seed_state, est_alpha] = mxt_perm1(...
    diff, 5000,.05,0,2,[],0); % null hypothesis: diff=0
% end
% 1st implementation
[pval, t_orig, tmx_ptile, seed_state, est_alpha] = mxt_perm2(...
    mat1, mat2, 5000,.05,1,2,[],0);
index = logical(pval<est_alpha); % 128*127/2*6
figure('units','normalized','outerposition',[0 0 1 1]);
for m=1:nbband
    subplot(2,6,m)
    imagesc(triu(t_orig(index))+tril(t_orig(index)'));
    title([num2str(bandTable(m)),' band']);
end
[pval, t_orig, tmx_ptile, seed_state, est_alpha] = mxt_perm2(...
    mat2, mat1, 5000,.05,1,2,[],0);
index = logical(pval<est_alpha);
for m=1:nbband
    subplot(2,6,m+6)
    
    title([num2str(bandTable(m)),' band']);
end



% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (01/2019)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% P2 dataset Power Analysis
% ~515 seconds to run Stage 5
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 
[xls0,txt,~] = xlsread([basePath,'\A00_ANALYSIS\[update]EEG_Code_U54_DeID.xlsx']);
% vectorization of subject array
tic
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
            s.rest_subj_gender = txt(xls0 == str2double(strtok(s.subj_basename,...
            {'D','_'})), 2);      
            s.regroup;  
            
            % power analysis
%             s.setFreqTable([1,3; 4,7; 8,12; 13,30; 30,57; 63,100]); % split gamma
            s.setFreqTable(); % split alpha
            s.getPntsTable;
            s.generateStoreRoom;
            s.generateStoreRoom3;
            s.bandAverageTrials;
%             s.generateNormalizedPowerSpectrum;
%             s.powerVisualization; % run under 1920*1440
            % End Analysis methods
            s.unloadDataset;
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end
toc

%% Stage 6
% Power analysis
% gender adjustment in line 113, 240-241
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
nbband = 6;
bandTable = ["\delta", "\theta", "lower \alpha", ...
    "upper \alpha", "\beta", "\gamma"];
% bandTable = ["\delta", "\theta", "\alpha", ...
%             "\beta", "lower \gamma","upper \gamma"];
nbpnts = 2*s.freqTable(6,2)+1;
% between subjects
for tmt=1:4 % treatment 
    % need manually adjust color scale in each treatment
    nbgroups = 2;
    [xls,~,~] = xlsread([s.pathdb.analysis,'\D',num2str(tmt,'%02d'),'.xlsx']);
    % extract numeric as the true matching criteria

    cum_rel_power_band = NaN(s.EEG.nbchan, 6, length(sub), nbgroups); 
    % alpha peak inspection
    cum_rel_power = NaN(nbpnts, length(sub), nbgroups); 
    flabel = NaN(size(xls,1), nbgroups); % fake label
    g1=0; g2=0;
    for i = 1: length(sub)

        s = sub(i);

        if ismember(i, objStageStatus)
            if strcmp(s.rest_subj_gender, 'Female')
                pxx_band = s.rest_rel_power_band_average_trials;
                pxx = s.rest_rel_power; % alpha peak
    %             inspection
                % frequencies*trials*chans
                if strcmp(s.subj_field, ['S01D', num2str(tmt, '%02d')])
                    gid = 1;
                    % cumulative rel_power @ delta, theta, alpha1, alpha2, beta, gamma
                    cum_rel_power(:, i, gid) = mean(squeeze(mean(s.rest_rel_power, 2)),2); 
                    cum_rel_power_band(:, :, i, gid) = pxx_band';
                    g1 = g1+1;
                    flabel(g1, gid) = str2double(strtok(s.subj_basename, {'D','_'}));

                elseif strcmp(s.subj_field, ['S02D', num2str(tmt, '%02d')])
                    gid = 2;
                    cum_rel_power(:, i, gid) = mean(squeeze(mean(s.rest_rel_power, 2)),2);
                    cum_rel_power_band(:, :, i, gid) = pxx_band';
                    g2 = g2+1;
                    flabel(g2, gid) = str2double(strtok(s.subj_basename, {'D','_'}));
                end
            end
        end
    end

    [C1,~,mlabel_grp1] = intersect(xls(:,1),flabel(:,1),'stable'); % matching label
    [C2,~,mlabel_grp2] = intersect(xls(:,3),flabel(:,2),'stable');
%     [C1 C2]

    % separate group 
    cum_rel_power_g1 = cum_rel_power_band(:,:,~isnan(...
        cum_rel_power_band(randi(128),randi(6),:,1)),1); 
    % isnan returns an array the same size containing logical (vector index)
    cum_rel_power_g2 = cum_rel_power_band(:,:,~isnan(...
        cum_rel_power_band(randi(128),randi(6),:,2)),2);
    
%     % alpha peak inspection
%     grp1 = squeeze(mean(cum_rel_power(:, ~isnan(cum_rel_power(randi(161), :, 1)), 1), 2)); 
%     grp2 = squeeze(mean(cum_rel_power(:, ~isnan(cum_rel_power(randi(161), :, 2)), 2), 2));
%     save(['resting_alpha_peak_shift_P2_2019_D',num2str(tmt, '%02d')], 'grp1', 'grp2')

    new_g1 = cum_rel_power_g1(:,:,mlabel_grp1');sample_size1 = size(new_g1,3);
    new_g2 = cum_rel_power_g2(:,:,mlabel_grp2');sample_size2 = size(new_g2,3);

    
    % dependent group multiple comparison
   
    figure('units','normalized','outerposition',[0 0 1 1]);
    % (1) relative power Group 1
    mean_rel_power_g1 = mean(new_g1, 3);
    % (2) relative power Group 2
    mean_rel_power_g2 = mean(new_g2, 3);
    
    for m = 1:nbband
        subplot(4,6,m)
        topoplot(mean_rel_power_g1(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                    'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
        lower_bound = min(min(mean_rel_power_g1(:,m)),min(mean_rel_power_g2(:,m)));
        upper_bound = max(max(mean_rel_power_g1(:,m)),max(mean_rel_power_g2(:,m)));
        caxis([lower_bound, upper_bound]);

        h=colorbar('SouthOutside');
        set(h, 'Position', [.135*(m-1)+.16 .71 .045 .015]);
        title(bandTable(m),'FontSize', 12)
    
        subplot(4,6,m+6)
        topoplot(mean_rel_power_g2(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                    'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
        caxis([lower_bound, upper_bound]);
%         h=colorbar('SouthOutside');
%         set(h, 'Position', [.135*(m-1)+.16 .5 .045 .015]);
    end
    
    dim = [.07 .62 .2 .18]; % x y w h
    str = {['S01 ', '(',num2str(sample_size1),')']};
    annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                    'String',str,'FitBoxToText','on', 'FontSize', 12);
    dim = [.07 .42 .2 .18]; % x y w h
    str = {['S02 ', '(',num2str(sample_size2),')']};
    annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                    'String',str,'FitBoxToText','on', 'FontSize', 12);
    % (3) Group 1 > Group 2
    [pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_dependent(...
        new_g1, new_g2,chan_hood,5000,.05,1,.05,2,[],1); 
    % 6th argument: 0-unequal, 1-greater
    index = zeros(s.EEG.nbchan, nbband);
    t_orig_sig = NaN(s.EEG.nbchan, nbband);
    for m = 1:nbband
        index(:,m) = logical(pval(:,m)<est_alpha);
        t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
        subplot(4,6,m+12);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
        caxis([0 5]);
        if m == 6
            h=colorbar('SouthOutside');
            set(h, 'Position', [.135*(6-1)+.16 .3 .045 .015]);
            h.Label.String = 'T values';
    %         h.Label.FontSize = 10;
        end
    end
    dim = [.065 .24 .2 .18]; % x y w h
    str = {'S01 > S02'};
    annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                    'String',str,'FitBoxToText','on', 'FontSize', 12);
    % (4) Group 1 < Group 2
    [pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_dependent(...
        new_g2, new_g1, chan_hood,5000,.05,1,.05,2,[],1); 
    % 6th argument: 0-unequal, 1-greater
    index = zeros(s.EEG.nbchan, nbband);
    t_orig_sig = NaN(s.EEG.nbchan, nbband);
    for m = 1:nbband
        index(:,m) = logical(pval(:,m)<est_alpha);
        t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
        subplot(4,6,m+18);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
        caxis([0 5]);
    %     colorbar
    end
    dim = [.065 .03 .2 .18]; % x y w h
    str = {'S01 < S02'};
    annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                    'String',str,'FitBoxToText','on', 'FontSize', 12);

%     axes( 'Position', [0, 0.95, 1, 0.05] ) ;
%     set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
%     text( 0.5, 0, 'Normalized Power', 'FontSize', 14', 'FontWeight', 'Bold', ...
%       'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;
  
%     suptitle(['S01D', num2str(tmt, '%02d'), ' paired S02D', num2str(tmt, '%02d'),])
%     saveas(gcf,[s.pathdb.figs, 'D', num2str(tmt, '%02d'), '_paired_relative_power.png']);
    suptitle(['S01D', num2str(tmt, '%02d'), ' paired S02D', num2str(tmt, '%02d'), ' (female)'])
    saveas(gcf,[s.pathdb.figs, '[female]D', num2str(tmt, '%02d'), '_paired_relative_power.png']);
    close

end
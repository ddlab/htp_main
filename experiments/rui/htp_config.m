

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage Configuration
% FILE: htp_config.m
%
% NOTES:
% for binica to correctly work install MKL (Intel Math Libraries) and add
% to system path: C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.5.274\windows\mkl


v = version; agp = @(x) addpath(genpath(x)); hostname = getHostName(); basePath = 'Error_Undefined';
global current_stage; global user;

% options
parallel_processing_on = 0;
chan_clean_mode = 'manual';

% data directories

switch hostname
    
    case 'DESKTOP-91N7ALB'
        basePath = 'C:\data\U54P1';
%         basePath = 'C:\data\ACNPEEG';
%         basePath = 'C:\data\P2 2019';
%         basePath = 'C:\data\SAT';
%         basePath = 'C:\data\K23';
%         basePath = 'C:\data\K23b';
%         basePath = 'C:\data\Minocycline Grant Data';
%         basePath = 'C:\data\SAT\Controls\32lead';
%         basePath = 'C:\data\SAT\Controls\128lead';
        
    case 'EA19-00124'
       % addpath('/Users/ped8zl/code/eeglab14_1_2b/');
        basePath = '/Users/ped8zl/data/sattest/';
        basePath = '/Users/ped8zl/data/binicatest/';
        
    case 'OFFICE8700'
        addpath('C:\code\eeglab14_1_2b');
        basePath = 'F:/satdata32/';
                
    case 'Desktop'
        parallel_processing_on = 0;
        %chan_clean_mode = 'asr';
        addpath('C:\EEG\eeglab');
        %basePath = 'E:\T32RestData';
        %basePath = 'E:\testdata32';
        %basePath = 'E:\sattest2';
        %basePath = 'D:\Datasets\SAT';
        basePath = 'E:\demo128';
        %basePath = 'E:\demo32';
        basePath = 'E:\K23_112018';
        basePath = 'E:\ACNPEEG';
end

% if strcmp( hostname, 'DESKTOP-91N7ALB'), basePath = 'C:/data/seconddataset/'; end
%     if strcmp( hostname, 'EA19-00124'), basePath = '/Users/ped8zl/data/sattest/'; end
%if strcmp( hostname, 'P15-5188'), basePath = 'C:\Data\SATdata\'; end
%if strcmp( hostname, 'OFFICE8700'), basePath = 'F:/satdata32/'; end
%if strcmp( hostname, 'OFFICE8700'), basePath = 'F:/satdata128/'; end
%if strcmp( hostname, 'P15-5188'), basePath = 'C:/Data/SATdata'; end
%if strcmp( hostname, 'P15-5187'), basePath = 'C:/erpHuman/chirp_test/'; end

% study title (can modify)
title_string = 'DemoData';


if strcmp(basePath, 'Error_Undefined')
    current_stage = 0;
    cprintf('err','\nERROR: Hostname is incorrect or not defined.\nCurrent host is: %s\nPlease edit %s file.\n\n', hostname, mfilename);
    return;
end

if current_stage < 5
    cprintf('*black','\n\nStarting High Throughput Pipeline v1.0 ...\n');
    cprintf('blue','\nCONFIRM CORRECT DATA DIRECTORY\nCurrent Data Directory: %s\n\n', basePath);
    
    %get User String
    user = getUserInitials;
    
    
    if strcmp(user, 'Not Entered')
        cprintf('err','\nERROR: No initials entered. Please run script again.\n\n');
        current_stage = 0;
        
    end
    
    %pause;
end

clear sub;

warning( 'off', 'MATLAB:MKDIR:DirectoryExists' );
warning( 'off', 'MATLAB:xlswrite:AddSheet' ) ;

if exist('ALLCOM')
    
else
    eeglab;
    close gcf;
end

timetag2    = datestr(now,'yymmddHHMM');

%Sets the units of your root object (screen) to pixels
set(0,'units','pixels'); 
%Obtains this pixel information
screensize = get(0,'screensize');

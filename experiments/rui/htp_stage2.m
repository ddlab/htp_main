

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 2 Manual Channel, Artifact, and Epoch Cleaning
% FILE: htp_stage2.m
%
% NOTES:

% Overview
% htp_stage1 - raw import, channel assignment, filter, resample
% htp_stage2 - epoching; manual channel, artifact, and epoch cleaning
% htp_stage3 - ICA (parallel binary)
% htp_stage4 - non-neural component identification
% htp_stage5 - individual analysis (level1)
% htp_stage6 - group level analysis

% setup environment
clear all; close all;

% ------ STAGE 2 CONFIGURATION -----
s2.cfg.cont_EpochLength         = 2;
s2.cfg.cont_EpochLimits     = [-1 1];
s2.cfg.chan_clean_mode          = 'manual'; % or asr
% ----------------------------------

global current_stage, current_stage = 2; global user;
cprintf('blue','\nStarting Stage 2: Manual Cleaning and Pre-ICA.');
try
    htp_config;
    clearWarnings;
catch
    disp('Error in Configuration Files');
end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'import'; stage_next = 'preica';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');
cprintf('*blue','\nManual Cleaning Operations\n');

prev_files = 0; skip_files = 0; errorchk = 0;

for i = 1:length(sub)
    
    sub(i).setUser( user );
    
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
        
        
        str_status = sprintf('\nOpening dataset (%d of %d): %s', i, length(sub), sub(i).subj_basename);
        sub(i).str_plottitle = str_status;
        
        cprintf('blue', str_status);
        cprintf('blue','\nFolder: %s\n', sub(i).subj_subfolder);
        
        % load dataset
        sub(i).openDataset(pathdb.import, sub(i).subj_subfolder, sub(i).filename.import);
        
        % ============= MANUAL CHANNEL CLEANING ======================
        
        switch s2.cfg.chan_clean_mode
            case 'manual'
                sub(i).manualChanClean;
                sub(i).removeInterpolateChans;
            case 'asr'
                sub(i).cleanData;
                sub(i).removeInterpolateChans;
        end
        
        cprintf('blue','\nManual Bad Channel Selection: ');
        cprintf('blue','\nBad Channel IDs Removed (blank = none): %s\n', num2str(sub(i).proc_badchans));
        
        cprintf('blue','True Data Rank: %d\n', sub(i).EEG.etc.dataRank);
        
        sub(i).proc_ipchans = length(sub(i).proc_badchans);
        sub(i).proc_badchans = ['['  num2str(sub(i).proc_badchans) ']'];
        sub(i).proc_dataRank = sub(i).EEG.etc.dataRank;
        
        
        
        % ============= MANUAL CONTINUOUS ARTIFACT CLEANING ==========
        
        cprintf('blue','\nManual Continuous Data Rejection: ');
        
        sub(i).manualContClean;
        
        cprintf('blue','\nSegments removed: ');
        cprintf('blue','Data Length: Pre Post', sub(i).EEG.etc.dataRank);
        
        
        % ==================== CREATE EPOCHS ======================
        
        % this will create regular epochs in the EEG_preica structure
        sub(i).proc_contEpochLength = s2.cfg.cont_EpochLength;
        sub(i).proc_contEpochLimits = s2.cfg.cont_EpochLimits; 
        sub(i).createEpochs;
       
        % ==================== CLEAN EPOCHS ==========================
        
        sub(i).cleanEpochs;
        
        % ============================================================
        
        
        % save cleaned dataset into the preica directory
        sub(i).storeDataset( sub(i).EEG_preica, pathdb.preica, sub(i).subj_subfolder, sub(i).filename.preica);
        
        % unload data & decrease memory footprint
        sub(i).unloadDataset;
        
        if errorchk == 0
            sub(i).outputRow('preica');
            
        else
            sub(i).outputRow('error');
        end
        
    else  % if object is not at correct stage, push through only as saved
        if ismember(i, objStageStatus_completed)
            % unload data & decrease memory footprint
            sub(i).unloadDataset;
            sub(i).outputRow(stage_next);
            prev_files = prev_files + 1;
        else
            % unload data & decrease memory footprint
            sub(i).unloadDataset;
            sub(i).outputRow('error');
            skip_files = skip_files + 1;
        end
    end
    
end
cprintf('*blue','\nSummary', length(objStageStatus));
cprintf('*green','\nFiles Processed: %d\n', length(objStageStatus));
cprintf('*black','Previously Completed Files: %d\n', prev_files);
cprintf('*red','Skipped Files: %d\n', skip_files);
cprintf('*blue','Other Errors: %d\n', errorchk);
fname = createResultsCsv(sub, 'preica');




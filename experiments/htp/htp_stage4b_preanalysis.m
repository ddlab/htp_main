

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 4b PreAnalysis Review
% FILE: htp_stage4b_preanalysis.m
%
% NOTES:

% Overview
% htp_stage1 - raw import, channel assignment, filter, resample
% htp_stage2 - epoching; manual channel, artifact, and epoch cleaning
% htp_stage3 - ICA (parallel binary)
% htp_stage4 - non-neural component identification
% --> htp_stage4b - preanalysis and review of stage 4
% htp_stage5 - individual analysis (level1)
% htp_stage6 - group level analysis

% setup environment
clear all; close all;
global current_stage; current_stage = 10; global user;
cprintf('blue','\nStarting Stage 4b: PREANALYSIS and DATA FINALIZATION');

try 
    nologin = 1;
    htp_config; 
    clearWarnings; 
catch 
    disp('Error in Configuration Files'); 
end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'preanalysis';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

cprintf('blue','\nCurrent Data Directory: %s\n\n', htpcfg.basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

sub(1).createPaths(htpcfg.basePath);
sub(1).updatePaths(htpcfg.basePath);

% Create selection GUI


f = figure('Units', 'normalized','Position', [0.75 0.2 0.25 0.5]);

sub_table = uitable('Parent', f, 'Units', 'normalized', 'Position', [.05 .45 .9 .5], ...
    'Data', vertcat(sub(:).log_subjRow), 'CellSelectionCallback', @cellSelectCallback );

sub_panel = uipanel('Parent', f, 'Units', 'normalized', 'Position', [.05 .02 .9 0.41], ...
    'Tag', 'sub_panel');
sub_table.ColumnName = sub(1).log_subjHeader;

h4 = uicontrol(...
'Parent', sub_panel,...
'FontUnits',get(0,'defaultuicontrolFontUnits'),...
'Units','normalized',...
'HorizontalAlignment','left',...
'String',{  'Static Text' },...
'Style','text',...
'Position',[0.05 0.02 0.9 0.41],...
'Children',[],...
'Tag','select_detail',...
'FontSize',get(0,'defaultuicontrolFontSize'),...
'FontSizeMode',get(0,'defaultuicontrolFontSizeMode'));


%l = uipanel(f,'Title','Component Selection Tool',...
 %           'Position',[.40 .1 .55 .22]);
% 
%         
% %%
% for i = 1 : length(sub)
%     
%     s = sub(i);
%     
%     if ismember(i, objStageStatus) % processes only files in correct stage
%         
%         s.setUser( user ); 
%         
%         s.loadDataset( 'postcomps' );
%         
%         maxcomps = 20;
%         
%         % Open component topoplot, 20 components
%         pop_selectcomps(s.EEG, 1:maxcomps);
%         
%         h.tp = gcf;
%         h.tp.Position = [100 50 700 460];
%         
%         p = uipanel(h.tp,'Title','Component Selection Tool',...
%             'Position',[.40 .1 .55 .22]);
%         
%         strStatus = sprintf('(%d of %d): %s', i, length(sub), s.subj_basename);
%         
%         title = uicontrol(p,'Style','text',...
%             'String', strStatus,...
%             'FontSize', 10,...
%             'Units', 'normalized', ...
%             'HorizontalAlignment', 'left', ...
%             'BackgroundColor', [1 1 1], 'ForegroundColor', [0 0 0], ...
%             'Position',[0.05 0.7 0.9 0.30]);
%         
%         b1 = uicontrol(p,'Style','pushbutton','String','Save Components',...
%             'Units','normalized',...
%             'UserData', s, ...
%             'Callback',@b1_callback, ...
%             'BackgroundColor', [0 1 0], ...
%             'Position',[.6 0.05 .35 .25]);
%         
%         t1 = uicontrol(p,'Style','edit',...
%             'tag', 'comp_entry', ...
%             'String','',...
%             'Max',1,'Min',0,...
%             'Units', 'normalized', ...
%             'Position',[0.6 0.34 0.35 0.30]);
%         
%         b2 = uicontrol(p,'Style','pushbutton','String','C. Detail',...
%             'Units','normalized',...
%             'UserData', s, ...
%             'Callback', @b2_callback, ...
%             'Position',[.05 0.05 .20 .25]);
%         
%         closebtn = findobj('tag', 'Set threhsolds', 'parent', h.tp);
%         
%         % loop with comp number
%         
%         for ri = 1 : maxcomps
%         
%         chbutton = findobj('tag', ['comp' num2str(ri)], 'Parent', h.tp);
%         %disp(chbutton);
%         chbutton.Callback = ['pop_prop_extended( s.EEG, 0,' num2str(ri) ')']';
%                 
%         end
%         
%         %  delete(t2)
%         t2 = uicontrol(p,'Style','edit',...
%             'tag', 'comp_entry2', ...
%             'String','',...
%             'Max',1,'Min',0,...
%             'Units', 'normalized', ...
%             'Position',[0.05 0.34 0.20 0.30]);
%         
%         b3 = uicontrol(p,'Style','pushbutton','String','ICLABEL',...
%             'Units','normalized',...
%             'UserData', s, ...
%             'Callback', @b3_callback, ...
%             'Position',[.35 0.05 .20 .25]);
%         
%                         
%         % Open component time series
%         pop_eegplot( s.EEG, 0, 1, 1);
%         h.ep = gcf;
%         h.ep.Units = 'pixels';
%         h.ep.Position = [100 550 700 450];
%         
%         g = h.ep.UserData;
%         
%         % adjust default spacing/zoom
%         ESpacing = findobj('tag','ESpacing','parent', h.ep);   % ui handle
%         ESpacing.String = 50;
%         eegplot('draws',0);
%         
%         % adjust default number of epochs & components shown
%         g.winlength = 10;
%         g.dispchans = 10;
%         
%         if g.dispchans < 0 || g.dispchans > g.chans
%             g.dispchans = g.chans;
%         end
%         
%         set(h.ep, 'UserData', g);
%         eegplot('updateslider', h.ep);
%         eegplot('drawp',0);
%         eegplot('scaleeye', [], h.ep);
%         
%         uiwait(h.ep);
%         
%         s.proc_state = 'postcomps';
%         
%         % save cleaned dataset into the preica directory
%         s.storeDataset( s.EEG, pathdb.postcomps, s.subj_subfolder, s.filename.postcomps);
%         
%         
%         % unload data & decrease memory footprint
%         s.unloadDataset;
%         
%         s.outputRow('postcomps');
%         
%         
%     else
%          
%         if ismember(i, objStageStatus_completed)
%             s.unloadDataset;
%             s.proc_state = 'postcomps';
%             s.outputRow(stage_next);
%             prev_files = prev_files + 1;
%             
%         else
%             s.unloadDataset;
%             s.outputRow('error');
%             s.outputRow('error');
%             skip_files = skip_files + 1;
%         end
%         
%         
%     end
%     
%     sub(i) = s;
%     
% end

cprintf('*blue','\nSummary', length(objStageStatus));
cprintf('*green','\nFiles Processed: %d\n', length(objStageStatus));
cprintf('*black','Previously Completed Files: %d\n', prev_files);
cprintf('*red','Skipped Files: %d\n', skip_files);
cprintf('*blue','Other Errors: %d\n', errorchk);

fname = createResultsCsv(sub, 'preanalysis');



function b2_callback(src, event)

comps = findobj('tag', 'comp_entry2');

EEG = src.UserData.EEG;

% pop_prop( EEG , 0, str2num(comps.String), NaN, {'freqrange' [2 50] });

pop_prop_extended( EEG, 0, str2num(comps.String));

%pop_selectcomps(src.UserData.EEG);

end

function b3_callback(src, event)

comps = findobj('tag', 'comp_entry2');

EEG = src.UserData.EEG;

EEG = pop_iclabel(EEG);

pop_viewprops( EEG, 0);

src.UserData.EEG = EEG;

%pop_selectcomps(src.UserData.EEG);

end


function b1_callback(src, event)

comps = findobj('tag', 'comp_entry');

src.UserData.proc_removeComps = str2num(comps.String);

close all;
end

 function cellSelectCallback( o, cellSelectObj )
 
 idx = cellSelectObj.Indices(1,1);
 uitbl = cellSelectObj.Source;
 cols = uitbl.ColumnName;
 
 colIdx(1) = find(strcmp(cols, 'subj_basename'));
 colIdx(2) = find(strcmp(cols, 'subj_subfolder'));
 
 sel_detail = findobj(gcf,'Tag', 'select_detail');
 
 detailbox = sprintf('Filename: %s\nSubfolder: %s', uitbl.Data{idx, colIdx(1)}, ... 
     uitbl.Data{idx, colIdx(2)});
 
 
 sel_detail.String = detailbox;
 
 
 end
 


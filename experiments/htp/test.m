

% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (11/2018)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% For ACNPEEG data set
% setup environment
clear all; close all;


if ~isGuiRunning() 
    guihtpcfg.enable = 0;
    guiS4cfg.enable = 0;
end


global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( htpcfg.basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', htpcfg.basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

% vectorization of subject array
tic
for i = 1:1 %length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) && ~strcmp(s.subj_id, 'BAD') % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
                        
            % power analysis
%             s.setFreqTable([.5,3.5;4,7.5;8,10;10.5,13;13.5,30;30.5,80]); % requires input


            s.setFreqTable();
            s.getPntsTable;

%             s.generateStoreRoom;
%             s.bandAverageTrials;
%             s.generateNormalizedPowerSpectrum;
%             s.powerVisualization; % run under 1920*1440
%             s.surfaceLaplacian;

        tic;
               s.hilbert; % real slow
            toc;

            tic;
            s.generateConnectivities;
            toc;

            % End Analysis methods
            
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end

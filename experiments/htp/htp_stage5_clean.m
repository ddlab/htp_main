% Stage 5 Template
% Title: Analysis Optimization
% Author: R. Liu, E. Pedapati
% Contact: ernest.pedapati@cchmc.org

% setup environment
clear all;
close all;

cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');

global user, user = 'stage5';
global current_stage, current_stage = 5;

% load common config file
try
    htp_config;
    clearWarnings;
catch
    warning('Error in Configuration Files');
end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; 
stage_next = 'level1';

% load data objects spreadsheet and mat file
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed
load(matfile, 'sub');

% assign current spreadsheet and mat file to data objects in memory
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setUser( user ), sub, 'UniformOutput', false );
arrayfun(@(sub) sub.updatePaths( basePath ), sub, 'UniformOutput', false );


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');
prev_files = 0; skip_files = 0; errorchk = 0;

for i = 1 : 1 %length( sub )
    
   s = sub(i);
   
    if ismember(i, objStageStatus)
        
        try
        s.loadDataset( 'postcomps' );
        s.compRemove;
        catch
            warning('St5:LoadDataset', 'Error Loading Dataset');
        end
        
        % data analysis function here
        
        s = analysisRoutine( s, 'dbwPLI', 'gpuOn', 1);
        
        s.proc_state = 'level1';
        
    else
        
       if ismember(i, objStageStatus_completed)
            s.proc_state = 'level1';
            s.outputRow(stage_next);
            prev_files = prev_files + 1;
        else
            s.outputRow('error');
            skip_files = skip_files + 1;
        end 
   
        
    end
    
   s.unloadDataset;
   % GetSize(s);
   sub(i) = s;
    
end


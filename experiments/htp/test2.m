% test2

%sub(1) = sub(1);
sub(1).loadDataset('postcomps');

nbgroups = 2;
cum_pli1_band = NaN(sub(1).EEG.nbchan, sub(1).EEG.nbchan, 6, length(sub), nbgroups); 
%cum_pli1_band = NaN(32, 32, 6, length(sub), nbgroups); 
cum_pli2_band = NaN(sub(1).EEG.nbchan, sub(1).EEG.nbchan, 6, length(sub), nbgroups); 

sub(1).subj_id='Group1';
g1=0; g2=0;
for i = 1:1 %length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pli1 = s.rest_dbwpli;
%         pli2 = s.rest_conn_pli2;
        % chan*chan*band
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_pli1_band(:, :, :, i, gid) = pli1; 
%             cum_pli2_band(:, :, :, i, gid) = pli2; 
            g1 = g1+1;
        end
        if strcmp(s.subj_id, 'Group1')
            gid = 2;
            cum_pli1_band(:, :, :, i, gid) = pli1; 
%             cum_pli2_band(:, :, :, i, gid) = pli2; 
            g2 = g2+1;
        end   
    end
    
end

bandTable = ["\delta", "\theta", "lower \alpha", ...
    "upper \alpha", "\beta", "\gamma"];


% regrouping 1st measure
cum_pli1_band_g1 = cum_pli1_band(:,:,:, ~isnan(cum_pli1_band(1,sub(i).EEG.nbchan,randi(6),:,1)),1); 
cum_pli1_band_g2 = cum_pli1_band(:,:,:, ~isnan(cum_pli1_band(1,sub(i).EEG.nbchan,randi(6),:,2)),2);
%%
figure
for i=1:6
    subplot(2,6,i);
    imagesc(triu(cum_pli1_band_g1(:,:,i))+tril(cum_pli1_band_g1(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end
dim = [.05 .64 .2 .18]; % x y w h
str = {'Group 1 (8)'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
for i=1:6
    subplot(2,6,i+6);
    imagesc(triu(cum_pli1_band_g2(:,:,i))+tril(cum_pli1_band_g2(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end

dim = [.05 .12 .2 .18]; % x y w h
str = {'Group 2 (15)'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
suptitle('dbWPLI: imag(cdd)')

ds.connectStrength = cum_pli1_band_g1(~isnan(cum_pli1_band_g1(:,:,1))); % upper triangle to vector
CI= zeros(size(ds.connectStrength));
RI= zeros(size(ds.connectStrength));
index = 1;

for ci=2:sub(i).EEG.nbchan
    for ri=1:(ci-1)
        CI(index)=ci;
        RI(index)=ri;
        index=index+1;
    end
end
ds.chanPairs = [RI CI];
topoplot_connect_r(ds, EEG.chanlocs, 0.01);
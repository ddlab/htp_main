

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 2 Manual Channel, Artifact, and Epoch Cleaning
% FILE: htp_stage2.m
%
% NOTES:

% Overview
% htp_stage1 - raw import, channel assignment, filter, resample
% htp_stage2 - epoching; manual channel, artifact, and epoch cleaning
% htp_stage3 - ICA (parallel binary)
% htp_stage4 - non-neural component identification
% htp_stage5 - individual analysis (level1)
% htp_stage6 - group level analysis

% setup environment
clear all; close all;

if ~isGuiRunning() 
    guihtpcfg.enable = 0;
    guiS2cfg.enable = 0;
end

% ------ STAGE 2 CONFIGURATION -----
s2.cfg.cont_EpochLength         = 2;
s2.cfg.cont_EpochLimits     = [-1 1];
s2.cfg.chan_clean_mode          = 'manual'; % or asr
% ----------------------------------

global current_stage, current_stage = 2; global user;
cprintf('blue','\nStarting Stage 2: Manual Cleaning and Pre-ICA.');
try
    htp_config;
    clearWarnings;
catch
    disp('Error in Configuration Files');
end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'import'; stage_next = 'preica';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );


cprintf('blue','\nCurrent Data Directory: %s\n\n', htpcfg.basePath');
cprintf('*blue','\nManual Cleaning Operations\n');

prev_files = 0; skip_files = 0; errorchk = 0;

% for i = 1:length(sub)
i = 1;

while i <= length(sub)
    
    s = sub(i); % allows for parallel loops and temporary changes
    
    s.setUser( user );
    
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
        
        
        str_status = sprintf('\nOpening dataset (%d of %d): %s', i, length(sub), s.subj_basename);
        s.str_plottitle = str_status;
        
        cprintf('blue', str_status);
        cprintf('blue','\nFolder: %s\n', s.subj_subfolder);
        
        % load dataset
        s.openDataset(pathdb.import, s.subj_subfolder, s.filename.import);
        
        % ============= MANUAL CHANNEL CLEANING ======================
        
        switch s2.cfg.chan_clean_mode
            case 'manual'
                s.manualChanClean;
                s.removeInterpolateChans;
            case 'asr'
                s.cleanData;
                s.removeInterpolateChans;
        end
        
        cprintf('blue','\nManual Bad Channel Selection: ');
        cprintf('blue','\nBad Channel IDs Removed (blank = none): %s\n', num2str(s.proc_badchans));
        
        cprintf('blue','True Data Rank: %d\n', s.EEG.etc.dataRank);
        
        s.proc_ipchans = length(s.proc_badchans);
        s.proc_badchans = ['['  num2str(s.proc_badchans) ']'];
        s.proc_dataRank = s.EEG.etc.dataRank;
        
        
        
        % ============= MANUAL CONTINUOUS ARTIFACT CLEANING ==========
        
        cprintf('blue','\nManual Continuous Data Rejection: ');
        
        s.manualContClean;
        
        cprintf('blue','\nSegments removed: ');
        cprintf('blue','Data Length: Pre Post', s.EEG.etc.dataRank);
        
        
        % ==================== CREATE EPOCHS ======================
        
        % this will create regular epochs in the EEG_preica structure
        s.proc_contEpochLength = s2.cfg.cont_EpochLength;
        s.proc_contEpochLimits = s2.cfg.cont_EpochLimits; 
        s.createEpochs;
       
        
        % task based epochs for cognitive flexibility
        
        % see snagit picture
        % time window of interest is t = 0 is the user response
        % or the maximum 2000 ms, always 600 ms afterwards
        % ERP window: -1000 + 1000 of trigger 9 (positive), 10 (no press), 11
        % (reversal)
        % bands: theta, alpha ( < 30 hz filter) keep background gamma
        % run it both ways with or without high pass
        % amplitude measures and latency of P300 P3A, P3B
        % N100 data
        % leads: 68 and regions, Fz (11, 12, 5,6) Cz (6, 7,107, 129), Pz
        % (62,61,70,67,73,78,68)
        
        % datasets: see onedrive link
        
        % DIN11 - DIN9 amplitude / latency
        % table
        % time frequency / amplitude
        % rows: FXS, CONTROL
        % columns: feedback column reversal (negative) versus non-reversal
        % (positive) within that you amplitudes, latencies
        
        % blinded epochs
        % all epochs look the same
       
        
        
        
        
        
        % ==================== CLEAN EPOCHS ==========================
        
        s.cleanEpochs;
        
        % ============================================================
        
        
        % save cleaned dataset into the preica directory
        s.storeDataset( s.EEG_preica, pathdb.preica, s.subj_subfolder, s.filename.preica);
        
        % unload data & decrease memory footprint
        s.unloadDataset;
        
        % Code to verify or redo subject
        opts.Default = 'Continue';
        opts.Interpreter = 'tex';
        quest = sprintf('Do you want to verify %s as complete or redo?', s.subj_basename);
        answer = questdlg(quest,'Verify Processing',...
                  'Continue','Redo',opts);
        
        if strcmp(answer, 'Redo'), flag = 1; errorchk = 1;  else, flag = 0; end
                
        
      
        if errorchk == 0
            s.outputRow('preica');
            
        else
            s.outputRow('error');
        end
        
    else  % if object is not at correct stage, push through only as saved
        if ismember(i, objStageStatus_completed)
            % unload data & decrease memory footprint
            s.unloadDataset;
            s.outputRow(stage_next);
            prev_files = prev_files + 1;
        else
            % unload data & decrease memory footprint
            s.unloadDataset;
            s.outputRow('error');
            skip_files = skip_files + 1;
        end
    end
    

    
    % Code to verify or redo subject
    if flag == 1  % do not increment "i" counter if redo
        
    else
        sub(i) = s;  % apply changes to persistant object
        i = i + 1;   % increment counter
    end

end
cprintf('*blue','\nSummary', length(objStageStatus));
cprintf('*green','\nFiles Processed: %d\n', length(objStageStatus));
cprintf('*black','Previously Completed Files: %d\n', prev_files);
cprintf('*red','Skipped Files: %d\n', skip_files);
cprintf('*blue','Other Errors: %d\n', errorchk);
fname = createResultsCsv(sub, 'preica');




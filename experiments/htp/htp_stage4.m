

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 4 Manual Component Removal
% FILE: htp_stage4.m
%
% NOTES:

% setup environment
clear all; close all;

if ~isGuiRunning() 
    guihtpcfg.enable = 0;
    guiS4cfg.enable = 0;
end


global current_stage; current_stage = 4.5; global user;
cprintf('blue','\nStarting Stage 4: Post-ICA Component Identification');

try 
    htp_config; 
    clearWarnings; 
catch 
    disp('Error in Configuration Files'); 
end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postica'; stage_next = 'postcomps';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );


cprintf('blue','\nCurrent Data Directory: %s\n\n', htpcfg.basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

%for i = 1 : length(sub)

i = 1;

while i <= length(sub)   
    
    s = sub(i);   % allows for parallel processing and temporary changes
    
    if ismember(i, objStageStatus) % processes only files in correct stage
        
        s.setUser( user ); 
        
        s.loadDataset( 'postica' );
        
        maxcomps = 20;
        
        % Open component topoplot, 20 components
        pop_selectcomps(s.EEG, 1:maxcomps);
        
        h.tp = gcf;
        h.tp.Position = [100 50 700 460];
        
        p = uipanel(h.tp,'Title','Component Selection Tool',...
            'Position',[.40 .1 .55 .22]);
        
        strStatus = sprintf('(%d of %d): %s', i, length(sub), s.subj_basename);
        
        title = uicontrol(p,'Style','text',...
            'String', strStatus,...
            'FontSize', 10,...
            'Units', 'normalized', ...
            'HorizontalAlignment', 'left', ...
            'BackgroundColor', [1 1 1], 'ForegroundColor', [0 0 0], ...
            'Position',[0.05 0.7 0.9 0.30]);
        
        b1 = uicontrol(p,'Style','pushbutton','String','Save Components',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback',@b1_callback, ...
            'BackgroundColor', [0 1 0], ...
            'Position',[.6 0.05 .35 .25]);
        
        t1 = uicontrol(p,'Style','edit',...
            'tag', 'comp_entry', ...
            'String','',...
            'Max',1,'Min',0,...
            'Units', 'normalized', ...
            'Position',[0.6 0.34 0.35 0.30]);
        
        b2 = uicontrol(p,'Style','pushbutton','String','C. Detail',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback', @b2_callback, ...
            'Position',[.05 0.05 .20 .25]);
        
        closebtn = findobj('tag', 'Set threhsolds', 'parent', h.tp);
        
        UIButtonArr = findobj(h.tp, 'Type', 'UIControl');
        OriginalButtons = findobj(UIButtonArr, 'BackgroundColor', '[0.6600 0.7600 1]');
        for button_i = 1 : length(OriginalButtons), OriginalButtons(button_i).Visible = 'off'; end
       % UI = findobj(UIButtonArr, 'String', 'OK');
        
       % okbutton.Visible = 'off';
        
        % loop with comp number
        
        for ri = 1 : maxcomps
        
        chbutton = findobj('tag', ['comp' num2str(ri)], 'Parent', h.tp);
        %disp(chbutton);
        chbutton.Callback = ['pop_prop_extended( s.EEG, 0,' num2str(ri) ')']';
                
        end
        
        %  delete(t2)
        t2 = uicontrol(p,'Style','edit',...
            'tag', 'comp_entry2', ...
            'String','',...
            'Max',1,'Min',0,...
            'Units', 'normalized', ...
            'Position',[0.05 0.34 0.20 0.30]);
        
        b3 = uicontrol(p,'Style','pushbutton','String','ICLABEL',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback', @b3_callback, ...
            'Position',[.35 0.05 .20 .25]);
        
                        
        % Open component time series
        pop_eegplot( s.EEG, 0, 1, 1);
        h.ep = gcf;
        h.ep.Units = 'pixels';
        h.ep.Position = [100 550 700 450];
        
        g = h.ep.UserData;
        
        % adjust default spacing/zoom
        ESpacing = findobj('tag','ESpacing','parent', h.ep);   % ui handle
        ESpacing.String = 50;
        eegplot('draws',0);
        
        % adjust default number of epochs & components shown
        g.winlength = 10;
        g.dispchans = 10;
        
        if g.dispchans < 0 || g.dispchans > g.chans
            g.dispchans = g.chans;
        end
        
        set(h.ep, 'UserData', g);
        eegplot('updateslider', h.ep);
        eegplot('drawp',0);
        eegplot('scaleeye', [], h.ep);
        
        uiwait(h.ep);
        
        s.proc_state = 'postcomps';
        
        % save cleaned dataset into the preica directory
        s.storeDataset( s.EEG, pathdb.postcomps, s.subj_subfolder, s.filename.postcomps);
        
        
        % unload data & decrease memory footprint
        s.unloadDataset;
        
        s.outputRow('postcomps');
        
        % Code to verify or redo subject
        opts.Default = 'Continue';
        opts.Interpreter = 'none';
        quest = sprintf('Do you want to verify %s (%d of %d) as complete or redo?', s.subj_basename, i, length(sub));
        answer = questdlg(quest,'Verify Processing',...
                  'Continue','Redo',opts);
        
        if strcmp(answer, 'Redo'), flag = 1; errorchk = 1;  else, flag = 0; end
                
        
        
    else
         
        if ismember(i, objStageStatus_completed)
            s.unloadDataset;
            s.proc_state = 'postcomps';
            s.outputRow(stage_next);
            prev_files = prev_files + 1;
            
        else
            s.unloadDataset;
            s.outputRow('error');
            skip_files = skip_files + 1;
        end
        
        
    end
    
        % Code to verify or redo subject
    if flag == 1  % do not increment "i" counter if redo
        
    else
        sub(i) = s;  % apply changes to persistant object
        i = i + 1;   % increment counter
    end
  
end

cprintf('*blue','\nSummary', length(objStageStatus));
cprintf('*green','\nFiles Processed: %d\n', length(objStageStatus));
cprintf('*black','Previously Completed Files: %d\n', prev_files);
cprintf('*red','Skipped Files: %d\n', skip_files);
cprintf('*blue','Other Errors: %d\n', errorchk);

fname = createResultsCsv(sub, 'postcomps');



function b2_callback(src, event)

comps = findobj('tag', 'comp_entry2');

EEG = src.UserData.EEG;

% pop_prop( EEG , 0, str2num(comps.String), NaN, {'freqrange' [2 50] });

pop_prop_extended( EEG, 0, str2num(comps.String));

%pop_selectcomps(src.UserData.EEG);

end

function b3_callback(src, event)

comps = findobj('tag', 'comp_entry2');

EEG = src.UserData.EEG;

EEG = pop_iclabel(EEG);

pop_viewprops( EEG, 0);

src.UserData.EEG = EEG;

%pop_selectcomps(src.UserData.EEG);

end


function b1_callback(src, event)

comps = findobj('tag', 'comp_entry');

src.UserData.proc_removeComps = str2num(comps.String);

close all;
end



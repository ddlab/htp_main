% analysisRoutine() - Catalog of EEG/MEA Routines
%                     to called from Stage 5
% Usage:
%   >> s = analysisRoutine( s, 'key', value1 ...);
%
% Required input arguments:
%  s        - Input eegDataClass object (or subclass)
%  routine  - specific analysis routine
%
% Optional input arguments:
%  'gpuOn'     - compute GPU present {default: 0 (off): 1 (on)}
%  '
%
% Outputs:
%   s          - eegDataClass object (or subclass)
%
%
% Available Routines:
%
%
% Author:  Rui Liu & Ernest Pedapati, Cincinnati Children's
%

function s = analysisRoutine( s, routine, varargin)

% Defaults (can be modified)

if nargin < 1
    help analysisRoutine
    return
end

% Setup Inputs
try
    options = varargin;
    if ~isempty( varargin )
        for i = 1:2:numel(options)
            opt.(options{i}) = options{i+1};
        end
    else, opt = [];
    end
catch
    warning('Routines:Setup', 'Error calling options');
    return;
end

try % setup defaults if opts are not provided
    opt.gpuOn; catch, opt.gpuOn = 0;
end

% display options
opt

% select routine
%
switch routine
    
    case 'dbwPLI'
        if gpuOn == 1
            s = dbwPLI( s );
        else
            
        end
    otherwise
        error('Routines:SelectRoutine', 'No Matching Routine Found');
end

    function s = dbwPLI( s )
        tic;
        s.setFreqTable();
        s.getPntsTable;
        toc;
        tic;
        s.hilbert;
        toc;
        tic;
        s.generateConnectivity;
        toc;
        
    end


end



% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 3.0 (12/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage Configuration
% FILE: htp_config.m
%
% NOTES:
% for binica to correctly work install MKL (Intel Math Libraries) and add
% to system path: C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2018.5.274\windows\mkl



if ~isGuiRunning()
    guihtpcfg.enable = 0;
end


try
    if guihtpcfg.enable == 1, disp('GUI ENTERED CONFIGURATION ACTIVE.');end
catch
    disp('Please close GUI if using script based analysis only.');
end


%- helper function
agp = @(x) addpath(genpath(x)); 

%- turn off excessive warnings
warning( 'off', 'MATLAB:MKDIR:DirectoryExists' );
warning( 'off', 'MATLAB:xlswrite:AddSheet' ) ;
warning( 'off', 'MATLAB:table:ModifiedAndSavedVarnames');
warning( 'off', 'MATLAB:subscripting:noSubscriptsSpecified');

%- global variables
global current_stage; global user;

%htpcfg.catalog = readtable('docs/htp_catalog.csv');

%- configuration paramaters
%- * can override a parameter for a specific computer below

htpcfg.v = version; 
htpcfg.hostname = getHostName(); 
htpcfg.basePath = 'Error_Undefined';

if ~guihtpcfg.enable
    htpcfg.chan_clean_mode = 'manual';
    htpcfg.userlogin = 0;
    htpcfg.title_string = 'DemoData';
    htpcfg.parallel_processing_on = 0;   
end

%- data directories - EDIT HERE

switch htpcfg.hostname
    
    case 'DESKTOP-91N7ALB'
        
        htpcfg.basePath = 'C:/data/seconddataset/';
        
    case 'EA19-00124'
        
        % addpath('/Users/ped8zl/code/eeglab14_1_2b/');
        htpcfg.basePath = '/Users/ped8zl/data/sattest/';
        %basePath = '/Users/ped8zl/data/binicatest/';
        %htpcfg.basePath = '/Users/ped8zl/data/mouse/';
        
    case 'OFFICE8700'
        addpath('C:\code\eeglab14_1_2b');
        htpcfg.basePath = 'F:\satdata128\';
       % htpcfg.basePath = 'F:\meaTest\';
        
    case 'Desktop'
        htpcfg.parallel_processing_on = 0;
        %chan_clean_mode = 'asr';
        addpath('C:\EEG\eeglab');
        %basePath = 'E:\T32RestData';
        %basePath = 'E:\testdata32';
        %basePath = 'E:\sattest2';
        %basePath = 'E:\preprocessed\SATDATA';
        %basePath = 'E:\demo32';
        %htpcfg.basePath = 'D:\Datasets\U54P1';
        htpcfg.basePath = 'E:\demo128';
        
    case 'P18-1610'
       % addpath('C:\code\eeglab14_1_2b');
        addpath('C:\code\eeglab14_1_2b\');
        htpcfg.basePath = 'C:\data\test128';
         htpcfg.basePath = 'C:\data\mea\';
      %htpcfg.basePath = 'D:\Datasets\U54P1\';

        
end



if strcmp(htpcfg.basePath, 'Error_Undefined')
    current_stage = 0;
    cprintf('err',...
        '\nERROR: Hostname is incorrect or not defined.\nCurrent host is: %s\nPlease edit %s file.\n\n', ...
        htpcfg.hostname, mfilename);
    return;
end

% define electrode configurations
htpcfg.chaninfo(3) = electrodeConfigClass;

% EGI 128 
    htpcfg.chaninfo(1).net_displayname  = 'EGI Hydrocel 128';
    htpcfg.chaninfo(1).net_name         = 'EGI128';
    htpcfg.chaninfo(1).net_file         = 'chanfiles/GSN-HydroCel-129.sfp';
    htpcfg.chaninfo(1).net_graphic      = 'chanfiles/EEG_128_channel_array_map.pdf';
    htpcfg.chaninfo(1).net_filter       = '*.raw';
    htpcfg.chaninfo(1).net_nochans      = 128;
    htpcfg.chaninfo(1).net_regions      = {...
        'Frontal',   [1,1],  [1,2,3,4,8,9,10,11,15,16,18,19,20,23,24,26,27,33,125,128]; ...
        'Premotor',  [2,1],  [21,30,36,29,40,35,25,28,34,39,119,124,112,123,118,122,117,116,121]; ...
        'Supplementary Motor',  [3,1],  [5,6,12,13,21,119,113]; ...
        'Motor', [4,1],  [36,41,37,42,38,32,7,107,106,105,104,111,110,81,88,]; ... %took out 129,
        'Somatosensory', [5,1], [32,38,43,48,42,46,47,54,55,80,81,88,94,99,104,103]; ... %took out 129
        'Parietal',[6,1], [51,52,53,54,57,58,59,60,61,62,66,67,68,72,73,77,78,79,80,85,86,87];
        'Occipital',[7,1],[65,66,72,73,77,85,91,69,70,71,76,84,90,75,83]};

% EGI 32
    htpcfg.chaninfo(2).net_displayname  = 'EGI Hydrocel 32';
    htpcfg.chaninfo(2).net_name         = 'EGI32';
    htpcfg.chaninfo(2).net_file         = 'chanfiles/GSN-HydroCel-32.sfp';
    htpcfg.chaninfo(2).net_graphic      = 'chanfiles/EEG_32_channel_array_map.pdf';
    htpcfg.chaninfo(2).net_filter       = '*.raw';
    htpcfg.chaninfo(2).net_nochans      = 32;
    htpcfg.chaninfo(2).net_regions      = { ...
        'Frontal',   [1,1],  [1,27,2,3,4,11,12,31,32]; ...
        'Premotor',  [2,1],  [11,12]; ...
        'Supplementary Motor',  [3,1], [17,28]; ...
        'Motor', [4,1],  [5,6,13,14]; ...
        'Somatosensory', [5,1], [13,7,8,14]; ...
        'Parietal',[6,1], [23,15,7,19,8,16,24]; ...
        'Occipital', [7, 1], [15,9,29,19,16]};

% MEA 32
% Notes: rest data has 32 channels with 1 piezo for movement (33)
    htpcfg.chaninfo(3).net_displayname  = 'MEA 30';
    htpcfg.chaninfo(3).net_name         = 'MEA30';
    %htpcfg.chaninfo(3).net_file         = 'chanfiles/mealabels_new.mat';
    htpcfg.chaninfo(3).net_file         = 'chanfiles/mea3d.mat';
    htpcfg.chaninfo(3).net_graphic      = 'chanfiles/meaimg.PNG';
    htpcfg.chaninfo(3).net_filter       = '*.edf';
    htpcfg.chaninfo(3).net_nochans      = 30;
    htpcfg.chaninfo(3).net_regions      = { ...
        'Frontal',   [1,1],  [17,16,15,14,19,18,13,12]; ...
        'Premotor',  [2,1],  [11,12]; ...
        'Supplementary Motor',  [3,1], [18,13]; ...
        'Motor', [4,1],  [20,11]; ...
        'Somatosensory', [5,1], [13,7,8,14]; ...
        'Parietal',[6,1], [23,22,9,8,26,25,6,5]; ...
        'Occipital', [7, 1], [25,6,28,3,29,2]};    

% GUSB     

    

if current_stage < 6 
    cprintf('*black','\n\nStarting High Throughput Pipeline v1.0 ...\n');
    cprintf('blue','\nCONFIRM CORRECT DATA DIRECTORY\nCurrent Data Directory: %s\n\n', htpcfg.basePath);
    
    % get user initials for processing
    if htpcfg.userlogin == 1
        user = getUserInitials;
    else
        user = 'DEMO';
    end
    
    
    if strcmp(user, 'Not Entered')
        cprintf('err','\nERROR: No initials entered. Please run script again.\n\n');
        current_stage = 0;
        
    end
    
    %pause;
end

clear sub;

if exist('ALLCOM')
    
else
    eeglab;
    close gcf;
end

htpcfg.timetag2    = datestr(now,'yymmddHHMM');

%Sets the units of your root object (screen) to pixels
set(0,'units','pixels');
%Obtains this pixel information
htpcfg.screensize = get(0,'screensize');

try
    htpcfg.eeglabpath = which('eeglab.m');
    htpcfg.eeglabpath = htpcfg.eeglabpath(1:end-length('eeglab.m'));
    htpcfg.hdmfile = fullfile(htpcfg.eeglabpath, '\plugins\dipfit2.3\standard_BEM\standard_vol_SCCN.mat');
    htpcfg.mrifile = fullfile(htpcfg.eeglabpath, '\plugins\dipfit2.3\standard_BEM\standard_mri.mat');
    
    htpcfg.eegpluginspath = fullfile([htpcfg.eeglabpath 'plugins']);
    htpcfg.plugerr = 0;
    
    
    % check plugins
    if checkEegPlugin('iclabel', htpcfg.eegpluginspath), else,    htpcfg.plugerr = 1; pluginErrMsg('ICLabel'); end
    if checkEegPlugin('dipfit', htpcfg.eegpluginspath), else,     htpcfg.plugerr = 1; pluginErrMsg('dipfit2.0'); end
    if checkEegPlugin('viewprops', htpcfg.eegpluginspath), else,  htpcfg.plugerr = 1; pluginErrMsg('ViewProps'); end
    if checkEegPlugin('fieldtrip', htpcfg.eegpluginspath), else,  htpcfg.plugerr = 1; pluginErrMsg('FieldTrip-lite'); end
    if checkEegPlugin('firfilt', htpcfg.eegpluginspath), else,    htpcfg.plugerr = 1; pluginErrMsg('FirFilt'); end
    if checkEegPlugin('biosig', htpcfg.eegpluginspath), else,    htpcfg.plugerr = 1; pluginErrMsg('Biosig'); end

   
        
catch
    
    fprintf('ERROR: Please set EEGLAB path.');
    
end

try
assert(~htpcfg.plugerr);
catch
    disp('Error in htp_config.m Script - Please check configuration');
end





function pluginErrMsg( error_str )

cprintf('err','\nERROR: EEGLAB plugin "%s" is not installed. \n\n', error_str);

end
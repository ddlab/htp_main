

% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (11/2018)
% AUTHOR: R. Liu, E. PEDAPATI
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 6 Group Analysis (Level2)
% FILE: htp_stage6.m
%
% NOTES: 
% For ACNPEEG data set
% setup environment
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( htpcfg.basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', htpcfg.basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

% vectorization of subject array

parfor i = 1:length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
           
        % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            
            % dynamic component removal using CSV file
            s.compRemove;
                        
            % power analysis
%             s.setFreqTable([.5,3.5;4,7.5;8,10;10.5,13;13.5,30;30.5,80]); % requires input
            s.setFreqTable();
            s.getPntsTable;
%             s.generateStoreRoom;
%             s.bandAverageTrials;
%             s.generateNormalizedPowerSpectrum;
%             s.powerVisualization; % run under 1920*1440
%             s.surfaceLaplacian;
            s.hilbert; % real slow
            s.generateConnectivities;
            % End Analysis methods
            
            s.proc_state = 'level1';
        
    else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
    end   
    
    sub(i) = s;
    i
end


%% Stage 6
% normalized power
nbgroups = 2;
% cum_rel_power_band = NaN(128, 6, length(sub), nbgroups); 
% cum_abs_power_band = NaN(128, 6, length(sub), nbgroups); 
cum_norm_power_band = NaN(128, 6, length(sub), nbgroups); 
% group1_peak = NaN(1, length(sub));group1_locs = NaN(1, length(sub));
% group2_peak = NaN(1, length(sub));group2_locs = NaN(1, length(sub));
cum_norm_power = NaN(80*2+1, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
%         pxx_rel = s.rest_rel_power_band_average_trials;
%         pxx_abs = s.rest_abs_power_band_average_trials;
        pxx_norm_band = s.rest_abs_norm_band_average_trials;
        pxx_norm = s.rest_abs_norm_average_trials;
        % frequencies*trials*chans
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            % cumulative rel_power @ delta, theta, alpha1, alpha2, beta, gamma
%             cum_rel_power_band(:, :, i, gid) = pxx_rel'; 
%             cum_abs_power_band(:, :, i, gid) = pxx_abs'; 
            cum_norm_power_band(:, :, i, gid) = pxx_norm_band'; 
            cum_norm_power(:, i, gid) = mean(pxx_norm, 2); 
            g1 = g1+1;
%             group1_peak(i) = s.pks; group1_locs(i) = (s.locs-1)/2+4;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
%             cum_rel_power_band(:, :, i, gid) = pxx_rel';
%             cum_abs_power_band(:, :, i, gid) = pxx_abs'; 
            cum_norm_power_band(:, :, i, gid) = pxx_norm_band'; 
            cum_norm_power(:, i, gid) = mean(pxx_norm, 2); 
            g2 = g2+1;
%             group2_peak(i) = s.pks; group2_locs(i) = (s.locs-1)/2+4;
            
        end
    end
end

% regrouping
% cum_rel_power_g1 = cum_rel_power_band(:,:,~isnan(...
%     cum_rel_power_band(randi(128),randi(6),:,1)),1); 
% % ideally nbChan*(6)nbBand*nbSubj
% % isnan returns an array the same size containing logical (vector index)
% cum_rel_power_g2 = cum_rel_power_band(:,:,~isnan(...
%     cum_rel_power_band(randi(128),randi(6),:,2)),2);
% 
% cum_abs_power_g1 = cum_abs_power_band(:,:,~isnan(...
%     cum_abs_power_band(randi(128),randi(6),:,1)),1); 
% 
% cum_abs_power_g2 = cum_abs_power_band(:,:,~isnan(...
%     cum_abs_power_band(randi(128),randi(6),:,2)),2);
% 
cum_norm_power_band_g1 = cum_norm_power_band(:,:,~isnan(...
    cum_norm_power_band(randi(128),randi(6),:,1)),1); 
cum_norm_power_band_g2 = cum_norm_power_band(:,:,~isnan(...
    cum_norm_power_band(randi(128),randi(6),:,2)),2);

% data for alpha peak shift examination
cum_norm_power_g1 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,1)),1);
cum_norm_power_g2 = cum_norm_power(:,~isnan(cum_norm_power(randi(161),:,2)),2);

% to be extended to GUI
upper = 80;
grp1 = mean(cum_norm_power_g1, 2);
grp2 = mean(cum_norm_power_g2, 2);
plot(0:0.5:upper, grp1(1:(upper*2+1)),'LineWidth',1.2);hold on
plot(0:0.5:upper, grp2(1:(upper*2+1)),'LineWidth',1.2);hold off
legend({'group1 (8)','group2 (15)'})
xlabel('Frequency (Hz)')
ylabel('normalized power')

chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
nbband = 6;
bandTable = ["\delta", "\theta", "lower \alpha", ...
    "upper \alpha", "\beta", "\gamma"];

%% 1206 poster figure
figure('units','normalized','outerposition',[0 0 1 1]);
% (1) relative power Group 1
mean_rel_power_g1 = mean(cum_rel_power_g1, 3);
for m = 1:nbband
    subplot(4,6,m)
    topoplot(mean_rel_power_g1(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
    if m == 1 || m==6
        caxis([0, .5]);
    elseif m==3 || m==4
        caxis([0, .15]);
    else
        caxis([0, .25]);
    end
    h=colorbar('SouthOutside');
    set(h, 'Position', [.135*(m-1)+.16 .73 .045 .015]);
    title(bandTable(m),'FontSize', 12)
end
dim = [.07 .64 .2 .18]; % x y w h
str = {['Group1', '(',num2str(g1),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (2) relative power Group 2
mean_rel_power_g2 = mean(cum_rel_power_g2, 3);
for m = 1:nbband
    subplot(4,6,m+6)
    topoplot(mean_rel_power_g2(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
    if m == 1 || m==6
        caxis([0, .5]);
    elseif m==3 || m==4
        caxis([0, .15]);
    else
        caxis([0, .25]);
    end
%     colorbar('Location', 'north')
end
dim = [.07 .44 .2 .18]; % x y w h
str = {['Group2', '(',num2str(g2),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+12);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    if m == 6
        h=colorbar('SouthOutside');
        set(h, 'Position', [.135*(6-1)+.16 .31 .045 .015]);
        h.Label.String = 'T values';
%         h.Label.FontSize = 10;
    end
end
dim = [.05 .24 .2 .18]; % x y w h
str = {'Group1 > Group2'};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g2,cum_rel_power_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+18);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
%     colorbar
end
dim = [.05 .04 .2 .18]; % x y w h
str = {'Group 1 < Group 2'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);

axes( 'Position', [0, 0.95, 1, 0.05] ) ;
set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
text( 0.5, 0, 'Relative Power', 'FontSize', 14', 'FontWeight', 'Bold', ...
  'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;

% saveas(gcf,[s.pathdb.figs, 'ACNPEEG_unpaired.png']);
% close

%% 12.26 poster figure preliminary
figure('units','normalized','outerposition',[0 0 1 1]);
% (1) normalized power Group 1
mean_norm_power_g1 = mean(cum_norm_power_band_g1, 3);
for m = 1:nbband
    subplot(4,6,m)
    topoplot(mean_norm_power_g1(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
%     if m == 1 || m==6
%         caxis([0, .5]);
%     elseif m==3 || m==4
%         caxis([0, .15]);
%     else
%         caxis([0, .25]);
%     end
    h=colorbar('SouthOutside');
    set(h, 'Position', [.135*(m-1)+.16 .73 .045 .015]);
    title(bandTable(m),'FontSize', 12)
end
dim = [.07 .64 .2 .18]; % x y w h
str = {['Group1', '(',num2str(g1),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (2) normalized power Group 2
mean_norm_power_g2 = mean(cum_norm_power_band_g2, 3);
for m = 1:nbband
    subplot(4,6,m+6)
    topoplot(mean_norm_power_g2(:,m), s.EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
%     if m == 1 || m==6
%         caxis([0, .5]);
%     elseif m==3 || m==4
%         caxis([0, .15]);
%     else
%         caxis([0, .25]);
%     end
    h=colorbar('SouthOutside');
    set(h, 'Position', [.135*(m-1)+.16 .52 .045 .015]);
    
end
dim = [.07 .44 .2 .18]; % x y w h
str = {['Group2', '(',num2str(g2),')']};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (3) Group 1 > Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_norm_power_band_g1,cum_norm_power_band_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+12);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
    if m == 6
        h=colorbar('SouthOutside');
        set(h, 'Position', [.135*(6-1)+.16 .31 .045 .015]);
        h.Label.String = 'T values';
%         h.Label.FontSize = 10;
    end
end
dim = [.05 .24 .2 .18]; % x y w h
str = {'Group1 > Group2'};
annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
% (4) Group 1 < Group 2
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_norm_power_band_g2,cum_norm_power_band_g1,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(4,6,m+18);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
    caxis([0 5]);
%     colorbar
end
dim = [.05 .04 .2 .18]; % x y w h
str = {'Group 1 < Group 2'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);

axes( 'Position', [0, 0.95, 1, 0.05] ) ;
set( gca, 'Color', 'None', 'XColor', 'White', 'YColor', 'White' ) ;
text( 0.5, 0, 'Normalized Power', 'FontSize', 14', 'FontWeight', 'Bold', ...
  'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom' ) ;

% saveas(gcf,[s.pathdb.figs, 'ACNPEEG_unpaired.png']);
% close


%% Stage 6 
% connectivity
nbgroups = 2;
cum_pli1_band = NaN(128, 128, 6, length(sub), nbgroups); 
%cum_pli1_band = NaN(32, 32, 6, length(sub), nbgroups); 

% cum_pli2_band = NaN(128, 128, 6, length(sub), nbgroups); 

g1=0; g2=0;
for i = 1: 1 %length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pli1 = s.rest_dbwpli;
%         pli2 = s.rest_conn_pli2;
        % chan*chan*band
        if strcmp(s.subj_id, 'Group1')
            gid = 1;
            cum_pli1_band(:, :, :, i, gid) = pli1; 
%             cum_pli2_band(:, :, :, i, gid) = pli2; 
            g1 = g1+1;
            
        elseif strcmp(s.subj_id, 'Group2')
            gid = 2;
            cum_pli1_band(:, :, :, i, gid) = pli1; 
%             cum_pli2_band(:, :, :, i, gid) = pli2; 
            g2 = g2+1;
        end   
    end
    
end

bandTable = ["\delta", "\theta", "lower \alpha", ...
    "upper \alpha", "\beta", "\gamma"];


% regrouping 1st measure
cum_pli1_band_g1 = cum_pli1_band(:,:,:, ~isnan(cum_pli1_band(1,128,randi(6),:,1)),1); 
%cum_pli1_band_g2 = cum_pli1_band(:,:,:, ~isnan(cum_pli1_band(1,128,randi(6),:,2)),2);
%%
figure
for i=1:6
    subplot(2,6,i);
    imagesc(triu(cum_pli1_band_g1(:,:,i))+tril(cum_pli1_band_g1(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end
dim = [.05 .64 .2 .18]; % x y w h
str = {'Group 1 (8)'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
for i=1:6
    subplot(2,6,i+6);
    imagesc(triu(cum_pli1_band_g2(:,:,i))+tril(cum_pli1_band_g2(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end
dim = [.05 .12 .2 .18]; % x y w h
str = {'Group 2 (15)'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
suptitle('dbWPLI: imag(cdd)')

ds.connectStrength = cum_pli1_band_g1(~isnan(cum_pli1_band_g1(:,:,1))); % upper triangle to vector
CI= zeros(size(ds.connectStrength));
RI= zeros(size(ds.connectStrength));
index = 1;
for ci=2:128
    for ri=1:(ci-1)
        CI(index)=ci;
        RI(index)=ri;
        index=index+1;
    end
end
ds.chanPairs = [RI CI];
topoplot_connect_r(ds, EEG.chanlocs, 0.01);


% regrouping 2nd measure 
cum_pli2_band_g1 = cum_pli2_band(:,:,:, ~isnan(cum_pli2_band(1,128,randi(6),:,1)),1); 
cum_pli2_band_g2 = cum_pli2_band(:,:,:, ~isnan(cum_pli2_band(1,128,randi(6),:,2)),2);

figure
for i=1:6
    subplot(2,6,i);
    imagesc(triu(cum_pli2_band_g1(:,:,i))+tril(cum_pli2_band_g1(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end
dim = [.05 .64 .2 .18]; % x y w h
str = {'Group 1 (8)'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);

for i=1:6
    subplot(2,6,i+6);
    imagesc(triu(cum_pli2_band_g2(:,:,i))+tril(cum_pli2_band_g2(:,:,i)'));
    colorbar('southoutside');
    title([num2str(bandTable(i)),' band']);
end
dim = [.05 .12 .2 .18]; % x y w h
str = {'Group 2 (15)'};
annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 12);
suptitle('PLI: imag(cdd)')
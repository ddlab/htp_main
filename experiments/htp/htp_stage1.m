
% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 1 Import Raw Files
% FILE: htp_stage1.m
%
% NOTES: Placed RAW files in S00_RAW in Subfolders

% Overview

% PREPROCESSING
% htp_stage1 - raw import, channel assignment, filter, resample
% htp_stage2 - epoching; manual channel, artifact, and epoch cleaning
% htp_stage3 - ICA (parallel binary)
% htp_stage4 - non-neural component identification

% ANALYSIS
% htp_stage5 - individual analysis (level1)
% htp_stage6 - group level analysis

% please set the number of channels used by your data files

% check if GUI is running
clear htpcfg sub;

if ~isGuiRunning() 
    guihtpcfg.enable = 0;
    guiS1cfg.enable = 0;
end

try
    if guihtpcfg.enable == 1, disp('GUI ENTERED CONFIGURATION ACTIVE.');end
catch
    disp('Please close GUI if using script based analysis only.');
end


%if  isGuiRunning, updateAppStatus(''); end; 

% setup environment
cprintf('blue', '\nStarting Stage 1: Raw Data Import and Preprocessing...\n');


global current_stage, current_stage = 1;


if guiS1cfg.enable == 0
    % ------ STAGE 1 CONFIGURATION -----
    %s1.cfg.chans       =     'EGI128';  % currently EGI32 or EGI128 or
    %MEA30
    s1.cfg.chans       =     'MEA30';
    s1.cfg.lowcutoff   =     1;
    s1.cfg.highcutoff  =     120;
    s1.cfg.notch       =     [57 63 3300];  % band limits (low high) and filter order
    s1.cfg.parfor      =     1; % turn on parallel processing (requires toolbox) 0 = off, 1 = on
    s1.cfg.resample    =     500;
    % -----------------------------------
    
else
    
    s1.cfg.chans       =     guiS1cfg.chans;  % currently EGI32 or EGI128
    s1.cfg.lowcutoff   =     guiS1cfg.lowcutoff;
    s1.cfg.highcutoff  =     guiS1cfg.highcutoff;
    s1.cfg.notch       =     guiS1cfg.notch;  % band limits (low high) and filter order
    s1.cfg.parfor      =     guiS1cfg.parfor; % turn on parallel processing (requires toolbox) 0 = off, 1 = on
    s1.cfg.resample    =     guiS1cfg.resample;

end


try
    htp_config;
    clearWarnings; 
catch 
    disp('');
    disp('Errors in htp_config, please clear before proceeding.');

end

assert(~logical(htpcfg.plugerr));

% REVISED: find correct index of channel cfg object defined in htp_config
chanindex = find(strcmp(s1.cfg.chans, {htpcfg.chaninfo.net_name}));
s1.cfg.chaninfo = htpcfg.chaninfo(chanindex);


if current_stage == 0, return; end

%numberofchannels = s1.cfg.chan_number;

% Setup Environment
s1.configObject = RestEegDataClass();
s1.configObject.createPaths( htpcfg.basePath );
pathdb = s1.configObject.pathdb;

% filters specific file ext. for EEG system
filter = s1.cfg.chaninfo.net_filter;


% Generate RAW data list
dirlist = getFileList( filter, pathdb.raw );
fnlist = dirlist.full';
subfolderlist = dirlist.subfolder';

% separate eventdata files - MEA specific
file_eventIdx = contains(fnlist,'dig.edf');
file_dataIdx = ~contains(fnlist,'dig.edf');

event_subfolderlist = subfolderlist(file_eventIdx);
event_fnlist = fnlist(file_eventIdx);
subfolderlist = subfolderlist(file_dataIdx);

if isempty(subfolderlist{1})
    cprintf('err', '\n\nERROR: Please place files in subfolders and restart.\n');
    return;
end

clear s1.configObject, pathdb, filter, dirlist, subfolderlist, file_eventIdx, file_dataIdx, ...
    event_subfolderlist, event_fnlist;


fnlist = fnlist(file_dataIdx);

% Create Data Objects Array
for i = 1:length(fnlist), sub(i) = RestEegDataClass();  end

for i = 1:length(fnlist), sub(i).updatePaths( htpcfg.basePath ); end

for i = 1:length(fnlist), sub(i).assignRawFile( subfolderlist{i}, fnlist{i} ); end

for i = 1:length(fnlist), sub(i).setUser( user ); end

for i = 1:length(fnlist), sub(i).changeStudyTitle( htpcfg.title_string ); end

for i = 1:length(fnlist), sub(i).createFileNames; end

for i = 1:length(fnlist), sub(i).setElectrodeSystem( s1.cfg.chaninfo ); end

for i = 1:length(fnlist), sub(i).getRawData; if  isGuiRunning, updateAppStatus(['Imported: ' sub(i).subj_basename]); end; end


% set resample rate
for i = 1:length(fnlist), sub(i).setResampleRate( s1.cfg.resample ); end

if s1.cfg.parfor == 1 % please note the assignment to 'sub' for pool to process correctly
    
    parfor i = 1:length(fnlist), s = sub(i); s.filtData; sub(i) = s; end
    parfor i = 1:length(fnlist), s = sub(i); s.resampleData;  sub(i) = s; end
    parfor i = 1:length(fnlist), s = sub(i); s.averageRefData;  sub(i) = s; end
    
else
    
    for i = 1:length(fnlist), sub(i).filt_highpass( s1.cfg.lowcutoff ); end
    for i = 1:length(fnlist), sub(i).filt_lowpass( s1.cfg.highcutoff ); end
    for i = 1:length(fnlist), sub(i).filt_notch( s1.cfg.notch(1), s1.cfg.notch(2), s1.cfg.notch(3) ); end
    
    
    % for i = 1:length(fnlist), sub(i).filt_for_ica([2 80]); end  %
    % alternative per Makato
    
    for i = 1:length(fnlist), sub(i).resampleData; end
    for i = 1:length(fnlist), sub(i).averageRefData; end
    
end

for i = 1:length(fnlist), sub(i).storeDataset( sub(i).EEG, pathdb.import, sub(i).subj_subfolder, sub(i).filename.import ); end
for i = 1:length(fnlist), sub(i).unloadDataset; end
for i = 1:length(fnlist), sub(i).outputRow('import'); end

if strcmp(fnlist, 'No File')
    cprintf('err', '\n\nERROR: Missing files. Check folder and subfolders.\n');
    return;
end

fname = createResultsCsv(sub, 'import');


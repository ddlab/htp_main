

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 3 Independent Component Analysis
% FILE: htp_stage3.m
%
% NOTES: for use with BINICA
%    instructions: place binica.exe in EEGLAB folder: eeglab/functions/resources
%    modify functions/sigprocfunc/icadefs.m (line 141):
%    ICABINARY = fullfile(eeglab_p, 'functions', 'resources', 'binica.exe');


% setup environment
clear all; close all;

if ~isGuiRunning() 
    guihtpcfg.enable = 0;
    guiS3cfg.enable = 0;
end

% ------ STAGE 3 CONFIGURATION -----

s3.cfg.dipfit_calculations = 0;

% -----------------------------------

dipfit_calculations = s3.cfg.dipfit_calculations;

global current_stage, current_stage = 3;
cprintf('*blue','\nStage 3: Independent Component Analysis');

try 
    htp_config; 
    clearWarnings; 
    icadefs; 
catch
    disp('Error in Configuration Files'); 
end

if current_stage == 0, return; end


cprintf('*blue','\nICA Processor: %s', ICABINARY);
cprintf('blue','\nCurrent Data Directory: %s\n', htpcfg.basePath);


% selecting objects by CSV
stage_last = 'preica'; stage_next = 'postica';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);

objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub');
arrayfun(@(sub) sub.updatePaths( htpcfg.basePath ), sub, 'UniformOutput', false);
arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

% Status update
cprintf('Blue','\nCurrent Stage 2 Spreadsheet: \n%s\n', csvfile);
cprintf('Blue','\nCurrent Stage 2 EEG Object File: \n%s', matfile);
cprintf('Blue', '\nTotal files: %d', length(sub));
cprintf('blue','\n', matfile);

currentdir = pwd;

cd(sub(1).pathdb.icaweights);

prev_files = 0; skip_files = 0; errorchk = 0;

tic
parfor i = 1 : length(sub)
    
    s = sub(i);
    
    if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
        
		try
            s.loadDataset('postica');
            s.proc_state = 'PostICA';
            
            
        catch
		
        s.loadDataset( 'preica' );
        
        rank = s.EEG.etc.dataRank;
        
        %s.EEG_postica = pop_runica(sub(i).EEG,'icatype','binica', 'extended',1,'interupt','on','pca',75);
        s.EEG = pop_runica(s.EEG,'icatype','binica', 'extended',1,'interupt','on','pca', rank);

         % Generic DIPFIT calculations        
        if dipfit_calculations == 1
            s.icview;
            s.set_dipfitsettings( hdmfile, mrifile );
            s.generic_dipfit;
            s.set_dipfitcalc( 1 );          
        else            
            s.set_dipfitcalc( 0 );            
        end
        
		end
		
        if ~isempty(s.EEG.icaweights)
            s.proc_icaweights = 'Yes';
            s.proc_state = 'PostICA';
            s.outputRow('postica');
            
        else
		  if ~strcmp(s.proc_state,'PostICA')
            s.proc_icaweights = 'Error';
            s.proc_state = 'ICA_Error';
			end
            s.outputRow('postica');
            
        end
        
        % save cleaned dataset into the preica directory
        s.storeDataset( s.EEG, pathdb.postica, s.subj_subfolder, s.filename.postica);
        
       
        
        % unload data & decrease memory footprint
        s.unloadDataset;
        
        
    else  % if object is not at correct stage, push through only as saved
        
        if ismember(i, objStageStatus_completed)
            % unload data & decrease memory footprint
            sub(i).unloadDataset;
            s.outputRow('postica');
            prev_files = prev_files + 1;
        else
            % unload data & decrease memory footprint
            sub(i).unloadDataset;
			  s.proc_state = 'ICA_Error';
            s.outputRow('error');
            skip_files = skip_files + 1;
        end
    end
    

    
    sub(i) = s;
    
end
toc
cd(currentdir);

% creates results spreadsheet
cprintf('*blue','\nSummary', length(objStageStatus));
cprintf('*green','\nFiles Processed: %d\n', length(objStageStatus));
cprintf('*black','Previously Completed Files: %d\n', prev_files);
cprintf('*red','Skipped Files: %d\n', skip_files);
cprintf('*blue','Other Errors: %d\n', errorchk);

fname = createResultsCsv(sub, 'postica');
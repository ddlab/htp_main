

% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 5 Subject Analysis (Level1)
% FILE: htp_stage5.m
%
% NOTES: 

% setup environment
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, htpcfg.basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( htpcfg.basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', htpcfg.basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

% specify single subject or array or ALL {sub.subj_basename}
stage45.specifySubject = {sub.subj_basename}; % 'D0918';

% vectorization of subject array
for i = 1: length(sub)
    
    s = sub(i);
    
    % special code to run a single subject
    if contains(s.subj_basename, stage45.specifySubject )
        
        if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
            
            % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            % Error accessing 'C:\Data\SATdata\S04_POSTCOMP\batch1\D0003_rest_postcomp.set'
            % due to batch .mat
            
            % dynamic component removal using CSV file
            s.compRemove;
            
            % power analysis
            s.setFreqTable(); % requires input
            s.getPntsTable;
            s.generateStoreRoom;
            s.bandAverageTrials;
            s.visualization;
            
            %s.conn;
            %s.pac;
            
            %s.destoryStoreRoom;
            
            
            % End Analysis methods
            
            s.proc_state = 'level1';
            
            %results = magnitude_eeg( s.EEG.data );
            
            
        else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
        end
    end
    sub(i) = s;
    i
end


%% Stage 6
% between subjects
nbgroups = 2;
% cumulative rel_power
cum_rel_power_band = NaN(128, 6, length(sub), nbgroups); 
ave_rel_power_band = NaN(128, 6, length(sub), nbgroups); 

g1=0; g2=0;% g3=0; %
for i = 1: length(sub)
    
    s = sub(i);
        
    if ismember(i, objStageStatus)
        pxx = s.rest_rel_power_sum(1:161, :, :);
        % frequencies*trials*chans
        if strcmp(s.subj_subfolder, 'Condition23')
            gid = 1;
            % cumulative rel_power @ delta, theta, alpha1, alpha2, beta, gamma
            cum_rel_power_band(:, :, i, gid) = [squeeze(sum(mean(pxx(3:7, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(9:15, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(17:21, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(21:25, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(27:61, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(61:161, :, :), 2), 1))];
            % average rel_power @ delta, theta, alpha1, alpha2, beta, gamma
            ave_rel_power_band(:, :, i, gid) = [squeeze(mean(mean(pxx(3:7, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(9:15, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(17:21, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(21:25, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(27:61, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(61:161, :, :), 2), 1))];
            g1 = g1+1;
            
        elseif strcmp(s.subj_subfolder, 'Condition24')
            gid = 2;
            cum_rel_power_band(:, :, i, gid) = [squeeze(sum(mean(pxx(3:7, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(9:15, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(17:21, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(21:25, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(27:61, :, :), 2), 1)), ...
                squeeze(sum(mean(pxx(61:161, :, :), 2), 1))];

            ave_rel_power_band(:, :, i, gid) = [squeeze(mean(mean(pxx(3:7, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(9:15, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(17:21, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(21:25, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(27:61, :, :), 2), 1)), ...
                squeeze(mean(mean(pxx(61:161, :, :), 2), 1))];
            g2 = g2+1;
            
%         elseif strcmp(s.subj_id, 'Group3')
%             gid = 3;
%             cum_rel_power_band(:, :, i, gid) = [squeeze(sum(mean(pxx(3:7, :, :), 2), 1)), ...
%                 squeeze(sum(mean(pxx(9:15, :, :), 2), 1)), ...
%                 squeeze(sum(mean(pxx(17:21, :, :), 2), 1)), ...
%                 squeeze(sum(mean(pxx(21:25, :, :), 2), 1)), ...
%                 squeeze(sum(mean(pxx(27:61, :, :), 2), 1)), ...
%                 squeeze(sum(mean(pxx(61:161, :, :), 2), 1))];
% 
%             ave_rel_power_band(:, :, i, gid) = [squeeze(mean(mean(pxx(3:7, :, :), 2), 1)), ...
%                 squeeze(mean(mean(pxx(9:15, :, :), 2), 1)), ...
%                 squeeze(mean(mean(pxx(17:21, :, :), 2), 1)), ...
%                 squeeze(mean(mean(pxx(21:25, :, :), 2), 1)), ...
%                 squeeze(mean(mean(pxx(27:61, :, :), 2), 1)), ...
%                 squeeze(mean(mean(pxx(61:161, :, :), 2), 1))];
%             g3 = g3+1;
%         else
%             cprintf('err', '\n\nERROR: Wrong group id.\n');
        end
        
    end
end


% separate group 
cum_rel_power_g1 = cum_rel_power_band(:,:,~isnan(...
    cum_rel_power_band(randi(128),randi(6),:,1)),1);
% isnan returns an array the same size containing logical (vector index)
cum_rel_power_g2 = cum_rel_power_band(:,:,~isnan(...
    cum_rel_power_band(randi(128),randi(6),:,2)),2);
% cum_rel_power_g3 = cum_rel_power_band(:,:,~isnan(...
%     cum_rel_power_band(randi(32),randi(6),:,3)),3);

% group multiple comparison
chan_hood=spatial_neighbors(s.EEG.chanlocs,5.44,56); 
figure();nbband = 6;
bandTable = ["delta", "theta", "lower alpha", ...
    "upper alpha", "beta", "gamma"];
[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
% 6th argument: 0-unequal, 1-greater

index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(3,6,m);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
%     caxis([0 5]);
    colorbar
    title(bandTable(m))
end

[pval, t_orig, clust_info, seed_state, est_alpha] = clust_perm2_rui(...
    cum_rel_power_g1,cum_rel_power_g2,chan_hood,5000,.05,1,.05,2,[],1); 
index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(3,6,m+6);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
%     caxis([0 5]);
    colorbar
    title(bandTable(m))
end

[pval, t_orig, tmx_ptile, seed_state, est_alpha] = mxt_perm2(...
    cum_rel_power_g1,cum_rel_power_g2,5000,.05,1,2,[],1);

index = zeros(s.EEG.nbchan, nbband);
t_orig_sig = NaN(s.EEG.nbchan, nbband);
for m = 1:nbband
    index(:,m) = logical(pval(:,m)<est_alpha);
    t_orig_sig(:,m) = t_orig(:,m).*index(:,m);
    subplot(3,6,m+12);topoplot(t_orig_sig(:,m),s.EEG.chanlocs);
%     caxis([0 5]);
    colorbar
    title(bandTable(m))
end


%% between groups
cum_rel_power_group = squeeze(nanmean(cum_rel_power_band, 3)); 


% topoplot of group relative power 
figure();
for group=1:nbgroups
    
    subplot(2,6,(group-1)*6+1)
    [h1, grid1, plotrad1, xmesh1, ymesh1] = topoplot(cum_rel_power_group(:,1,group),s.EEG.chanlocs, 'conv', 'on');
    title(['cumulative delta band',' Ave=',...
        num2str(mean(cum_rel_power_group(:,1,group)))])
%     caxis([max(cum_rel_power_group(:,1,group))/2 max(cum_rel_power_group(:,1,group))]);colorbar
    caxis([0 .6]);colorbar

    subplot(2,6,(group-1)*6+2)
    [h2, grid2, plotrad2, xmesh2, ymesh2] = topoplot(cum_rel_power_group(:,2,group),s.EEG.chanlocs, 'conv', 'on');
    title(['cumulative theta band',' Ave=',...
     num2str(mean(cum_rel_power_group(:,2,group)))])
%     caxis([max(cum_rel_power_group(:,2,group))/4 max(cum_rel_power_group(:,2,group))*1.25]);colorbar
    caxis([0 .22]);colorbar

    subplot(2,6,(group-1)*6+3)
    [h3, grid3, plotrad3, xmesh3, ymesh3] = topoplot(cum_rel_power_group(:,3,group),s.EEG.chanlocs, 'conv', 'on');
    title(['cumulative alpha1 band',' Ave=',...
        num2str(mean(cum_rel_power_group(:,3,group)))])
%     caxis([max(cum_rel_power_group(:,3,group))/4 max(cum_rel_power_group(:,3,group))*1.25]);colorbar
    caxis([.03 .1]);colorbar

    subplot(2,6,(group-1)*6+4)
    [h4, grid4, plotrad4, xmesh4, ymesh4] = topoplot(cum_rel_power_group(:,4,group),s.EEG.chanlocs, 'conv', 'on');
    title(['cumulative alpha2 band',' Ave=',...
        num2str(mean(cum_rel_power_group(:,4,group)))])
%     caxis([max(cum_rel_power_group(:,4,group))/4 max(cum_rel_power_group(:,4,group))*1.25]);colorbar
    caxis([.02 .1]);colorbar

    subplot(2,6,(group-1)*6+5)
    [h5, grid5, plotrad5, xmesh5, ymesh5] = topoplot(cum_rel_power_group(:,5,group),s.EEG.chanlocs, 'conv', 'on');
    title(['cumulative beta band',' Ave=',...
        num2str(mean(cum_rel_power_group(:,5,group)))])
%     caxis([max(cum_rel_power_group(:,5,group))/2 max(cum_rel_power_group(:,5,group))*1.1]);colorbar
    caxis([0 .22]);colorbar

    subplot(2,6,(group-1)*6+6)
    [h6, grid6, plotrad6, xmesh6, ymesh6] = topoplot(cum_rel_power_group(:,6,group),s.EEG.chanlocs, 'conv', 'on');
    title(['cumulative gamma band',' Ave=',...
        num2str(mean(cum_rel_power_group(:,6,group)))])
%     caxis([max(cum_rel_power_group(:,6,group))/2 max(cum_rel_power_group(:,6,group))]);colorbar
    caxis([.08 .25]);colorbar
      
    
    
end
suptitle('Condition 23 vs 24')


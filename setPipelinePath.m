% Configuration script to correctly set path prior to running experiment.
% This is a temporary solution as we are working on the entire code

% Author: E. Pedapati
% October 8, 2018

% Selected Experiment Path

global pathsuccess; pathsuccess = 0;

try
% Anonymous functions for setting path
    % path with subdirectories
    agp = @(x) addpath(genpath(x));

    % add to path without subdirectories
    ap = @(x) addpath(x);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% experiment of choice, change path to experiment file

currentExperiment = 'experiments/htp';
agp(currentExperiment); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% general pipeline files    
    
    agp('config');
    agp('chanfiles');
    agp('functions');
    agp('subclasses');
    agp('superclasses');
    agp('docs');
    agp('templates');
    agp('external\eegdb');
    
    
% add correct eeglab

eeglabpath = which('eeglab.m');
eeglabpath = eeglabpath(1:end-length('eeglab.m'));
if isempty(eeglabpath), eeglabpath = 'ERROR. Add EEGLAB path to Matlab.'; end

ap(eeglabpath);



pathsuccess =1;

catch
    
    pathsuccess = 0;
    
end

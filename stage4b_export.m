function h1 = stage4b_export()
% This is the machine-generated representation of a Handle Graphics object
% and its children.  Note that handle values may change when these objects
% are re-created. This may cause problems with any callbacks written to
% depend on the value of the handle at the time the object was saved.
% This problem is solved by saving the output as a FIG-file.
% 
% To reopen this object, just type the name of the MATLAB file at the MATLAB
% prompt. The MATLAB code file and its associated MAT-file must be on your path.
% 
% NOTE: certain newer features in MATLAB may not have been saved in this
% file due to limitations of this format, which has been superseded by
% FIG-files.  Figures which have been annotated using the plot editor tools
% are incompatible with the MATLAB code file/MAT-file format, and should be saved as
% FIG-files.


load stage4b_export.mat


appdata = [];
appdata.FileMenuFcnLastExportedAsType = 5;
appdata.GUIDEOptions = struct(...
    'active_h', [], ...
    'taginfo', struct(...
    'text', 2, ...
    'checkbox', 2, ...
    'axes', 2, ...
    'pushbutton', 4), ...
    'override', 0, ...
    'release', 12, ...
    'resize', 'simple', ...
    'accessibility', 'on', ...
    'mfile', 0, ...
    'callbacks', [], ...
    'singleton', [], ...
    'syscolorfig', [], ...
    'blocking', 0, ...
    'lastFilename', 'C:\Users\ernes\Dropbox\bitbucket\htp_main\functions\gui\stage4b.fig');
appdata.lastValidTag = blanks(0);
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'figMenuHelp');

h1 = figure(...
'Units','normalized',...
'Position',[0.75 0.2 0.2 0.5],...
'Visible',get(0,'defaultfigureVisible'),...
'Color',get(0,'defaultfigureColor'),...
'CurrentAxesMode','manual',...
'CurrentObjectMode','manual',...
'SelectionTypeMode','manual',...
'IntegerHandle',get(0,'defaultfigureIntegerHandle'),...
'Colormap',get(0,'defaultfigureColormap'),...
'ColormapMode',get(0,'defaultfigureColormapMode'),...
'PaperPosition',[0.25 1.33333333333333 8 8.33333333333333],...
'PaperPositionMode',get(0,'defaultfigurePaperPositionMode'),...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'InvertHardcopyMode',get(0,'defaultfigureInvertHardcopyMode'),...
'ScreenPixelsPerInchMode','manual',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = blanks(0);

h2 = uitable(...
'Parent',h1,...
'Units','normalized',...
'Position',[0.05 0.45 0.9 0.5],...
'Data',{  'D1812231247' 'p' 'postcomps' 'D2665_rest-Copy(2)' blanks(0) 'Group 12' 'rest' 'DemoData' '32' '32' '[]' '58' '62' '0.5' '120' '1000' '256' '302.5039' '302.5039' '100' '299.4141' '2' '-1  1' '150' '7' '95.3333' '[11  15  16  17  18  19  26]' blanks(0) '32' '0' 'Yes' '1  3  4  6' 'D2665_rest-Copy(2)_import.set'; 'D1812231247' 'p' 'postcomps' 'D2665_rest-Copy(2)' blanks(0) 'Group 12 - Copy' 'rest' 'DemoData' '32' '31' '[20]' '58' '62' '0.5' '120' '1000' '256' '302.5039' '299.6836' '99.0677' '291.4297' '2' '-1  1' '146' '1' '99.3151' '[4]' '#1@2103(181); #2@2258(207); #3@2916(334); ' '31' '1' 'Yes' '1  3  4  5  8' 'D2665_rest-Copy(2)_import.set' },...
'CellSelectionCallback',mat{1},...
'Children',[],...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = blanks(0);

h3 = uipanel(...
'Parent',h1,...
'FontUnits',get(0,'defaultuipanelFontUnits'),...
'Units',get(0,'defaultuipanelUnits'),...
'Position',[0.05 0.02 0.9 0.41],...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'select_detail';

h4 = uicontrol(...
'Parent',h3,...
'FontUnits',get(0,'defaultuicontrolFontUnits'),...
'Units','normalized',...
'HorizontalAlignment','left',...
'String',{  'Static Text' },...
'Style','text',...
'Position',[0.0348837209302326 0.867283950617284 0.296511627906977 0.095679012345679],...
'Children',[],...
'Tag','select_detail',...
'FontSize',get(0,'defaultuicontrolFontSize'),...
'FontSizeMode',get(0,'defaultuicontrolFontSizeMode'),...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'pushbutton1';

h5 = uicontrol(...
'Parent',h3,...
'FontUnits',get(0,'defaultuicontrolFontUnits'),...
'Units','normalized',...
'String',{  'Push Button' },...
'Style',get(0,'defaultuicontrolStyle'),...
'Position',[0.0363372093023256 0.716049382716049 0.123546511627907 0.095679012345679],...
'Children',[],...
'Tag','pushbutton1',...
'FontSize',get(0,'defaultuicontrolFontSize'),...
'FontSizeMode',get(0,'defaultuicontrolFontSizeMode'),...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'pushbutton2';

h6 = uicontrol(...
'Parent',h3,...
'FontUnits',get(0,'defaultuicontrolFontUnits'),...
'Units','normalized',...
'String',{  'Push Button' },...
'Style',get(0,'defaultuicontrolStyle'),...
'Position',[0.0348837209302326 0.558641975308642 0.126453488372093 0.095679012345679],...
'Children',[],...
'Tag','pushbutton2',...
'FontSize',get(0,'defaultuicontrolFontSize'),...
'FontSizeMode',get(0,'defaultuicontrolFontSizeMode'),...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'pushbutton3';

h7 = uicontrol(...
'Parent',h3,...
'FontUnits',get(0,'defaultuicontrolFontUnits'),...
'Units','normalized',...
'String',{  'Push Button' },...
'Style',get(0,'defaultuicontrolStyle'),...
'Position',[0.0494186046511628 0.404320987654321 0.111918604651163 0.0895061728395062],...
'Children',[],...
'Tag','pushbutton3',...
'FontSize',get(0,'defaultuicontrolFontSize'),...
'FontSizeMode',get(0,'defaultuicontrolFontSizeMode'),...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );



% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end

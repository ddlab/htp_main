classdef eegDataClass < handle
    % eegDataClass: Generic EEG data superclass
    % extend with subclasses
    
    properties (Abstract)   
    end
    
    properties
        
        % File system variables keep file and directory structures equal
        filename = struct(...
            'raw','',   ...
            'import','',...
            'preica','',...
            'postica','',...
            'level1','',...
            'postcomps','',...
            'preanalysis','',...
            'group','',...
            'figs','',...
            'icaweights','',...
            'do','', ...
            'eventdata','');
        
        pathdb = struct(...
            'raw','',   ...
            'import','',...
            'preica','',...
            'postica','',...
            'level1','',...
            'postcomps','',...
            'preanalysis','',...
            'group','',...
            'figs','',...
            'icaweights','',...
            'do','',...
            'eventdata','');
        
        pathCellArr;
        
        % GUI variables
        includeInStudyList;
        includeInStudyType;
        resultTbl;
        
        % Study Specific Information
        study_id    = 0;
        study_title = 'Assign Study Title';
        study_desc  = 'Assign Study Description';
        study_type = 'rest';
        study_user = ''; % initials of the user processing
        study_csv = '';
        study_mat = '';
        
        % Subject Specific Information
        subj_id;           % consequtive subject id by process order
        subj_basename;     % generate unique prefix for subject
        subj_basepath;     % root path to datafiles
        subj_subfolder;    % subfolder/group of patient
        
        % Electrode configuration
        net_name;           % idenfier for net
        net_file;           % assign EEG channel file
        net_nbchan_orig;    % original number of channels
        net_nbchan_post;    % number of channels following pre-processing
        net_filter;         % file ext of recording system format (i.e. '*.raw')
        net_ref;            % reference electrode number
        net_regions;        % define subgroups of electrodes
        
        % Processing Parameters
        proc_timetag                 % time of dataset creation
        proc_clearEpochs;            % remove epochs following raw import
        proc_deleteMissingChans;     % remove raw channels without locations
        proc_dataRank;               % indenpendent data ranks following preprocessing
        proc_sRate1;                 % final sampling rate 1
        proc_sRate2;                 % final sampling rate 2 (i.e. for phase analysis)
        proc_sRate_raw;              % original sampling rate
        proc_contEpochLength;        % epochs for resting data
        proc_contEpochLimits;        % epoch limits including baseline
        proc_xmax_raw;               % original length before pre-procssing
        proc_xmax_post;              % length post cleaning
        proc_xmax_percent;           % length percentage due to cleaning
        proc_xmax_epoch;             % length following epoching
        proc_removed_regions;
        proc_state;                  % what stage file is in
        proc_removeComps;            % vector of components to be removed
        proc_comments;               % additional comments
        proc_fileStatus;             % current state of each directory
        proc_badchans;                % chans removed and interpolated
        proc_ipchans;                % number of interpolated channels
        
        proc_filt_bandlow;              % filter settings
        proc_filt_bandhigh
        proc_filt_lowcutoff;
        proc_filt_highcutoff;
        
        proc_icaweights;
        
        epoch_length;
        epoch_limits;
        epoch_trials;
        epoch_badtrials;
        epoch_badid;
        epoch_percent;
        
        % data structures
        EEG;
        EEG_raw;
        EEG_prefilt;
        EEG_prechan;
        EEG_import;
        EEG_preica;
        EEG_postica;
        EEG_postcomps;
        EEG_preanalysis;
        EEG_level1;
        
        % event data
        events_event;               % import from data channel in EEGLAB format
        events_urevent;               % import from data channel in EEGLAB format
        events_pnts;                 % num of pts in event file for QI
        
        % analysis variables
        sigPairs;       % unique channel pairs for connectivity analysis
        
        % manual clean variables
        man_chanRemove;
        
        % display strings
        str_plottitle; 
        
        % logging functions
        log_subjRow = {};
        log_subjHeader = {};
        
        % dipfit configuration
        dipfit;
        
    end
    
    
    methods (Abstract)
    end
    
    methods (Static = true)
    end
    
    methods
        function o = eegDataClass()
            % Superclass constructor
            % create eeglab default set file
            o.EEG = eeg_emptyset; % (EEGLAB function)
            
            try o.proc_timetag = evalin('base', 'timetag2');
            catch o.proc_timetag = '0000'; end
            
        end
        
        function o = setBasepath( o, pathStr )
            o.subj_basepath = pathStr;
        end
        
        function o = assignRawFile( o, subfolder, filename )
            % assign raw data filename to object
            [~, o.filename.raw, tmpExt]  = fileparts(filename);
            o.filename.raw = [o.filename.raw tmpExt];
            
            % assign subfolder/group
            o.subj_subfolder = subfolder;
            
            % create base, unique filename prefix
            
            % remove spaces from name
            filename(strfind( filename, ' ' )) = '';
            
            % remove path & ext info
            [~, o.subj_basename, ~]  = fileparts(filename);
            
            % add char D at beginning of each prefix
            o.subj_basename = ['D' o.subj_basename];
            
            rawfullfile = fullfile(o.pathdb.raw, o.subj_subfolder, o.filename.raw);
            
            if exist(rawfullfile, 'file') == 2              
            else
                fprintf('%s\nFile does not exist, check filename and path.', rawfullfile);
                
            end
            
        end
        
        function o = loadDataset(o, stage)
            if strcmp(stage, 'import') || strcmp(stage, 'import_trim')
                setfile = o.filename.import;
                path = fullfile(o.pathdb.import,o.subj_subfolder);
                
            elseif strcmp(stage, 'preica')
                setfile = o.filename.preica;
                path = fullfile(o.pathdb.preica,o.subj_subfolder);
                
            elseif strcmp(stage, 'postica')
                setfile = o.filename.postica;
                path = fullfile(o.pathdb.postica,o.subj_subfolder);
                
            elseif strcmp(stage, 'postcomps')
                setfile = o.filename.postcomps;
                path = fullfile(o.pathdb.postcomps,o.subj_subfolder);
                
            elseif strcmp(stage, 'preanalysis')
                setfile = o.filename.preanalysis;
                path = fullfile(o.pathdb.preanalysis,o.subj_subfolder);

            elseif strcmp(stage, 'level1')
                setfile = o.filename.level1;
                path = fullfile(o.pathdb.level1,o.subj_subfolder);

              elseif strcmp(stage, 'group')
                setfile = o.filename.group;
                path = fullfile(o.pathdb.group,o.subj_subfolder);    
               
            end
            
            o.EEG = pop_loadset( 'filename', setfile, 'filepath', path);
            
            o.EEG  = eeg_checkset( o.EEG );
            
            
        end
        
        function o = openDataset(o, open_directory, open_subfolder, open_filename)
            
            openfile = fullfile(open_directory, open_subfolder, open_filename);
            
            o.EEG = pop_loadset( 'filename', openfile);
            
        end
       
        function obj = removeData ( obj )
            
            obj.EEG = eeg_emptyset;
            obj.EEG_raw = eeg_emptyset;
            obj.EEG_prefilt = eeg_emptyset;
            obj.EEG_prechan = eeg_emptyset;
            obj.EEG_postcomps = eeg_emptyset;
            obj.EEG_preanalysis = eeg_emptyset;
            obj.EEG_preica = eeg_emptyset;
            obj.EEG_postica = eeg_emptyset;
            obj.EEG_import = eeg_emptyset;
            obj.EEG_level1 = eeg_emptyset;

            
        end
        
        function obj = unloadDataset ( obj )
            
            obj.EEG = eeg_emptyset;
            obj.EEG_raw = eeg_emptyset;
            obj.EEG_prefilt = eeg_emptyset;
            obj.EEG_prechan = eeg_emptyset;
            obj.EEG_postcomps = eeg_emptyset;
            obj.EEG_preanalysis = eeg_emptyset;
            obj.EEG_preica = eeg_emptyset;
            obj.EEG_postica = eeg_emptyset;
            obj.EEG_import = eeg_emptyset;
            obj.EEG_level1 = eeg_emptyset;
            
        end
        
        function o = createEpochs( o )
            
            arg_recurrence = o.proc_contEpochLength;
            arg_limits = o.proc_contEpochLimits;
            %o.EEG_preica = eeg_regepochs(o.EEG,'recurrence', ...
              %  arg_recurrence,'limits',arg_limits,'rmbase',NaN);
            o.EEG_preica = eeg_regepochs( o.EEG, 'recurrence', 2,'limits',[-2 0], 'extractepochs', 'on', 'rmbase', NaN );

            o.proc_xmax_epoch = o.EEG_preica.trials * abs(o.EEG_preica.xmax-o.EEG_preica.xmin);
            
            for i = 1 : length( o.EEG_preica.epoch )
                
                o.EEG_preica.epoch( i ).trialno = i;
                
            end

            o.EEG_preica  = eeg_checkset( o.EEG_preica );
            
            o.epoch_length = arg_recurrence;
            o.epoch_limits = arg_limits;
            o.epoch_trials = o.EEG_preica.trials;
            
        end
        

                
        function o = createFileNames( o )
            
            %o.filename.raw    = [o.subj_basename
            o.filename.import = [o.subj_basename '_import.set'];
            o.filename.preica = [o.subj_basename '_preica.set'];
            o.filename.postica = [o.subj_basename '_postica.set'];
            o.filename.level1 = [o.subj_basename '_level1.set'];
            o.filename.postcomps = [o.subj_basename '_postcomp.set'];
            o.filename.preanalysis = [o.subj_basename '_preanalysis.set'];

        end
        
        function [o, savefile] = saveDataset(o, EEG, path, stage )
            
            if ~exist('stage','var')
                % third parameter does not exist, so default it to something
                stage='default';
            end
            
            % set EEG setname
            if strcmp(stage,'import')
                EEG.setname = o.subj_basename;
                o.EEG.setname = o.subj_basename;
                o.filename.import = [EEG.setname '.set'];
                %o.filename.import = [path EEG.setname '.set'];
            elseif strcmp(stage,'import_trim') || strcmp (stage, 'preica') 
                EEG.setname = o.EEG.setname;
                %o.filename.import = [path EEG.setname '.set'];
                o.filename.import = [EEG.setname '.set'];
            elseif strcmp(stage, 'postcomps')|| strcmp (stage, 'preanalysis')
                
            end
            % create subfolder if does not exist
            workingdir = fullfile(path, o.subj_subfolder);
            if 7~=exist(workingdir,'dir'), status = mkdir(workingdir); end
            
            savefile = [EEG.setname '.set'];
            
            EEG = pop_saveset( EEG, 'filename', savefile, 'filepath', workingdir );
            
        end
        
        function o = storeDataset(o, EEG, save_directory, save_subfolder, save_filename)
            
            EEG.setname = save_filename;
            
            savefile = fullfile(save_directory, save_subfolder, save_filename);
            
            workingdir = fullfile(save_directory, save_subfolder);
            if 7~=exist(workingdir,'dir'), status = mkdir(workingdir); end
            
            
            EEG = pop_saveset( EEG, 'filename', savefile );
            cprintf('Magenta', 'Saved: %s\n', savefile);
            
        end
        
        function o = fplot( o )
            fastplot(o.EEG);
        end
        
        function o = createPaths( o, varargin )
            minArgs=0;
            maxArgs=2;
            
            narginchk(minArgs,maxArgs);
            
            if ~isempty(varargin)
                
                basepath = varargin{1};
                o.subj_basepath = basepath;
                
            else
                
                basepath = o.subj_basepath;
                
            end
            
            % define default directories of study
            % user should add only to the RAW folder
            o.pathdb.analysis        = fullfile(basepath, 'A00_ANALYSIS/');
            o.pathdb.raw             = fullfile(basepath, 'S00_RAW/');
            o.pathdb.import          = fullfile(basepath, 'S01_IMPORT/');
            o.pathdb.preica          = fullfile(basepath, 'S02_PREICA/');
            o.pathdb.postica         = fullfile(basepath, 'S03_POSTICA/');
            o.pathdb.postcomps       = fullfile(basepath, 'S04_POSTCOMP');
            o.pathdb.postanalysis    = fullfile(basepath, 'S04b_PREANALYSIS');
            o.pathdb.level1          = fullfile(basepath, 'S05_LEVEL1/');
            o.pathdb.group           = fullfile(basepath, 'S06_GROUP/');
            o.pathdb.figs            = fullfile(basepath, 'A01_FIGS/');
            o.pathdb.icaweights      = fullfile(basepath, 'ICAWEIGHTS');
            o.pathdb.do              = fullfile(basepath, 'dataobjects');
            
            
            o.pathCellArr = {};
            fns = fieldnames(o.pathdb);
            
            for i = 1:length(fns)
                
                o.pathCellArr{end+1} = o.pathdb.(fns{i});
                
            end
            
            md = @(x) mkdir(x);
            
            try
                
                for iPath = o.pathCellArr, md(iPath{1}); end
                
            catch
                fprintf('Please check data directory name.\n%s', basepath);
            end
            
        end
        
        function o = makePathDirectories( o )
            try
                md = @(x) mkdir(x);
                
                % create directory
                for iPath = o.pathCellArr, md(iPath{1}); end
                
            catch
                disp('Please check directory name.');
            end
            
            
        end
        
        function o = setUser( o, username )
            
            o.study_user = username;
            
        end
        
        function o = setCsv( o, csvname )
            
            o.study_csv = csvname;
            
        end
        
         function o = setMat( o, matname )
            
            o.study_mat = matname;
            
        end
        
        function o = updatePaths( o, basepath )
            
            % define default directories of study
            % user should add only to the RAW folder
            o.pathdb.analysis        = fullfile(basepath, 'A00_ANALYSIS/');
            o.pathdb.raw             = fullfile(basepath, 'S00_RAW/');
            o.pathdb.import          = fullfile(basepath, 'S01_IMPORT/');
            o.pathdb.preica          = fullfile(basepath, 'S02_PREICA/');
            o.pathdb.postica         = fullfile(basepath, 'S03_POSTICA/');
            o.pathdb.postcomps       = fullfile(basepath, 'S04_POSTCOMP');
            o.pathdb.preanalysis     = fullfile(basepath, 'S04b_PREANALYSIS');
            o.pathdb.level1          = fullfile(basepath, 'S05_LEVEL1/');
            o.pathdb.group           = fullfile(basepath, 'S06_GROUP/');
            o.pathdb.figs            = fullfile(basepath, 'A01_FIGS/');
            o.pathdb.icaweights      = fullfile(basepath, 'ICAWEIGHTS');
            o.pathdb.do              = fullfile(basepath, 'dataobjects');
            
            o.pathCellArr = {};
            fns = fieldnames(o.pathdb);
            
            for i = 1:length(fns)
                
                o.pathCellArr{end+1} = o.pathdb.(fns{i});
                
            end
            
            dataPath = {o.pathdb.analysis, o.pathdb.raw, o.pathdb.import, o.pathdb.preica, ...
                o.pathdb.level1, o.pathdb.postcomps, o.pathdb.preanalysis, o.pathdb.group, o.pathdb.figs, o.pathdb.icaweights};
            
        end
        
        function o = findProcFiles( o )
            % method locate current status of processed files
            % by using subfolders
            
            % create cell array of potential paths
            pathCellArr = {};
            fns = fieldnames(o.pathdb);
            
            for i = 1:length(fns)
                
                pathCellArr{end+1} = o.pathdb.(fns{i});
                
            end
            
            
            % define search directory
            
            searchIdx = [];
            
            searchIdx(end+1) = find(contains(pathCellArr,'A00_RAW'));
            searchIdx(end+1) = find(contains(pathCellArr,'A01_IMPORT'));
            searchIdx(end+1) = find(contains(pathCellArr,'A02_PREICA'));
            searchIdx(end+1) = find(contains(pathCellArr,'A03_POSTICA'));
            searchIdx(end+1) = find(contains(pathCellArr,'A04_POSTCOMPS'));
            searchIdx(end+1) = find(contains(pathCellArr,'A04b_PREANALYSIS'));
            searchIdx(end+1) = find(contains(pathCellArr,'A05_LEVEL1'));
            
            dispIdx = {};
            
            % search for files
            for i = 1:length(searchIdx)
                
                % create a search file for eeglab datasets + original
                testFile = fullfile(pathCellArr{searchIdx(i)}, ...
                    o.subj_subfolder, [o.subj_basename '.set']);
                
                endout=regexp(testFile,filesep,'split');
                
                stageName = regexp(pathCellArr{searchIdx(i)},filesep,'split');
                stageName = stageName{end-1};
                
                
                if  find(contains(endout,'A01_RAW')) > 0
                    
                    testFile = fullfile(pathCellArr{searchIdx(i)}, ...
                        o.subj_subfolder, o.filename.raw);
                    
                    if exist(testFile, 'file') == 2
                        file = dir(testFile);
                        dispIdx{end+1} = [stageName ': ' file.date ];
                        disp([testFile ' file exists']);
                    end
                    
                else
                    
                    
                    if exist(testFile, 'file') == 2
                        file = dir(testFile);
                        dispIdx{end+1} = [ stageName ': '  file.date  ];
                        disp([testFile ' file exists']);
                    else
                        dispIdx{end+1} = [ stageName ': Missing ' ];
                        disp([testFile ' does not exists']);
                    end
                    
                    
                    
                end
                
            end
            
            
            o.proc_fileStatus = dispIdx;
            
            
        end
        
        function o = ica( o )
            
            o.EEG = pop_runica(o.EEG,'icatype','binica', 'extended',1,'interupt','on','pca', o.EEG.nbchan);
            
        end
               
        function o = sigCreateUniqueChanPairs( o )
        % creates all unique pairs of electrodes for connectivity analysis    
            pairArr1 = 1:o.EEG.nbchan;
            pairArr2 = pairArr1;
            
            nPairArr = length(pairArr1);
            
            [pairArr1, pairArr2] = meshgrid(pairArr1, pairArr2);
            
            mask   = triu(ones(nPairArr), 1) > 0.5; % returns upper triangular part of matrix
            
            tmpPairs  = [pairArr1(mask) pairArr2(mask)];
            
            for i = 1:length(tmpPairs)
                
                distArr(i) = getPairDist( num2str(tmpPairs(i,1)), num2str(tmpPairs(i,2)), o.EEG);
                
            end
            o.sigPairs = [tmpPairs distArr'];
        end
        
        function o = sigChirp( o )
            
            for i=1:400
                rcrits(i,1)=sqrt(-(1/i)*log(.5));
            end
            
            channel2plot = '23';
            % find sensor index
            sensoridx = find(strcmpi(channel2plot,{o.EEG.chanlocs.labels}));
            
            
            data            = o.EEG.data(sensoridx,:,:);
            %data = mean(EEG.data([28,24],:,:),1);
            %data = mean(EEG.data([120,3,117,118,4,111],:,:),1);
            
            frames          = o.EEG.pnts;
            epochlim        = [-500 2000];
            srate           =  o.EEG.srate;
            cycles          = [1 30];
            winsize         = 100;
            nfreqs          = 119;
            freqs           = [2 120];
            timesout        = 250;
            
            [ersp1,itc,n2,t_s,f_s]=newtimef( data, frames, epochlim, o.EEG.srate,[1 30],...
                'winsize',100,'nfreqs',119,'freqs',[2 120], ...
                'plotersp','on','plotitc','on','verbose','off',...
                'baseline',NaN,'timesout',250);
            
            %disp(n)
            
            ITC1=(abs(itc))-rcrits(length(o.EEG.data(1,1,:)));
            
            
            
            figure; imagesc(t_s,f_s,ersp1), axis xy;
            figure; imagesc(t_s,f_s,ITC1), axis xy;
            
        end
        
        function o = plotBadChan( o )
            
            EEG = o.EEG;
            originalEEG = o.EEG_prechan;
            
            
            
            eegplot(EEG.data,'data2', originalEEG.data)

            
        end
        
        
        function o = manualChanClean( o )
            % open eegplot window to examine for bad channels
            
            gui.position = [0.01 0.20 0.95 0.70];


            % create plot of data
            eegplot(o.EEG.data,'srate',o.EEG.srate,'winlength',10, ...
                'plottitle', ['Step 1/3: Identify Bad Channels ' o.str_plottitle], ...
                'events',o.EEG.event,'color','on','wincolor',[1 0.5 0.5], ...
                'eloc_file',o.EEG.chanlocs,  'butlabel', 'Close Window', 'submean', 'on', ...
                'command', 't = 1', 'position', [400 400 1024 768] ...
                );
            
            % Formatting Main EEGPLOT
            h = findobj('tag', 'eegplottitle');
            h.FontWeight = 'Bold'; h.FontSize = 16; h.Position = [0.5000 0.93 0];
            
            % add gui elements for channel selection
            chanlist = {o.EEG.chanlocs.labels};
            
            % initialize bad channel array if no channels are selected
            o.proc_badchans = '';
                                    
            handle = gcf;
            handle.Units = 'normalized';
            handle.Position = gui.position;
            
          
            popup = uicontrol(handle,'Tag', 'chanselect', 'Style', 'listbox', ...
                'max',10,'min',1, ...
                'String', chanlist , 'Value', [],...
                'Units', 'normalized', ...
                'Position', [.05 0.15 0.035 .70], 'BackgroundColor', [0.94 0.94 0.94]);
            
            showBadDetail = uicontrol(handle,...
                'Tag', 'detailbutton', ...
                'Style', 'pushbutton', 'BackgroundColor', [0 1 1],...
                'Units', 'normalized', ...
                'Position', [0.7 0.08 0.10 0.05],...
                'String', 'Detail', 'Callback', @o.showChanDetail);
            
            toggleBadChannels = uicontrol(handle,...
                'Tag', 'savebutton', ...
                'Style', 'togglebutton', 'BackgroundColor', [0 1 0],...
                'Units', 'normalized', ...
                'Position', [0.8 0.08 0.14 0.05],...
                'String', 'Save', 'Callback', @o.selBadChan);
           
            textBadChannels = uicontrol(handle, 'Style', 'text', ...
                'String', 'Manual Bad Channel Rejection: no channels selected', 'Tag', 'badchantitle', ...
                'FontSize', 14,    'Units', 'normalized', 'Position', [0.1 0.86 0.5 0.03], 'HorizontalAlignment', 'left');
      
            waitfor(gcf);
            
            return;
            
        end

        function o = showChanDetail(o , src, event)
            
            EEG = o.EEG;
            
               
            ui_main = findobj(gcf,'Tag', 'EEGPLOT');
            h = findobj(gcf,'Style','togglebutton');
            hlist = findobj(gcf,'Style','listbox');
            htext = findobj(gcf,'Tag','badchantitle');
            
            ui_list = h.Value;
            ui_selection = hlist.Value;
            
            if ~isempty(ui_selection)
            
            pop_prop( EEG, 1, ui_selection, NaN, {'freqrange' [0.5 120] });
           
            end
            
        end
        
        function o = selBadChan(o, src,event)
            
            o.EEG_prechan = o.EEG;
            
            ui_main = findobj(gcf,'Tag', 'EEGPLOT');
            h = findobj(gcf,'Style','togglebutton');
            hlist = findobj(gcf,'Style','listbox');
            htext = findobj(gcf,'Tag','badchantitle');
            
            ui_list = h.Value;
            ui_selection = hlist.Value;
            
            if ui_list == 1
                ui_main.UserData.delchans = ui_selection;
                htext.String = 'Manual Bad Channel Rejection: ';
                htext.String = [htext.String num2str(ui_selection)];
                %fprintf(htext.String);
                htext.BackgroundColor = [0 1 0];
                o.proc_badchans = ui_selection;
            else
                ui_main.UserData.delchans = '';
                htext.String = 'Manual Bad Channel Rejection: no channels selected';
                htext.BackgroundColor = [0.93 0.93 0.93];
                o.proc_badchans = '';
            end
            
            
            
            %display(boxBadChannels.String);
            
            %ui_obj = get(gcf, 'Children');
        end
        
        function o = removeInterpolateChans( o )
            % convert bad channels to matrix
            
            
            badchannels = o.proc_badchans;
            
            EEG = o.EEG;  % original EEG
            EEG = eeg_checkset(EEG);

            EEGtemp = EEG;  % file to remove channels from
            
            if length(badchannels) >= 1
                               
                % remove bad channels
                EEGtemp = pop_select( EEG, 'nochannel', badchannels);
                
                % calculate correct data rank
                EEGtemp.etc.dataRank = rank(double(EEGtemp.data'));
                
                % interpolate bad channels;
                EEG = pop_interp(EEGtemp,eeg_mergelocs(EEG.chanlocs),'spherical');
                
                o.net_nbchan_post = EEGtemp.etc.dataRank;
                
            else
                
                EEG.etc.dataRank = EEG.nbchan;
                o.net_nbchan_post = EEG.etc.dataRank;
                
            end
            
            EEG = eeg_checkset(EEG);
            
            o.EEG = EEG;
            
        end
        
        function o = getBoundaryEvents( o, EEG )
           % provide EEG std
            events = EEG.event;
            
            cutIndex = strcmp({events.type}, 'boundary');
            cutIndexNo = find(cutIndex);
            
            latencies = [events(cutIndexNo).latency];
            durations = [events(cutIndexNo).duration];
            
            tmpstr = '';
            finalstr = '';
            
            for i = 1 : length(cutIndexNo)
                
                tmpstr = sprintf('#%0d@%.0f(%.0f); ', cutIndexNo(i), latencies(i), durations(i));
                
                finalstr = [finalstr tmpstr];
            end
            
             o.proc_removed_regions = finalstr;
            
            
        end
        
        function o = manualContClean( o )
            
            gui.position = [0.01 0.20 0.95 0.70];
            
            o.EEG = eeg_checkset(o.EEG);
            o.EEG_prechan = o.EEG;
            
            % Manual Rejection of Bad Sample Segments in Continuous Data
            %  Manually mark bad segments of continuous data in an interactive eegplot
            %  Returns the marked sample intervals in the variable 'V_Rejected_Sample_Range'
            
            
            global rej
            
            o.proc_removed_regions = [];
            
            eegplot(o.EEG.data,'srate',o.EEG.srate,'winlength',8, ...
                'plottitle', ['Step 2/3: Continuous Artifact Rejection'  o.str_plottitle], ...
                'events',o.EEG.event,'wincolor',[1 0.5 0.5], ...
                'command','global rej,rej=TMPREJ',...
                'eloc_file',o.EEG.chanlocs);
            
            
            
            handle = gcf;
            handle.Units = 'normalized';
            handle.Position = gui.position;
            
            % Formatting Main EEGPLOT
            h = findobj('tag', 'eegplottitle');
            h.FontWeight = 'Bold'; h.FontSize = 16; h.Position = [0.5000 0.93 0];
            
            
            
            waitfor(gcf);
            
            try
                
                if ~isempty(rej)
                    
                    tmprej = eegplot2event(rej, -1);
                    [o.EEG,~] = eeg_eegrej(o.EEG,tmprej(:,[3 4]));
                    
                    o.getBoundaryEvents(o.EEG); % updates proc_removed_regions

                else
                    o.proc_removed_regions = '';
                end
                
            catch
                
                cprintf('Red', '\nError in Manual Cleaning\n');
                
            end
            
            
            
            o.proc_xmax_raw = o.EEG_prechan.trials * (o.EEG_prechan.xmax-o.EEG_prechan.xmin);
            o.proc_xmax_post = o.EEG.trials * (o.EEG.xmax-o.EEG.xmin);
            o.proc_xmax_percent =  (o.proc_xmax_post / o.proc_xmax_raw) * 100;
            %o.proc_xmax_epoch
           
           o.EEG = eeg_checkset(o.EEG); 

            return;
            
        end
        
        function o = cleanEpochs( o )
            global rej;
            
            gui.position = [0.01 0.20 0.95 0.70];
            
            EEG = o.EEG_preica;
            % check epoched EEG
            EEG = eeg_checkset( EEG );
            
            eegplot(EEG.data,'srate',EEG.srate,'winlength',8, ...
                'events',EEG.event,'wincolor',[1 0.5 0.5], ...
                'plottitle', ['Step 3/3: Epoch Rejection'  o.str_plottitle], ...
                'command','global rej,rej=TMPREJ',...
                'eloc_file',EEG.chanlocs);
   
            % Formatting Main EEGPLOT
            h = findobj('tag', 'eegplottitle');
            h.FontWeight = 'Bold'; h.FontSize = 16; h.Position = [0.5000 0.93 0];
            
            
            handle = gcf;
            handle.Units = 'normalized';
            handle.Position = gui.position;
            
            %pop_eegplot( EEG, 1, 1, 0);
            waitfor(gcf);
            
            if ~isempty(rej)
                tmprej = eegplot2trial(rej, EEG.pnts, EEG.trials);
                EEG = eeg_checkset( EEG );
                o.epoch_badtrials = length(find(tmprej));
                o.epoch_badid = ['[' num2str(find(tmprej)) ']'];
                o.epoch_percent = 100-(o.epoch_badtrials / o.epoch_trials)*100;
            else
                
                o.epoch_badtrials = '';
                o.epoch_badid = '';
                o.epoch_percent = 100;
                
            end
            
            
            

            %EEG = eeg_rejsuperpose( EEG, 1, 1, 1, 1, 1, 1, 1, 1);
            %EEG = pop_rejepoch( EEG, tmprej ,0);
            
            o.EEG_preica = EEG;
            
        end
        
        function o = compRemove( o )
            
            % function removes component found in the proc_removeComps
            % EEG.reject.gcompreject matrix
            
            try
                EEG = o.EEG;
                
                compIdx = o.proc_removeComps;
                
                EEG.reject.gcompreject(1,compIdx) = 1;
                
                % component rejection operation
                EEG = pop_subcomp( EEG, compIdx, 0);
                
                cprintf('red', '\nComponents Removed: %s\n', num2str(o.proc_removeComps));
                       
            catch
                cprintf('red', '\nError: Component Removal Incomplete.', num2str(o.proc_removeComps));
                
            end
            
            
            o.EEG = EEG;
        end
        
        function o = sig_ispc( o )
            % load data
            %o.loadDataset('import');
            
            % define all possible unique pairs of channels
            
            o.sigCreateUniqueChanPairs;
            pairs = o.sigPairs;
            for iPair = 1:length(pairs)
                channel1 = num2str(pairs(iPair,1));
                channel2 = num2str(pairs(iPair,2));
                
                % specify some time-frequency parameters
                freqs2use  = logspace(log10(4),log10(30),15); % 4-30 Hz in 15 steps
                times2save = -400:10:1000;
                timewindow = linspace(1.5,3,length(freqs2use)); % number of cycles on either end of the center point (1.5 means a total of 3 cycles))
                baselinetm = [-400 -200];
                
                % wavelet and FFT parameters
                time          = -1:1/o.EEG.srate:1;
                half_wavelet  = (length(time)-1)/2;
                num_cycles    = logspace(log10(4),log10(8),length(freqs2use));
                n_wavelet     = length(time);
                n_data        = o.EEG.pnts*o.EEG.trials;
                n_convolution = n_wavelet+n_data-1;
                
                % time in indices
                times2saveidx = dsearchn(o.EEG.times',times2save');
                baselineidxF  = dsearchn(o.EEG.times',baselinetm');  % for the full temporal resolution data (thanks to Daniel Roberts for finding/reporting this bug here!)
                baselineidx   = dsearchn(times2save',baselinetm'); % for the temporally downsampled data
                
                chanidx = zeros(1,2); % always initialize!
                chanidx(1) = find(strcmpi(channel1,{o.EEG.chanlocs.labels}));
                chanidx(2) = find(strcmpi(channel2,{o.EEG.chanlocs.labels}));
                
                
                % data FFTs
                data_fft1 = fft(reshape(o.EEG.data(chanidx(1),:,:),1,n_data),n_convolution);
                data_fft2 = fft(reshape(o.EEG.data(chanidx(2),:,:),1,n_data),n_convolution);
                
                % initialize
                ispc    = zeros(length(freqs2use),o.EEG.pnts);
                pli     = zeros(length(freqs2use),o.EEG.pnts);
                wpli    = zeros(length(freqs2use),o.EEG.pnts);
                dwpli   = zeros(length(freqs2use),o.EEG.pnts);
                dwpli_t = zeros(length(freqs2use),length(times2save));
                ispc_t  = zeros(length(freqs2use),length(times2save));
                
                for fi=1:length(freqs2use)
                    
                    % create wavelet and take FFT
                    s = num_cycles(fi)/(2*pi*freqs2use(fi));
                    wavelet_fft = fft( exp(2*1i*pi*freqs2use(fi).*time) .* exp(-time.^2./(2*(s^2))) ,n_convolution);
                    
                    % phase angles from channel 1 via convolution
                    convolution_result_fft = ifft(wavelet_fft.*data_fft1,n_convolution);
                    convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
                    sig1 = reshape(convolution_result_fft,o.EEG.pnts,o.EEG.trials);
                    
                    % phase angles from channel 2 via convolution
                    convolution_result_fft = ifft(wavelet_fft.*data_fft2,n_convolution);
                    convolution_result_fft = convolution_result_fft(half_wavelet+1:end-half_wavelet);
                    sig2 = reshape(convolution_result_fft,o.EEG.pnts,o.EEG.trials);
                    
                    % cross-spectral density
                    cdd = sig1 .* conj(sig2);
                    
                    % ISPC
                    ispc(fi,:) = abs(mean(exp(1i*angle(cdd)),2)); % note: equivalent to ispc(fi,:) = abs(mean(exp(1i*(angle(sig1)-angle(sig2))),2));
                    
                    
                    % take imaginary part of signal only
                    cdi = imag(cdd);
                    
                    % phase-lag index
                    pli(fi,:)  = abs(mean(sign(imag(cdd)),2));
                    
                    % weighted phase-lag index (eq. 8 in Vink et al. NeuroImage 2011)
                    wpli(fi,:) = abs( mean( abs(cdi).*sign(cdi) ,2) )./mean(abs(cdi),2);
                    
                    % debiased weighted phase-lag index (shortcut, as implemented in fieldtrip)
                    imagsum      = sum(cdi,2);
                    imagsumW     = sum(abs(cdi),2);
                    debiasfactor = sum(cdi.^2,2);
                    dwpli(fi,:)  = (imagsum.^2 - debiasfactor)./(imagsumW.^2 - debiasfactor);
                    
                    % compute time window in indices for this frequency
                    time_window_idx = round((1000/freqs2use(fi))*timewindow(fi)/(1000/o.EEG.srate));
                    
                    for ti=1:length(times2save)
                        imagsum        = sum(cdi(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:),1);
                        imagsumW       = sum(abs(cdi(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:)),1);
                        debiasfactor   = sum(cdi(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:).^2,1);
                        dwpli_t(fi,ti) = mean((imagsum.^2 - debiasfactor)./(imagsumW.^2 - debiasfactor));
                        
                        % compute phase synchronization
                        phasesynch     = abs(mean(exp(1i*angle(cdd(times2saveidx(ti)-time_window_idx:times2saveidx(ti)+time_window_idx,:))),1));
                        ispc_t(fi,ti)  = mean(phasesynch);
                    end
                end
                
                % baseline subtraction from all measures
                ispc    = bsxfun(@minus,ispc,mean(ispc(:,baselineidxF(1):baselineidxF(2)),2)); % not plotted in the book, but you can plot it for comparison with PLI
                ispc_t  = bsxfun(@minus,ispc_t,mean(ispc_t(:,baselineidx(1):baselineidx(2)),2));
                pli     = bsxfun(@minus,pli,mean(pli(:,baselineidxF(1):baselineidxF(2)),2));
                dwpli   = bsxfun(@minus,dwpli,mean(dwpli(:,baselineidxF(1):baselineidxF(2)),2));
                dwpli_t = bsxfun(@minus,dwpli_t,mean(dwpli_t(:,baselineidx(1):baselineidx(2)),2));
                
                tbl_dwpli_t = array2table(dwpli_t);
                
                frqpli(iPair,:) = mean(ispc_t,2);
                
                
                frqdwpli(iPair,:) = mean(dwpli_t,2);
            end
            figure
            subplot(221)
            contourf(times2save,freqs2use,pli(:,times2saveidx),40,'linecolor','none')
            set(gca,'clim',[-.3 .3],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('PLI over trials')
            
            subplot(222)
            contourf(times2save,freqs2use,dwpli(:,times2saveidx),40,'linecolor','none')
            set(gca,'clim',[-.2 .2],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('dWPLI over trials')
            
            subplot(223)
            contourf(times2save,freqs2use,ispc_t,40,'linecolor','none')
            set(gca,'clim',[-.1 .1],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('ICPS over time')
            
            subplot(224)
            contourf(times2save,freqs2use,dwpli_t,40,'linecolor','none')
            set(gca,'clim',[-.1 .1],'yscale','log','ytick',round(logspace(log10(freqs2use(1)),log10(freqs2use(end)),8)))
            title('dWPLI over time')
            
        end
        
        function o = compsremove( o )
            
        s = o;    
            
        h.tp = gcf;
        h.tp.Position = [100 50 700 460];
        
        p = uipanel(h.tp,'Title','Component Selection Tool',...
            'Position',[.40 .1 .55 .22]); 
        
        %strStatus = sprintf('(%d of %d): %s', i, length(sub), s.subj_basename);
        
        title = uicontrol(p,'Style','text',...
            'String', strStatus,...
            'FontSize', 10,...
            'Units', 'normalized', ...
            'HorizontalAlignment', 'left', ...
            'BackgroundColor', [1 1 1], 'ForegroundColor', [0 0 0], ...
            'Position',[0.05 0.7 0.9 0.30]);
        
        b1 = uicontrol(p,'Style','pushbutton','String','Save Components',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback',@o.b1_callback, ...
            'BackgroundColor', [0 1 0], ...
            'Position',[.6 0.05 .35 .25]);
        
        t1 = uicontrol(p,'Style','edit',...
            'tag', 'comp_entry', ...
            'String','',...
            'Max',1,'Min',0,...
            'Units', 'normalized', ...
            'Position',[0.6 0.34 0.35 0.30]);
        
        b2 = uicontrol(p,'Style','pushbutton','String','C. Detail',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback', @o.b2_callback, ...
            'Position',[.05 0.05 .20 .25]);
        
        closebtn = findobj('tag', 'Set threhsolds', 'parent', h.tp);
        
        UIButtonArr = findobj(h.tp, 'Type', 'UIControl');
        OriginalButtons = findobj(UIButtonArr, 'BackgroundColor', '[0.6600 0.7600 1]');
        for button_i = 1 : length(OriginalButtons), OriginalButtons(button_i).Visible = 'off'; end
       % UI = findobj(UIButtonArr, 'String', 'OK');
        
       % okbutton.Visible = 'off';
        
        % loop with comp number
        
        for ri = 1 : maxcomps
        
        chbutton = findobj('tag', ['comp' num2str(ri)], 'Parent', h.tp);
        %disp(chbutton);
        chbutton.Callback = ['pop_prop_extended( s.EEG, 0,' num2str(ri) ')']';
                
        end
        
        %  delete(t2)
        t2 = uicontrol(p,'Style','edit',...
            'tag', 'comp_entry2', ...
            'String','',...
            'Max',1,'Min',0,...
            'Units', 'normalized', ...
            'Position',[0.05 0.34 0.20 0.30]);
        
        b3 = uicontrol(p,'Style','pushbutton','String','ICLABEL',...
            'Units','normalized',...
            'UserData', s, ...
            'Callback', @o.b3_callback, ...
            'Position',[.35 0.05 .20 .25]);
        
                        
        % Open component time series
        pop_eegplot( s.EEG, 0, 1, 1);
        h.ep = gcf;
        h.ep.Units = 'pixels';
        h.ep.Position = [100 550 700 450];
        
        g = h.ep.UserData;
        
        % adjust default spacing/zoom
        ESpacing = findobj('tag','ESpacing','parent', h.ep);   % ui handle
        ESpacing.String = 50;
        eegplot('draws',0);
        
        % adjust default number of epochs & components shown
        g.winlength = 10;
        g.dispchans = 10;
        
        if g.dispchans < 0 || g.dispchans > g.chans
            g.dispchans = g.chans;
        end
        
        set(h.ep, 'UserData', g);
        eegplot('updateslider', h.ep);
        eegplot('drawp',0);
        eegplot('scaleeye', [], h.ep);
        
        uiwait(h.ep);
        
        s.proc_state = 'postcomps';
        
        % save cleaned dataset into the preica directory
        s.storeDataset( s.EEG, pathdb.postcomps, s.subj_subfolder, s.filename.postcomps);
        
        
        % unload data & decrease memory footprint
        s.unloadDataset;
            
            
        end
    
        
        

function b2_callback(o, src, event)

comps = findobj('tag', 'comp_entry2');

EEG = src.UserData.EEG;

% pop_prop( EEG , 0, str2num(comps.String), NaN, {'freqrange' [2 50] });

pop_prop_extended( EEG, 0, str2num(comps.String));

%pop_selectcomps(src.UserData.EEG);

end

function b3_callback(o, src, event)

comps = findobj('tag', 'comp_entry2');

EEG = src.UserData.EEG;

EEG = pop_iclabel(EEG);

pop_viewprops( EEG, 0);

src.UserData.EEG = EEG;

%pop_selectcomps(src.UserData.EEG);

end


function b1_callback(o, src, event)

comps = findobj('tag', 'comp_entry');

src.UserData.proc_removeComps = str2num(comps.String);

close all;

end
        
        
        function o = quickplot( o, stage, type )
           
            
            
            o.loadDataset(stage);
            
            EEG = o.EEG;
            
            pop_eegplot(EEG, type, 1, 0);
            
            
        end
        
        
        function o = icview( o )
            
            EEG = o.EEG;
            
            EEG = pop_iclabel(EEG);
            
            o.EEG = EEG;
            
        end
        
        function o = generic_dipfit( o )
            
            if isempty(o.EEG.data)
                
                o.loadDataset('postcomps')
                
            end
            
            EEG = o.EEG;
            
            dipfit = o.dipfit;
            
            [EEG, com] = pop_dipfit_settings( EEG, ...
                'hdmfile',     dipfit.hdmfile, ...
                'coordformat', dipfit.coordformat, ...
                'mrifile',     dipfit.mrifile, ...
                'chanfile',    dipfit.chanfile, ...
                'coord_transform',[0.05476 -17.3653 -8.1318 0.075502 0.0031836 -1.5696 11.7138 12.7933 12.213] ,'chansel', dipfit.chansel);
            
            EEG = pop_multifit(EEG, dipfit.chansel ,'threshold',100,'plotopt',{'normlen' 'on'});
            
            o.EEG = EEG;
            
        end
        
        function o = set_dipfitcalc( o, status )
           
            o.dipfit.calc = status;
            
        end
        
        function o = plot_dipole( o )
            
            EEG = o.EEG;
            
            dipfit = o.dipfit;
            
            pop_dipplot( EEG, dipfit.compsel ,'mri', dipfit.mrifile,'normlen','on');

            o.EEG = EEG;
            
        end
        
        function o = set_dipfitsettings( o, hdmfile, mrifile )
           
            o.dipfit.hdmfile = hdmfile;
            o.dipfit.mrifile = mrifile;
            o.dipfit.coordformat = 'MNI';
            o.dipfit.chanfile = o.net_file;
            o.dipfit.chansel = 1: o.net_nbchan_orig;
            o.dipfit.compsel = 1 : o.net_nbchan_post;
            

        end
        
        function o = checkAvailableDatasets( o )
            
            fns = o.filename;
            pathdb = o.pathdb;
            subf = o.subj_subfolder;
            
            resultTbl(1).stage = 'import';
            resultTbl(1).available = isfile(fullfile(pathdb.import, subf, fns.import));
            
            resultTbl(2).stage = 'preica';
            resultTbl(2).available = isfile(fullfile(pathdb.preica, subf, fns.preica));
            
            resultTbl(3).stage = 'postica';
            resultTbl(3).available = isfile(fullfile(pathdb.postica, subf, fns.postica));
            
            resultTbl(4).stage = 'postcomps';
            resultTbl(4).available = isfile(fullfile(pathdb.postcomps, subf, fns.postcomps));
            
            resultTbl(5).stage = 'level1';
            resultTbl(5).available = isfile(fullfile(pathdb.level1, subf, fns.level1));
            
            o.resultTbl = resultTbl;
                    
        end
        
    end
end


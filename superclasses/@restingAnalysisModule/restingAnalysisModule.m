classdef restingAnalysisModule < handle
    % Object ext. for eegDataClass
    
    % standard interfaces for view/figure code
    % methods to generate figures
    
    properties (Abstract)   
    end    
    properties
        % gpu acceleration properties
        gpuOn; % 0 or 1; indicates compute capable GPU card is present
        
        subj_field;
        % result storage of resting analyses
                
        freqTable;
        pntsTable; 
        pks;
        locs;
               
        rest_abs_power;
        rest_rel_power; 
        rest_abs_norm_average_trials
        
        rest_abs_hz;
        rest_rel_hz;
        
        rest_abs_power_band_average_trials;
        rest_rel_power_band_average_trials;
        rest_abs_norm_band_average_trials
        
        rest_hilbert_complex;
        rest_hilbert_phase;
        rest_hilbert_power;

        rest_CSDdata;
        rest_dbwpli;
        
    end    
    methods (Abstract)
    end   
    methods (Static)
        
        function pnts = hz2pnts(hz, epoch_length)
            % pnts based on Freq & segment length in seconds
            pnts = epoch_length * hz + 1;
        end
                      
    end   
    methods
        
        function obj = restingAnalysisModule(  )
             % constructor
             obj.gpuOn = 0; % GPU off by default
        end
              
        
        function obj = setFreqTable( obj, newFreqMat ) 
                
            if nargin<2
                newFreqMat = [1,3; 4,7; 8,10; 10,12; 13,30; 30,80];
            end
                 
            EEG = obj.EEG;
            if size(newFreqMat,1)==6 && newFreqMat(6,2) <= EEG.srate/2
                % check if max frequency is permissible due to nyquist
                obj.freqTable = newFreqMat;
                
            else
                % return error if frequency too high
                cprintf('err', '\n\nERROR: Highest possible frequency lower than selected frequency band.\n');
                return
                
            end
        end
        
        
        function obj = getPntsTable( obj )

            epoch_length = obj.proc_contEpochLength; % inherit property
            freq = obj.freqTable;
            
            pnt(1,1) = restingAnalysisModule.hz2pnts(freq(1,1), epoch_length); 
            pnt(1,2) = restingAnalysisModule.hz2pnts(freq(1,2), epoch_length);
            pnt(2,1) = restingAnalysisModule.hz2pnts(freq(2,1), epoch_length); 
            pnt(2,2) = restingAnalysisModule.hz2pnts(freq(2,2), epoch_length);
            pnt(3,1) = restingAnalysisModule.hz2pnts(freq(3,1), epoch_length); 
            pnt(3,2) = restingAnalysisModule.hz2pnts(freq(3,2), epoch_length);
            pnt(4,1) = restingAnalysisModule.hz2pnts(freq(4,1), epoch_length); 
            pnt(4,2) = restingAnalysisModule.hz2pnts(freq(4,2), epoch_length);
            pnt(5,1) = restingAnalysisModule.hz2pnts(freq(5,1), epoch_length); 
            pnt(5,2) = restingAnalysisModule.hz2pnts(freq(5,2), epoch_length);
            pnt(6,1) = restingAnalysisModule.hz2pnts(freq(6,1), epoch_length); 
            pnt(6,2) = restingAnalysisModule.hz2pnts(freq(6,2), epoch_length);
            
            obj.pntsTable = pnt;
            
        end
 
 
        function obj = generateStoreRoom( obj )

            EEG = obj.EEG; 
            dat = EEG.data;
            data = permute(dat, [2, 1, 3]); % pnts*nchan*trials(nseg)
            datap = permute(data, [1, 3, 2]); % pnts*trials(nseg)*nchan

            if size(datap, 1) == EEG.pnts
                complex = zeros(size(datap));
                for chani=1:EEG.nbchan
                    for triali=1:EEG.trials
                        datapd = detrend(datap(:, triali, chani)); % detrend
                        complex(:, triali, chani) = fft(datapd.*hann(length(datapd)));
                    end
                end
            else
                cprintf('err', '\n\nERROR: Wrong dimension for FFT.\n');
            end
            
            
            % absolute & normalized power
            EEG.abs_power = 2*abs(complex(1:EEG.pnts/2,:,:)).^2/(EEG.pnts*EEG.srate); % uV^2/Hz
            % normalization #1
%             EEG.phase = angle(complex(1:EEG.pnts/2,:,:));

            pnt = obj.pntsTable;
            freq = obj.freqTable;
            EEG.rel_power = NaN*ones(size(EEG.abs_power));EEG.rel_power = EEG.rel_power(1:pnt(6,2),:,:);
            for i = 1:EEG.nbchan
                for j = 1:EEG.trials 

%                     EEG.rel_power_sum(1:EEG.srate/2+1, j, i) = EEG.abs_power(1:EEG.srate/2+1, j, i)./ ...
%                         sum(EEG.abs_power(1:EEG.srate/2+1, j, i)); % Jeste & Jun's
% relative power from here:
                    EEG.rel_power_sum(:, j, i) = EEG.abs_power(1:pnt(6,2), j, i)./ ...
                        sum(EEG.abs_power([ pnt(1,1):pnt(1,2),...
                                            pnt(2,1):pnt(2,2),...
                                            pnt(3,1):pnt(3,2),...
                                            pnt(4,1):pnt(4,2),...
                                            pnt(5,1):pnt(5,2),...
                                            pnt(6,1):pnt(6,2)], j, i)); 
                    % denominator: sum over target frequency bands
                    
                end
            end
            
%             cross_spectrum = NaN(EEG.trials, EEG.nbchan, EEG.nbchan, EEG.srate+1);
%             for k = 1:5%EEG.trials
%                 for i = 1:EEG.nbchan
%                     for j = 1:EEG.nbchan
%                         cross_spectrum(k, i, j, :) = cpsd(datap(:,k,i),datap(:,k,j), ...
%                             hann(1000),[],1000);
%                         
%                     end
%                 end
%             end
%             pli(i, j, :) = abs(squeeze(mean(sign(imag(cross_spectrum(:,:,:,1:161))))));
            
%             obj.rest_abs_power = 10*log10(EEG.abs_power); % save as dB
            obj.rest_abs_power = EEG.abs_power; % save as uV^2/Hz
%             obj.rest_fft_phase = EEG.phase;
            obj.rest_abs_hz = linspace(0, EEG.srate/2, EEG.pnts/2+1);
            obj.rest_rel_power = EEG.rel_power_sum;
            obj.rest_rel_hz = linspace(0, freq(6,2), pnt(6,2));
%             obj.rest_cpsd_pli = pli;
            
        end
        
        
        function obj = generateNormalizedPowerSpectrum( obj )
            abs = obj.rest_abs_power;
            hz = obj.rest_abs_hz;
            pnt = obj.pntsTable;
            abs_norm = zeros(pnt(6,2), size(abs, 2), size(abs, 3));
            for i=1:size(abs, 2)
                for j=1:size(abs, 3)
                    abs_norm(pnt(1,1):pnt(6,2),i,j) = 100*abs(pnt(1,1):pnt(6,2),i,j)./ ...
                        trapz(hz(pnt(1,1):pnt(6,2)),abs(pnt(1,1):pnt(6,2),i,j));
                    % removed DC
                end
            end
            abs_norm_average_trial = squeeze(mean(abs_norm, 2)); % freq*chan
            abs_norm_whole_brain_average_trial = squeeze(mean(abs_norm_average_trial, 2));
%             if all(diff(diff(abs_norm_whole_brain_average_trial(pnt(2,1):pnt(3,2))))>0)
%                 obj.pks = NaN;
%                 obj.locs = NaN;
%             elseif all(diff(abs_norm_whole_brain_average_trial(pnt(2,1):pnt(3,2)))>0)
%                 obj.pks = NaN;
%                 obj.locs = NaN;
%             elseif all(diff(abs_norm_whole_brain_average_trial(pnt(2,1):pnt(3,2)))<0)
%                 obj.pks = NaN;
%                 obj.locs = NaN;
%             else
%                 [obj.pks, obj.locs] = findpeaks(abs_norm_whole_brain_average_trial(pnt(2,1):pnt(3,2)),'NPeaks',1); 
%             end
            % average trials, average over channels
%             figure;plot(hz(pnt(1,1):pnt(6,2)), abs_norm_whole_brain_average_trial(pnt(1,1):pnt(6,2)));
% %             figure;plot(hz(1:pnt(6,2)), squeeze(mean(mean(rel(1:pnt(6,2),:,:),2),3)));
%             title(obj.subj_id)
%             xlabel('Freq (Hz)')
%             ylabel('Normalized power spectrum')
%             saveas(gcf,[obj.pathdb.figs, obj.subj_basename, '_whole brain_norm_power_spectrum.png']);
%             close
            
            obj.rest_abs_norm_average_trials = abs_norm_average_trial;
            obj.rest_abs_norm_band_average_trials = ...
                [mean(abs_norm_average_trial(pnt(1,1):pnt(1,2), :));
                 mean(abs_norm_average_trial(pnt(2,1):pnt(2,2), :));
                 mean(abs_norm_average_trial(pnt(3,1):pnt(3,2), :));
                 mean(abs_norm_average_trial(pnt(4,1):pnt(4,2), :));
                 mean(abs_norm_average_trial(pnt(5,1):pnt(5,2), :));
                 mean(abs_norm_average_trial(pnt(6,1):pnt(6,2), :))]; % 6*chan
                         
        end
        
        
        function obj = bandAverageTrials( obj )   
            
            abs_power = obj.rest_abs_power; % freq*trial*chan
            rel_power = obj.rest_rel_power;
            pnt = obj.pntsTable;
            
            % rectangular-rule within each freq band, then average over trials
            msum_abs_power_band = [mean(squeeze(0.5*sum(abs_power(pnt(1,1):pnt(1,2), :, :))));
                                mean(squeeze(0.5*sum(abs_power(pnt(2,1):pnt(2,2), :, :))));
                                mean(squeeze(0.5*sum(abs_power(pnt(3,1):pnt(3,2), :, :))));
                                mean(squeeze(0.5*sum(abs_power(pnt(4,1):pnt(4,2), :, :))));
                                mean(squeeze(0.5*sum(abs_power(pnt(5,1):pnt(5,2), :, :))));
                                mean(squeeze(0.5*sum(abs_power(pnt(6,1):pnt(6,2), :, :))))];
%                                 mean(squeeze(mean(abs_power(pnt(6,1):pnt(6,2), :, :))))];
          
            % sum within each freq band, average over trials
            sum_rel_power_band = [mean(squeeze(sum(rel_power(pnt(1,1):pnt(1,2), :, :))));
                                mean(squeeze(sum(rel_power(pnt(2,1):pnt(2,2), :, :))));
                                mean(squeeze(sum(rel_power(pnt(3,1):pnt(3,2), :, :))));
                                mean(squeeze(sum(rel_power(pnt(4,1):pnt(4,2), :, :))));
                                mean(squeeze(sum(rel_power(pnt(5,1):pnt(5,2), :, :))));
                                mean(squeeze(sum(rel_power(pnt(6,1):pnt(6,2), :, :))))];
           

%             ave_abs_power_band = [mean(squeeze(mean(10.^(abs_power(pnt(1,1):pnt(1,2), :, :)/10))));
%                 mean(squeeze(mean(10.^(abs_power(pnt(2,1):pnt(2,2), :, :)/10))));
%                 mean(squeeze(mean(10.^(abs_power(pnt(3,1):pnt(3,2), :, :)/10))));
%                 mean(squeeze(mean(10.^(abs_power(pnt(4,1):pnt(4,2), :, :)/10))));
%                 mean(squeeze(mean(10.^(abs_power(pnt(5,1):pnt(5,2), :, :)/10))));
%                 mean(squeeze(mean(10.^(abs_power(pnt(6,1):pnt(6,2), :, :)/10))))];
            
            obj.rest_rel_power_band_average_trials = sum_rel_power_band;
            obj.rest_abs_power_band_average_trials = msum_abs_power_band;
%             obj.rest_abs_power_band_average_trials = 10*log10(ave_abs_power_band);

        end
        
        
        function obj = powerVisualization( obj )
            
            EEG = obj.EEG;
            abs_pow = obj.rest_abs_power;abs_hz = obj.rest_abs_hz;
            rel_power_band = obj.rest_rel_power_band_average_trials; % 6*128
            abs_power_band = obj.rest_abs_power_band_average_trials; % 6*128
            
            pnt = obj.pntsTable;
            freq = obj.freqTable;
            nbEpochs = 4;
            nbRegions = size(obj.net_regions, 1);
            
            average_trial = squeeze(mean(EEG.data,2));
            variability = std(average_trial); % column corresponding to single trial
            [~, originalpos] = sort(variability, 'descend');
            selectTrials(1:4) = originalpos(1:4);

            
%             randTrials = randi(EEG.trials, [1, nbEpochs]); % vector of size nbEpoch
            EEG_region_dat = NaN(nbRegions, ...
                EEG.srate * obj.proc_contEpochLength, nbEpochs); % init
            bandTable = {'\delta', '\theta', 'lower \alpha', 'upper \alpha', '\beta', '\gamma'};
            rel_power_region_band = NaN(length(bandTable), nbRegions+1); % region per band readouts
            abs_power_region_band = NaN(length(bandTable), nbRegions+1); % added whole brain as 8th region
            
            for i = 1:nbRegions
                chan_region = obj.net_regions{i,3};
                EEG_region_dat(i,:,:) = mean(EEG.data(chan_region, :, selectTrials));
                % same random epoch per 7 regions
                rel_power_region_band(:, i) = mean(rel_power_band(:,chan_region), 2); 
                abs_power_region_band(:, i) = mean(abs_power_band(:,chan_region), 2); 
            end
            
            rel_power_region_band(:, 8) = mean(rel_power_band(:,:), 2);
            abs_power_region_band(:, 8) = mean(abs_power_band(:,:), 2);
            
            figure('units','normalized','outerposition',[0 0 1 1], 'visible', 'off');
            dim = [.1 .77 .45 .18]; % x y w h
            str = {['PROJECT: ', obj.study_title], ...
                ['DATE: ', obj.proc_timetag], ...
                ['SUBJECT: ', strrep(obj.subj_basename,'_','\_')], ...
                ['GROUP: ', obj.subj_subfolder], ...
                ['NUMBER OF CHANNELS: ', num2str(obj.net_nbchan_post), ' (', num2str(obj.net_nbchan_orig), ')'], ...
                ['BAD CHANNEL(S): [ ', num2str(str2num(obj.proc_badchans)), ' ]'], ...
                ['SAMPLING RATE: ', num2str(obj.proc_sRate1), ' (', num2str(obj.proc_sRate_raw), ') Hz']}; 
            annotation(gcf, 'textbox',dim, 'String',str,'FitBoxToText','on','FontSize',8);
            
            dim = [.35 .75 .45 .18]; % x y w h
            str = {['RAW DATA LENGTH: ', num2str(obj.proc_xmax_raw)], ...
                ['POST CLEANING DATA LENGTH: ', num2str(obj.proc_xmax_post)], ...
                ['POST CLEANING DATA PERCENTAGE: ', num2str(obj.proc_xmax_percent)], ...
                ['POST EPOCHING DATA LENGTH: ', num2str(obj.proc_xmax_epoch)]}; 
            annotation(gcf, 'textbox',dim, 'String',str,'FitBoxToText','on','FontSize',6);
            
            % power spectrum
            subplot(6,6,6)
            plot(abs_hz(1:pnt(6,2)),squeeze(mean(abs_pow(1:pnt(6,2),:,:),2)))
            xlabel('frequency (Hz)');ylabel('Magnitude (\muVdB)')
            axis tight;title('Power Spectrum')
            
            % butterfly plot
            
            subplot(6, 6, [7 8 9])
            plot(average_trial');axis tight
            xlabel('trials');ylabel('Amplitude (mV)')
            
            
            for i=1:2
            subplot(6, 6, i+3) 
            % ordered sample plot - random selected trials, region averaged
            plot(EEG_region_dat(1,:,i)'+70, 'Color', [.25, .25, .25]);alpha(0.2);hold on
            plot(EEG_region_dat(2,:,i)'+60, 'Color', [0 .5 0]);alpha(0.2)
            plot(EEG_region_dat(3,:,i)'+50, 'Color', [.85 .325 .098]);alpha(0.2)
            plot(EEG_region_dat(4,:,i)'+40, 'Color', [.301 .745 .933]);alpha(0.2)
            plot(EEG_region_dat(5,:,i)'+30, 'Color', [.635 .078 .184]);alpha(0.2)
            plot(EEG_region_dat(6,:,i)'+20, 'Color', [.929 .694 .125]);alpha(0.2)
            plot(EEG_region_dat(7,:,i)'+10, 'Color', [.494 .184 .556]);alpha(0.2)
            hold off; axis tight;set(gca,'xtick',[]);set(gca,'ytick',[])
            xlabel('2 sec')
            title(['Trial # ', num2str(selectTrials(i))]);
            end
            
            for i=3:4
            subplot(6, 6, i+7) 
            % ordered sample plot - random selected trials, region averaged
            plot(EEG_region_dat(1,:,i)'+70, 'Color', [.25, .25, .25]);alpha(0.2);hold on
            plot(EEG_region_dat(2,:,i)'+60, 'Color', [0 .5 0]);alpha(0.2)
            plot(EEG_region_dat(3,:,i)'+50, 'Color', [.85 .325 .098]);alpha(0.2)
            plot(EEG_region_dat(4,:,i)'+40, 'Color', [.301 .745 .933]);alpha(0.2)
            plot(EEG_region_dat(5,:,i)'+30, 'Color', [.635 .078 .184]);alpha(0.2)
            plot(EEG_region_dat(6,:,i)'+20, 'Color', [.929 .694 .125]);alpha(0.2)
            plot(EEG_region_dat(7,:,i)'+10, 'Color', [.494 .184 .556]);alpha(0.2)
            hold off; axis tight;set(gca,'xtick',[]);set(gca,'ytick',[])
            xlabel('2 sec')
            title(['Trial # ', num2str(selectTrials(i))]);
            end
            
            hSub = subplot(6, 6, 12);
            plot(NaN, 'Color', [.25, .25, .25]);alpha(0.2);hold on
            plot(NaN, 'Color', [0 .5 0]);alpha(0.2)
            plot(NaN, 'Color', [.85 .325 .098]);alpha(0.2)
            plot(NaN, 'Color', [.301 .745 .933]);alpha(0.2)
            plot(NaN, 'Color', [.635 .078 .184]);alpha(0.2)
            plot(NaN, 'Color', [.929 .694 .125]);alpha(0.2)
            plot(NaN, 'Color', [.494 .184 .556]);alpha(0.2)
            hold off; set(hSub, 'Visible', 'off');
            lgd = legend(hSub, obj.net_regions{1,1}, obj.net_regions{2,1}, 'SMA', ...
                obj.net_regions{4,1}, obj.net_regions{5,1}, obj.net_regions{6,1}, ...
                obj.net_regions{7,1});  
            lgd.FontSize = 5;
            
            
            
            % 3rd Row 
            for i=1:6
                subplot(6, 6, i+12)
                topoplot(abs_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                    'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
                caxis([min(abs_power_band(i, :)), max(abs_power_band(i, :))]);
                colorbar
                title(bandTable(i))
            end
            dim = [.05 .44 .2 .18]; % x y w h
            str = {'absolute power'};
            annotation(gcf, 'textbox',dim,  'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
            % 4th row
            i=1;
            subplot(6, 6, i+18)
            topoplot(rel_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
            caxis([0, .8]);
            colorbar
            title(bandTable(i))
            dim = [.14 .36 .2 .18]; % x y w h
            str = {['[',num2str(freq(1,1)),',', num2str(freq(1,2)),'] Hz']};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
            i=2;
            subplot(6, 6, i+18)
            topoplot(rel_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
            caxis([0, .5]);
            colorbar
            title(bandTable(i))
            dim = [.27 .36 .2 .18]; % x y w h
            str = {['[',num2str(freq(2,1)),',', num2str(freq(2,2)),'] Hz']};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
            i=3;
            subplot(6, 6, i+18)
            topoplot(rel_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
            caxis([0, .25]);
            colorbar
            title(bandTable(i))
            dim = [.40 .36 .2 .18]; % x y w h
            str = {['[',num2str(freq(3,1)),',', num2str(freq(3,2)),'] Hz']};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
            i=4;
            subplot(6, 6, i+18)
            topoplot(rel_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
            caxis([0, .25]);
            colorbar
            title(bandTable(i))
            dim = [.54 .36 .2 .18]; % x y w h
            str = {['[',num2str(freq(4,1)),',', num2str(freq(4,2)),'] Hz']};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
            i=5;
            subplot(6, 6, i+18)
            topoplot(rel_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
            caxis([0, .25]);
            colorbar
            title(bandTable(i))
            dim = [.67 .36 .2 .18]; % x y w h
            str = {['[',num2str(freq(5,1)),',', num2str(freq(5,2)),'] Hz']};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
            i=6;
            subplot(6, 6, i+18)
            topoplot(rel_power_band(i, :), EEG.chanlocs, 'whitebk','on', ...
                'headrad' ,0.25, 'maplimits','maxmin', 'headrad', .6)
            caxis([0, .5]);
            colorbar
            title(bandTable(i))
            dim = [.81 .36 .2 .18]; % x y w h
            str = {['[',num2str(freq(6,1)),',', num2str(freq(6,2)),'] Hz']};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);

            dim = [.05 .3 .2 .18]; % x y w h
            str = {'relative power'};
            annotation(gcf, 'textbox', dim, 'EdgeColor', 'none', ...
                'String',str,'FitBoxToText','on', 'FontSize', 8);
            
             
            % Table - readouts

%             str1 = sprintf('%.2s\t%.2s\t%.2f');
%             str1 = sprintf('%.2f', abs_power_region_band);

            regionNames = {obj.net_regions{1}; obj.net_regions{2}; 'SMA'; ...
                obj.net_regions{4}; obj.net_regions{5}; obj.net_regions{6};...
                obj.net_regions{7}; 'Cross brain'};
                                  
            delta = [abs_power_region_band(1,:)' rel_power_region_band(1,:)']; % abs vs rel
            theta = [abs_power_region_band(2,:)' rel_power_region_band(2,:)'];
            alpha1= [abs_power_region_band(3,:)' rel_power_region_band(3,:)'];
            alpha2= [abs_power_region_band(4,:)' rel_power_region_band(4,:)'];
            beta  = [abs_power_region_band(5,:)' rel_power_region_band(5,:)'];
            gamma = [abs_power_region_band(6,:)' rel_power_region_band(6,:)'];
            
            format shortg;
            delta = round(delta, 2); 
            theta = round(theta, 2); 
            alpha1 = round(alpha1, 2); 
            alpha2 = round(alpha2, 2); 
            beta = round(beta, 2); 
            gamma = round(gamma, 2); 
                     
            T = table(regionNames, delta, theta, alpha1, alpha2, beta, gamma);
            
            TString = evalc('disp(T)');
            % Use TeX Markup for bold formatting and underscores.
            TString = strrep(TString,'<strong>','\bf');
            TString = strrep(TString,'</strong>','\rm');
            TString = strrep(TString,'_','\_');
            % Get a fixed-width font.
            FixedWidth = get(0,'FixedWidthFontName');
            % Output the table using the annotation command.
            annotation(gcf,'Textbox','String',TString,'Interpreter','Tex',...
                'FontName',FixedWidth,'Units','Normalized','Position',[.1 0.02 .7 .2], ...
                'FontSize',8);
            
            tempfilename = [obj.subj_basename, 'pow_.png'];
            obj.filename.figs = tempfilename;
            
            saveas(gcf,[obj.pathdb.figs, tempfilename]);
            close
            format            
        end
        
        
        function obj = surfaceLaplacian( obj )
            EEG = obj.EEG;
            surfP_data = laplacian_perrinX(EEG.data, ...
                            [EEG.chanlocs.X],[EEG.chanlocs.Y], ...
                            [EEG.chanlocs.Z],[],1e-5);
            obj.rest_CSDdata = surfP_data; 
        end
        
        
        function obj = hilbert( obj )
            gpuOn = 1;
            freq = obj.freqTable;  
            EEG = obj.EEG;
            dat = gpuArray( double(EEG.data) );
           % dat = EEG.data;
%             dat = obj.rest_CSDdata;
            data = permute(dat, [2, 1, 3]); % timepnts*nchan*trials(nseg)
            datar = reshape(data, [], EEG.nbchan);
            
            if gpuOn == 1
           % hil_phase = NaN(EEG.pnts, EEG.trials, EEG.nbchan, size(freq, 1));
            hil_complex = NaN(EEG.pnts, EEG.trials, EEG.nbchan, size(freq, 1));
           % hil_pow = NaN(EEG.pnts, EEG.trials, EEG.nbchan, size(freq, 1));
            else
            hil_phase = NaN(EEG.pnts, EEG.trials, EEG.nbchan, size(freq, 1), 'gpuArray');
            hil_complex = NaN(EEG.pnts, EEG.trials, EEG.nbchan, size(freq, 1), 'gpuArray');
            hil_pow = NaN(EEG.pnts, EEG.trials, EEG.nbchan, size(freq, 1), 'gpuArray');
            end
            
%             bandTable = {'\delta', '\theta', 'lower \alpha', 'upper \alpha', '\beta', '\gamma'};
            

            for i = 1:size(freq,1)
                
                
                filt_order = round(9 * ( EEG.srate/freq(i, 1) ));
               % filterweights = fir1(filt_order,[freq(i,1), freq(i,2)]/(EEG.srate/2));
                
    %             % 2nd implementation - firls
    %             ffrequencies   = [0, (1-.05)*filtbound(1), filtbound(1), ...
    %                 filtbound(2), (1+.05)*filtbound(2), EEG.srate/2]/EEG.srate;
    %             filterweights = firls(filt_order, ffrequencies, [0, 0, 1, 1, 0, 0]);
    %             % phase response not linear
%                 figure;freqz(filterweights, 1, filt_order+1)
%                 % (b, a) - indicate an all-zero (FIR) filter

                % apply the filter kernal to the data to obtain the band-pass filtered signal
                
                % CPU route
               % filtered_data = filtfilt(filterweights, 1, datar); 
                
                % fft gpu filter
                d = designfilt('bandpassfir','FilterOrder', filt_order, ...
                    'CutoffFrequency1',freq(i,1),'CutoffFrequency2',freq(i,2), ...
                    'SampleRate', EEG.srate);
                
                B = d.Coefficients;
                
                filtered_data = fftfilt(gpuArray(B), datar);
               

                
               % filtered_data = fftfilt(filterweights, 1, datar); 
                
                                
                % GPU acceleration
                %filtgpu = double(datar) ;
                %forward_filt = filter(filterweights,1,  filtgpu);
                %reverse_filt = filter(filterweights,1,forward_filt(end:-1:1,:));
                %filtered_data = gather(reverse_filt(end:-1:1,:)); % must reverse time again!
                
                %   filtered_data = reverse_filt(end:-1:1,:); % must reverse time again!
                
                
                %filtered_data = FiltFiltM(filterweights, 1, double(datar));
                
                % Hint: data must have length more than 3 times the filter order.
                complex = zeros(EEG.pnts*EEG.trials, EEG.nbchan, 'gpuArray');
               % phase = zeros(EEG.pnts*EEG.trials, EEG.nbchan, 'gpuArray');
              %  pow = zeros(EEG.pnts*EEG.trials, EEG.nbchan, 'gpuArray');
                
                
                for chani=1:EEG.nbchan           
                    complex(:, chani) = hilbert(filtered_data(:, chani));
                 %   phase(:, chani) = angle(complex(:, chani));
                %    pow(:, chani) = abs(complex(:, chani));
                end
                
                filtered_data = [];
                
            %    hil_phase(:, :, :, i) = gather(reshape(phase, EEG.pnts, [], EEG.nbchan)); % pnts*trials*chan
            %    phase = [];
                hil_complex(:, :, :, i) = gather(reshape(complex, EEG.pnts, [], EEG.nbchan));
                complex = [];
             %   hil_pow(:, :, :, i) = gather(reshape(pow, EEG.pnts, [], EEG.nbchan));
             %   pow = [];
%                 figure;
%                 subplot(211);plot(hil_pow(:,1,1,i));title(bandTable(i))
%                 subplot(212);plot(hil_phase(:,1,1,i))
            end
            
            obj.rest_hilbert_complex = hil_complex;
          %  obj.rest_hilbert_phase = hil_phase; % pnts*trials*chan*6
          %  obj.rest_hilbert_power = hil_pow;
        end
        
        function obj = generateConnectivity( obj )
            % GPU Revisin
            hcomplex = zeros(size(obj.rest_hilbert_complex), 'gpuArray');
            hcomplex = gpuArray(obj.rest_hilbert_complex); % timepoints*ntrial*nchan*(6)subband
            nbchan = obj.EEG.nbchan;
            dbwpli_mat = gpuArray(NaN(nbchan,nbchan, 6));   

            % version 3
%             for chan2 = 2:nbchan
%                 mass2_rep = repmat(hcomplex(:,:,chan2,:), [1,1,nbchan,1]);
%                 cdd_mass = hcomplex .* conj(mass2_rep);
%                 x_in = sum(imag(cdd_mass).^2);
%                 x_out = sum(imag(cdd_mass)).^2; 
%                 x_out_abs = sum(abs(imag(cdd_mass))).^2;
%                 dbwpli = (x_out - x_in) ./ (x_out_abs - x_in);
%                 dbwpli_mat(:,chan2,:) = squeeze(mean(squeeze(dbwpli)));
%             end
         
            % version 2
%             parfor chan1 = 1:nbchan
%                 n = chan1+1;
%                 mass1 = squeeze(hcomplex(:,:,chan1,:)); % pnts*ntrial*band
%                 dbwpli_ave_trial = NaN(nbchan, 6);
%                 for chan2 = n:nbchan
%                     % vectorize trial and band
%                     mass2 = squeeze(hcomplex(:,:,chan2,:));
%                     cdd_mass = mass1.*conj(mass2); % pnts*ntrial*band
%                     dbwpli = (sum(imag(cdd_mass)).^2-sum(imag(cdd_mass).^2))...
%                         ./(sum(abs(imag(cdd_mass))).^2-sum(imag(cdd_mass).^2)); % 1*ntrials*bands
%                     dbwpli_ave_trial(chan2,:) = squeeze(mean(squeeze(dbwpli))); % vector of length 6
%                 end
%                 dbwpli_mat(chan1,:,:) = dbwpli_ave_trial;
%             end

            % version 1
            tic;
            for chan1 = 1:nbchan
                for chan2 = (chan1+1):nbchan
                    mass1 = squeeze(hcomplex(:,:,chan1,:)); % pnts*ntrial*band
                    mass2 = squeeze(hcomplex(:,:,chan2,:));
                    cdd_mass = mass1.*conj(mass2); % pnts*ntrial*band
                    dbwpli = (sum(imag(cdd_mass)).^2-sum(imag(cdd_mass).^2))...
                        ./(sum(abs(imag(cdd_mass))).^2-sum(imag(cdd_mass).^2)); % 1*ntrials*bands
                    dbwpli_mat(chan1,chan2,:) = squeeze(mean(squeeze(dbwpli))); % vector of length 6
                end    
            end
            toc;
            
            obj.rest_hilbert_complex = [];
            obj.rest_hilbert_phase = [];
            obj.rest_hilbert_power = [];

            obj.rest_dbwpli = gather(dbwpli_mat);
        end
                    
        function obj = generateConnectivities( obj )
            
            complex1 = obj.rest_hilbert_complex; % timepoints*ntrial*nchan*(6)subband
            nbchan = obj.EEG.nbchan;
            dbwpli_mat = NaN(nbchan,nbchan, 6);    
%             
%             tic;
%             for chan1 = 1:nbchan
%                 chan2 = chan1+chan1;
%                 
%                 mass3 = squeeze(complex1(:,:,chan1,:));
%                 
%                 cdd_mass = mass1 .* conj(mass2);
%                 
%                 dbwpli = (sum(imag(cdd_mass)).^2-sum(imag(cdd_mass).^2))...
%                     ./(sum(abs(imag(cdd_mass))).^2-sum(imag(cdd_mass).^2)); % 1*ntrials*bands
%                 dbwpli_mat(chan1,chan2,:) = mean(squeeze(dbwpli)); % vector of length 6
%                 
%                 
%             end
%             
%             toc;
%tic
% for chan2 = 2:nbchan
%     
%     % define mass2 gpu
%     m1gpu = gpuArray(complex1(:,:,:,:));
%    % m2gpu = m1gpu(:,:,chan2,:);
%    % m2gpu = repmat(m2gpu, [1,1,nbchan,1]);
%   %  cdd_mass = m1gpu .* conj(m2gpu);
%     
%     cdd_mass = m1gpu .* conj(repmat(m1gpu(:,:,chan2,:), [1,1,nbchan,1]));
%     
%     %cdd_mass = complex(zeros(1000, 166, 32, 6));
%    
%     %mass1 = complex1(:,:,:,:);
%     
%     %mass2 = complex1(:,:,chan2,:);
%     %mass2_rep = repmat(complex1(:,:,chan2,:), [1,1,nbchan,1]);
%     
%     %m1gpu = gpuArray(complex1(:,:,:,:));
%     %m2gpu = gpuArray(mass2_rep);
% 
%     %cdd_mass = bsxfun(@times, mass1, conj(mass2_rep));
%     %cdd_mass = complex1(:,:,:,:) .* conj(mass2_rep);
%     
%     
% 
%     clear m1gpu;
%     clear m2gpu;
%        
%    % x_in = sum(imag(cdd_mass).^2);
%    % x_out = sum(imag(cdd_mass)).^2; 
%    % x_out_abs = sum(abs(imag(cdd_mass))).^2;
%    
% 
%     dbwpli = (sum(imag(cdd_mass)).^2 - sum(imag(cdd_mass).^2)) ./ (sum(abs(imag(cdd_mass))).^2 - sum(imag(cdd_mass).^2));
%  clear cdd_mass;
%     %dbwpli = (x_out - x_in) ./ (x_out_abs - x_in);
% % 
% %     dbwpli = (sum(imag(cdd_mass)).^2 - sum(imag(cdd_mass).^2))...
% %         ./(sum(abs(imag(cdd_mass))).^2 - sum(imag(cdd_mass).^2)); % 1*ntrials*bands
% %     
%     %size(dbwpli)
%     
%     dbwpli_mat(:,chan2,:) = gather(mean(squeeze(dbwpli))); % vector of length 6
% 
%     
% end
% toc
     tic       
            for chan1 = 1:nbchan
               for chan2 = (chan1+1):nbchan
                
                    % vectorize trial and band
                    %mass1 = squeeze(complex(:,:,chan1,:)); % pnts*ntrial*band
                    %mass2 = squeeze(complex(:,:,chan2,:));
                    
                    %mass1 = complex(:,:,chan1,:); % pnts*ntrial*band
                       %mass2 = complex(:,:,chan2,:);
                    
                    %tic;
                    mass1 = gpuArray(squeeze(complex1(:,:,chan1,:))); % pnts*ntrial*band
                    mass2 = gpuArray(squeeze(complex1(:,:,chan2,:)));
                    cdd_mass = mass1 .* conj(mass2); % pnts*ntrial*band  
                   % toc;
                    
                    
                    
                    %clear cdd_mass;
                    
                    dbwpli = (sum(imag(cdd_mass)).^2-sum(imag(cdd_mass).^2))...
                        ./(sum(abs(imag(cdd_mass))).^2-sum(imag(cdd_mass).^2)); % 1*ntrials*bands
                    dbwpli_mat(chan1,chan2,:) = gather(mean(squeeze(dbwpli))); % vector of length 6
                    
                end
            end
            
            obj.rest_dbwpli = dbwpli_mat;
       toc;
        end
              
        
        function obj = sustained_conn( obj )
            EEG = obj.EEG;
            dat = EEG.data;
            for i = 1:nbchan
                for j = (i+1):nbchan
                    [xcf, lags, ~] = crosscorr(dat(i,:,k),dat(j,:,k));
                    [M, ~] = max(abs(xcf)); % maximal absolute value & index
                    xq = linspace(lags(1),lags(end),4*length(lags));
                    vq = interp1(lags,xcf,xq,'pchip'); % 'cubic' original
                    I = find ( vq == M/2); % hard to match!
                end
                % to be continued
            end
            obj.conn_sustained = conn;
        end

        
        function obj = fft_conn( obj )
            % connectivity analysis -- may not use in future     
            pnts = obj.pntsTable;
            fft_phases = obj.rest_fft_phase(1:pnts(6,2),:,:); % [-pi, pi]
            nbchan = obj.EEG.nbchan;
            trials = obj.EEG.trials;
            
            diff_phases = zeros(pnts(6,2), trials, nbchan, nbchan);
            % filled with pair-wise phase difference
            for i = 1:nbchan
                for j = (i+1):nbchan
                    diff_phases(:, :, i, j) = fft_phases(:, :, i)- ...
                        fft_phases(:, :, j);
                end
            end
            pli = abs(squeeze(mean(sign(imag(exp(1i*diff_phases))),2)));
            
            figure
            for i=1:6
                subplot(1,6,i)
                a = squeeze(mean(pli(pnts(i,1):pnts(i,2), :, :))); % 6th frequency component
                adjMat = (a - triu(a)) == 0;
                [ds.chanPairs(:, 1), ds.chanPairs(:, 2)] = ind2sub(size(adjMat), find(adjMat));
                alpha = .15;
                ds.connectStrength = a(adjMat);
                colormap('hot');
                topoplot_connect_r(ds, obj.EEG.chanlocs, alpha);
            end
%             obj.rest_pli = pli;
        end   
        
        
        function obj = complex_wavelet_conn( obj )
            EEG = obj.EEG;
            % "phase synchronization from same source"
            time=linspace(-1,1,2*EEG.srate+1);
            frequency =6;
            s = (4/(2*pi*frequency))^2; 
            wavelet = exp(2*1i*pi*frequency.*time) .* exp(-time.^2./(2*s)/frequency); %?
            
            % convolution preparation
            n_wavelet            = length(wavelet);
            n_data               = EEG.pnts;
            n_convolution        = n_wavelet+n_data-1;
            half_of_wavelet_size = (length(wavelet)-1)/2;
            % convolution
            fft_wavelet = fft(wavelet,n_convolution);
            % 
            phases = NaN(EEG.trials, EEG.nbchan, n_data);
            for i = 1:EEG.trials
                for j = 1:EEG.nbchan
                    fft_data = fft(squeeze(EEG.data(j,:,i)),n_convolution); 
                    convolution_result_fft = ifft(fft_wavelet.*fft_data,n_convolution) * sqrt(s);
                    convolution_result_fft = convolution_result_fft(half_of_wavelet_size+1: ...
                                                                end-half_of_wavelet_size);
                    phases(i, j, :) = angle(convolution_result_fft);
                end
            end
        end
      
        
        function obj = regroup( obj )
            
            if strcmp(obj.subj_subfolder, 'Condition22')
                newlabel = 'S01D02';
            elseif strcmp(obj.subj_subfolder, 'Condition23')
                newlabel = 'S01D01';
            elseif strcmp(obj.subj_subfolder, 'Condition24')
                newlabel = 'S02D01';
            elseif strcmp(obj.subj_subfolder, 'Condition25')
                newlabel = 'S02D02';
            elseif strcmp(obj.subj_subfolder, 'Condition26')
                newlabel = 'S01D03';
            elseif strcmp(obj.subj_subfolder, 'Condition28')
                newlabel = 'S02D03';
            elseif strcmp(obj.subj_subfolder, 'Condition21')
                newlabel = 'S01D04';
            elseif strcmp(obj.subj_subfolder, 'Condition27')
                newlabel = 'S02D04';
            else
                newlabel = [];
            end
            
            obj.subj_field = newlabel;
        end
            
            
        function obj = destoryStoreRoom (obj)
            % ???
             obj.rest_complex = [];
             
        end
         
                   
         
              
    end
end

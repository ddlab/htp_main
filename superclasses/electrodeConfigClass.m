classdef electrodeConfigClass < handle
    % Class to define electrode configuration
    % can be loaded from a CSV file
    
    properties
        
        net_displayname;
        net_name;
        net_modelno;
        net_graphic;
        net_file;
        net_filter;
        net_regions;
        net_nochans;
        net_desc;
        net_notes;
        
    end
    
    methods
        function o = electrodeConfigClass
            
        end
    end
end


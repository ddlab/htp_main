
function saveresult = save_comp_plot(EEG, comps, epochs, subfolder, fn, filepath, mode)

% create 2 filenames
fn1 =''; fn2 = '';
fn1 = [fn '_A03b_' mode '_Scroll.png'];
fn2 = [fn '_A03b_' mode '_Topo.png'];

% new filename for joined image
savefile = sprintf('%s_%s_S3b_comp_%d_%d.png', fn, char(subfolder), comps(1), comps(end));

% create filename w/ path
savefile1 = fullfile(filepath, filesep(), fn1);
savefile2 = fullfile(filepath, filesep(), fn2);

% display scroll plot
% get component data
tmpdata = eeg_getdatact(EEG, 'component', [1:size(EEG.icaweights,1)]);

% plot component data
eegplot( tmpdata(comps,:,:), 'srate', EEG.srate, 'title', 'Scroll component activities -- eegplot()', ...
    'limits', [EEG.xmin EEG.xmax]*1000);

% ------ adjust component plot-------------------------------------

% get user data on plot = g
g = get(gcf,'UserData');
g.spacing       = 20;
g.winlength     = epochs(end) - epochs(1);
g.dispchans     = comps(end);
g.position      = epochs(1);

ESpacing = findobj('tag','ESpacing','parent',gcf);   % ui handle
EPosition = findobj('tag','EPosition','parent',gcf); % ui handle
set(ESpacing,'string',num2str(g.spacing,4))
set(EPosition,'string',num2str(g.position,4))

% location of y axis (channels)
g.elecoffset = g.chans-g.dispchans;

% update plot setting
set(gcf, 'userdata', g);

% get axis to fix following plot
ax1 = findobj('tag','eegaxis','parent',gcf);         % axes handle
%data = get(ax1, 'userdata');

% set max min of y axis
set(ax1,'YLim',[0 (g.chans+1)*g.spacing],'YTick',[0:g.spacing:g.chans*g.spacing])
set(ax1, 'ylim',[g.elecoffset*g.spacing (g.elecoffset+g.dispchans+1)*g.spacing] );

% slider bar
%h = findobj(gcf, 'tag', 'eegslider');
%set(h, 'visible', 'on');

eegplot('drawp', 0);

%
% save figure
saveas(gcf, savefile1);
% close scrollplot
close gcf;

% display topo plot
pop_topoplot(EEG,0, comps, ['Component Topography: ' EEG.filename],...
    [3 5] ,0,'electrodes', 'on', 'masksurf', 'on');
%title(sprintf('EEG Time Series\nSubject: %s\n', savefile))

% adjust topo plot

% save figure
saveas(gcf, savefile2);

% close topo plot
close gcf;

% ====================


plot_stitch(fn, savefile, filepath);

saveresult = 1;

end
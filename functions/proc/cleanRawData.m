function obj = cleanRawData( obj, param )

   %EEG = clean_rawdata(EEG, 5, -1, 0.85, 4, 20, 0.25);
   % Assign EEG from object
    EEG = obj.EEG;

    % count of channels with locations (i.e. avoids EOG)
    % weighted functions will crash otherwise
    chan_w_locs = length(find([EEG.chanlocs(:).X]));

    % create 2 vectors for candidate channels
    vchan        = 1 : EEG.nbchan;
    vchan_w_locs = 1 : chan_w_locs;
    
    % Create backup of original channels
    originalEEG = EEG;

    try
        EEG = clean_rawdata(EEG, ... 
            param.arg_flatline, param.arg_highpass, ...
            param.arg_channel, param.arg_noisy, ...
            param.arg_burst, param.arg_window);
    catch
        disp('Error in Channel Cleaning');
    end

    EEG = pop_interp(EEG, originalEEG.chanlocs, 'spherical');

    obj.EEG = EEG;
end
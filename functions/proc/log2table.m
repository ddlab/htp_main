function subjTable = log2table( sub, pathdb )

timetag2    = datestr(now,'yymmddHHMM');
subjHeader = regexprep(sub.log_subjHeader, '\.', '') ;
subjTable.Properties.VariableNames = subjHeader;

subjTable = cell2table(vertcat(sub(i).log_subjRow));
saveFN = [pathdb.analysis 'A' timetag2 '_subjTable'];

writetable(subjTable, fullfile([saveFN '.csv']));
objSaveFile = fullfile([saveFN '.mat']);
save(objSaveFile, 'sub');

end
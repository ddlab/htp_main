function EEG = manualRejectSegments( EEG )

EEG = eeg_checkset( EEG );

global rej
eegplot(EEG.data,'srate',EEG.srate,'winlength',8, ...
        'events',EEG.event,'color','on','wincolor',[1 0.5 0.5], ...
        'command','global rej,rej=TMPREJ',...
        'eloc_file',EEG.chanlocs);
waitfor(gcf);
tmprej = eegplot2event(rej, -1);
[EEG,~] = eeg_eegrej(EEG,tmprej(:,[3 4]));

% txt_cell_array = {EEG.chanlocs.labels};
% h_list = uicontrol('style','listbox','max',5,...
%    'min',1,...
%    'string',txt_cell_array);


end

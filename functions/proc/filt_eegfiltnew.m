function [EEG, com, b] = filtData( EEG, param )
%FILTDATA Summary of this function goes here
%   Detailed explanation goes here
% [EEG, com, b] = pop_eegfiltnew(EEG, locutoff, hicutoff, filtorder,
%                                      revfilt, usefft, plotfreqz, minphase);
locutoff    = param.locutoff;
hicutoff    = param.hicutoff;
filtorder   = param.filtorder;
revfilt     = param.revfilt;
plotfreqz   = param.plotfreqz;
minphase    = param.minphase;

[EEG, com, b] =  pop_eegfiltnew( EEG, locutoff, hicutoff, filtorder, revfilt, plotfreqz, minphase);

EEG = eeg_checkset( EEG );

end


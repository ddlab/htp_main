function param = CleanRawDataInit()

param.arg_flatline  = 5; % 5
param.arg_highpass  = [0.25 0.75];
param.arg_channel   = 0.85;
param.arg_noisy     = 4;
param.arg_burst     = 5;
param.arg_window    = 0.25;

end

function filename = createResultsCsv( sub, stage )

timetag2    = datestr(now,'yymmddHHMM');

switch stage

    case 'level1'
        
        desc = 'Stage5';
        msg = 'FIVE';
        outputrowstage = 'level1';
        
    case 'preanalysis'
        desc = 'Stage4b';
        msg = 'FOUR-B';
        outputrowstage = 'preanalysis';
    
    case 'postcomps'
        
        desc = 'Stage4';
        msg = 'FOUR';
        outputrowstage = 'postcomps';
        
    case 'postica'
        
        desc = 'Stage3';
        msg = 'THREE';
        outputrowstage = 'postica';
        
        
    case 'preica'
        
        desc = 'Stage2';
        msg = 'TWO';
        outputrowstage = 'preica';
        
        
    case 'import'
        
        desc = 'Stage1';
        msg = 'ONE';
        outputrowstage = 'import';
        
    case 'combined'

        desc = 'Stage3';
        msg = 'FOUR';
        outputrowstage = 'postcomps';    
        
end

pathdb = sub(1).pathdb;

subjHeader = regexprep(sub(1).log_subjHeader, '\.', '') ;

saveFN = [pathdb.analysis 'A' timetag2 '_subjTable_' desc];

for i = 1 : length(sub)
    %sub(i).outputRow(outputrowstage); 
    subjTable(i,:) = cell2table(vertcat(sub(i).log_subjRow)); 
end

subjTable.Properties.VariableNames = subjHeader;

csvfile = fullfile([saveFN '.csv']);

writetable(subjTable, csvfile );
%objSaveFile = fullfile([saveFN '.mat']);
objSaveFile = fullfile([saveFN '.mat']);
save(objSaveFile, 'sub');

filename = objSaveFile;


cprintf('*blue','\nSTAGE %s COMPLETE\n', msg); 
cprintf('*blue','\nTable of Results:\n%s\n', csvfile); 
cprintf('*blue','\nFile of Data Objects:\n%s\n\n',objSaveFile); 


end

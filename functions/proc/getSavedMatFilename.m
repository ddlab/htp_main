function matfile = getSavedMatFilename( file, path )

matfile = [path file(1:end-length('.csv')) '.mat'];

end
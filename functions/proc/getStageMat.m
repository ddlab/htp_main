

% HIGH THROUGHPUT PIPELINE (HTP)
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Get Stage CSV
% FILE: getStageCSV.m
%
% NOTES:


function [csvfile, matfile, pathdb]  = getStageMat( stage, basePath )

configObject = RestEegDataClass( );
configObject.updatePaths( basePath );
pathdb = configObject.pathdb;

switch stage
    
    case 'raw'
        filter = '';
        caption = 'Stage 1 Raw Import';
        
    case 'import'
        filter = '*Stage*.csv';
        caption = 'Select Stage 1 (Import) or Stage 2 (Partial) Subject Log';
        
    case 'preica'
        filter = '*Stage*.csv';
        caption = 'Select Stage 2 (Preica) or Stage 3 (Partial) Subject Log';
        
    case 'postica'
        
        filter = '*Stage3.csv';
        caption = 'Select Stage 3 (PostICA) or Stage 4 (Partial) Subject Log';
        
    case 'postcomps'
        filter = '*Stage*.csv';
        caption = 'Select Stage 4 (PostComps) or Stage 5 (Partial) Subject Log';
        
    case 'level1'
        filter = '*Stage*.csv';
        caption = 'Select Stage 5 (Level1) or Stage 6 (Partial) Subject Log';
        
    case 'group'
        filter = '*Stage*.csv';
        caption = 'Select Stage 6 (Partial) Subject Log';
        
        
end

[file,path] = uigetfile(fullfile(pathdb.analysis,filter), ...
    caption);

csvfile = fullfile(path,file);
matfile = [path file(1:end-length('.csv')) '.mat'];


end

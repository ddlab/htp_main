

function pathdb = getLocalPaths( basePath )

configObject = RestEegDataClass( );
configObject.createPaths( basePath );
pathdb = configObject.pathdb;

end

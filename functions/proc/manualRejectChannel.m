
function EEG = manualRejectChannel( EEG )

EEG = eeg_checkset( EEG );

% open multicolor eegplot

eegplot(EEG.data,'srate',EEG.srate,'winlength',8, ...
        'plottitle', 'Identify Bad Channels', ...  
        'winlength', 10,  ...
        'events',EEG.event,'color','on','wincolor',[1 0.5 0.5], ...
        'command','global rej,rej=TMPREJ',...
        'eloc_file',EEG.chanlocs,  'butlabel', 'Close Window', 'submean', 'on', ...
        'command', 'test = get(gcf,''UserData'')', 'position', [0 0 800 600] ...
        );
  set(gcf,'Name', 'Identify Bad Channels');
  handle = gcf;
  %set(gcf,'Units','pixels');

  popup = uicontrol(gcf,'Style', 'listbox','max',10,'min',1, ...
            'String', {EEG.chanlocs.labels} , 'Value', [],...
            'Position', [3 3 50 400]);
           %'Position', [handle.Position(1,3)-50 50 50 500]);      
  buttonBadChannels = uicontrol(gcf,'Style', 'togglebutton', 'Position', [100 50 100 20], 'String', 'Save', 'Callback', @selectBadChans);
       
  % textBadChannels = uicontrol('Style', 'text', 'String', 'Enter Bad Channels: ', 'Position', [10 50 150 20]);      
  % boxBadChannels  = uicontrol('Style', 'edit', 'Value', [],...
   %          'Position', [150 50 200 20]);
          

global rej

marker = 'Enter Bad Channel: ';

    % 'dispchans', 20,
waitfor(gcf);
tmprej = eegplot2event(rej, -1);
[EEG,~] = eeg_eegrej(EEG,tmprej(:,[3 4]));

% txt_cell_array = {EEG.chanlocs.labels};
% h_list = uicontrol('style','listbox','max',5,...
%    'min',1,...
%    'string',txt_cell_array);
set(0,'Units','pixels')
scnsize = get(0,'ScreenSize');
bdwidth = 5;
topbdwidth = 30;
% pos1  = [bdwidth,... 
%     2/3*scnsize(4) + bdwidth,...
%     scnsize(3)/2 - 2*bdwidth,...
%     scnsize(4)/3 - (topbdwidth + bdwidth)];

pos1 = [0 0 800 600]
set(gcf,'Units','pixels');
get(gcf,'Position'); 
set(gcf,'Position', pos1);
OuterPosition: [-8 -8 816 666]
set(gcf,'InnerPosition', [-8 -8 1200 1200]);

  
    

         jScrollPane = findobj(popup)
         
% Set the mouse-click callback
% Note: MousePressedCallback is better than MouseClickedCallback
%       since it fires immediately when mouse button is pressed,
%       without waiting for its release, as MouseClickedCallback does
set(jListbox, 'MousePressedCallback',{@myCallbackFcn,hListbox});


%function myui
    % Create a figure and axes
    f = figure('Visible','off');
    ax = axes('Units','pixels');
    surf(peaks)
      Colors_to_choose = {'parula','jet','hsv','hot','cool','gray'};
      % Create pop-up menu
    
      % Make figure visble after adding all components
      f.Visible = 'on'; 
%end

end

   

function EEG = assignChanLocs( EEG, keyStruct, deleteMissingChans )
% input must be the EEGLAB chanlocs struct format

% debug commands
%chanlocs = loadbvef('chanfiles/channel map.bvef');
%  keyStruct = chanlocs;
%  origStruct = newMea(1).EEG.chanlocs;

origStruct = EEG.chanlocs;

if nargin < 3
    deleteMissingChans = false;
end

% generate index of lables from original Struct
origLabels = {origStruct(1,:).labels};

% generate index of lables from key struct
keyLabels = {keyStruct(1,:).labels};

% clean up char from BVEF file (optional)
% removes CH_ from key array

for i = 1:size(keyLabels,2)
    catKeyLabels{1,i} = keyLabels{1,i}(4:5);
end

% converts BVEF file to numbers
cleanKeyLabels = cellfun(@str2num, catKeyLabels);
cleanOrigLabels = cellfun(@str2num, origLabels);

% replace channel labels
keyStruct = rmfield(keyStruct, 'labels');
origStruct = rmfield(origStruct, 'labels');

for i = 1:length(keyStruct)
    keyStruct(i).labels = cleanKeyLabels(i);
end

for i = 1:length(origStruct)
    origStruct(i).labels = cleanOrigLabels(i);
end

% find all key channels in original array
availableChans = ismember(cleanKeyLabels, cleanOrigLabels);
availableChans = find(availableChans);

% check to see that all keys were found
if length(availableChans) ~= length(cleanKeyLabels)
    disp('ERROR: Missing Key Channels in Original File');
end

% replace channel with location information
for i = 1:length(cleanKeyLabels)
    
    replaceIdx = find(cleanOrigLabels == cleanKeyLabels(i));
    newStruct(replaceIdx) = keyStruct(i);
    
end

% extra channels that aren't in the location file
[missingchans missingIdx] = setdiff(cleanOrigLabels, cleanKeyLabels );

for i = 1:length(missingIdx)
    
    newStruct(missingIdx(i)).labels    = origStruct(missingIdx(i)).labels;
    newStruct(missingIdx(i)).type      = origStruct(missingIdx(i)).type;
    newStruct(missingIdx(i)).urchan    = origStruct(missingIdx(i)).urchan;
    
end

EEG = rmfield(EEG, 'chanlocs');
EEG.chanlocs = newStruct;

for i = 1:length(EEG.chanlocs)
    
    replaceStr = num2str(EEG.chanlocs(i).labels);
    EEG.chanlocs(i).labels = replaceStr;
    
end

%missingIdx =[missingIdx; 33];

% remove missing channels using EEGLAB to prevent any corruption
if deleteMissingChans == true
    
    EEG = pop_select( EEG, 'nochannel', missingIdx );
    
    for i = 1:length(missingIdx)
                disp(['Missing Channel Deleted: ' num2str(missingchans(i)) ...
            ' index: ' num2str(missingIdx(i))]);
        
    end
end

EEG = eeg_checkset( EEG );

end

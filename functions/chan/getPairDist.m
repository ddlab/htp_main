function dist = getPairDist(channel1, channel2, EEG)
    
    
% find channel indices

chanidx = zeros(1,2); % always initialize!
chanidx1 = find(strcmpi(channel1,{EEG.chanlocs.labels}));
chanidx2 = find(strcmpi(channel2,{EEG.chanlocs.labels}));

% get interelectrode distance

% get XYZ coordinates in convenient variables
X = [EEG.chanlocs.X];
Y = [EEG.chanlocs.Y];
Z = [EEG.chanlocs.Z];


xdiff = (EEG.chanlocs(chanidx1).X - EEG.chanlocs(chanidx2).X)^2;
ydiff = (EEG.chanlocs(chanidx1).Y - EEG.chanlocs(chanidx2).Y)^2;
zdiff = (EEG.chanlocs(chanidx1).Z - EEG.chanlocs(chanidx2).Z)^2;

dist = sqrt(xdiff + ydiff + zdiff);

end
function delchan = selectBadChans(src,event)

ui_main = gcf;
h = findobj(gcf,'Style','togglebutton');
hlist = findobj(gcf,'Style','listbox');
htext = findobj(gcf,'Tag','badchantitle');

ui_list = h.Value;
ui_selection = hlist.Value;

if ui_list == 1
    ui_main.UserData.delchans = ui_selection;
    htext.String = [htext.String num2str(ui_selection)];
else
    ui_main.UserData.delchans = '';
    htext.String = 'Select Bad Channels: ';
end



%display(boxBadChannels.String);

%ui_obj = get(gcf, 'Children');
end

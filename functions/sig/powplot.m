function handle = powplot( pxx, f, label, savefile ) 
% POWPLOT Colorful power/frequency plot
%
% INPUTS    : 
%   pxx     : power spectrum by channel
%   f       : vector of available freq
% (optional inputs)
%
%
% OUTPUTS   :
%  pxx_sum  : cumulative power per channel
%  pxx_mean : average power per channel
% (optional outputs)
% 
% NOTES
%   (1) Created on 4/11/2018
% written by E. Pedapati


    %Delta (1-4?Hz), Theta (4-10?Hz), Alpha (10-13?Hz), Beta (13-30?Hz) and Gamma (30-100?Hz). 
% Define freq bands
    % delta
    frq(1).label           = 'delta';
    frq(1).band           = [0 4];    
    frq(1).color           = [0.93    0.69    0.13];
    
    % theta
    frq(2).label          = 'theta';
    frq(2).band           = [4 10];    
    frq(2).color          = [1 1 0];
    
    % low alpha
    frq(3).label           = 'low alpha';
    frq(3).band            = [10 10];  
    frq(3).color           = [0.25 0.5 0.15];
    
    % high alpha
    frq(4).label           = 'high alpha';
    frq(4).band            = [10 13];  
    frq(4).color           = [0.25 0.5 0.15];
    
    % beta
    frq(5).label           = 'beta';
    frq(5).band            = [13 30];  
    frq(5).color           = [0 0 1];
    
    % gamma
    frq(6).label           = 'gamma';
    frq(6).band            = [30 80];  
    frq(6).color           = [1.0000   0 0];

       
 % create new figure
 handle = figure();
 clf;
 hold on  
 
 annotation('textbox', [0 0.9 1 0.1], ...
    'String', label, ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center')

 % set x-axis
 xlim([0 80]);
 ax = gca;
 ax.YLabel.String = 'Average Power over All Chans (\muV^2)';
 ax.XLabel.String = 'Frequency (Hz)';
 set(gca, 'YScale', 'log')

 
 for bandid = 1:length(frq)
 
      % define index of relevant hz
     IdxLo = frq(bandid).band(1,1); 
     IdxHi = frq(bandid).band(1,2);
     Idx = find( f >= IdxLo-0.5 & f <= IdxHi+0.5 );
     plot(f(Idx),mean(pxx(:,Idx),1),'-k','LineWidth',3);
     
   for i = 1:length(Idx)-1
     
     % create x points for polygon
     x = [f(Idx(i)) f(Idx(i)) f(Idx(i+1)) f(Idx(i+1))];
     y = [0.001 mean(pxx(:,Idx(i),1)) mean(pxx(:,Idx(i+1),1)) 0.001];
     patch( x,y, frq( bandid ).color, 'LineStyle','none');
     
    end
     
     
 end
 
    %plot(mean(pxx(:,1:80),1))
assignin('base', 'handle', handle); 
saveas(gcf, savefile);
%close gcf;



 
    
    end
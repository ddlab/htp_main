function scale = htp_topo_rel_scale( maxmin )

    A = maxmin;
    B = [0.25 0.5 0.75 1];
    
    [~,I] = min(abs(bsxfun(@minus,A,B'))); 
    scale = B(I);
end




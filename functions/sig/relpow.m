function [pxx_rel] = topo( band, total, nchans )
% RELPOW Calculates Relative Power for freq band
%
% INPUTS    :
%   band    : freq. bands of interest vector [low high]
%   total   : total power
% (optional inputs)
%
%
% OUTPUTS   :
%  pxx_rel  : relative power
%  pxx_tot  : total power (denominator)
% (optional outputs)
%
% NOTES
%
%   (1) Created on 4/7/2018
% written by E. Pedapati

% per channel

% band / total power

pxx_rel = zeros(nchans,1);

for chanid = 1:nchans
    
    pxx_rel( chanid, 1 ) = band( chanid ) / total( chanid );
    
end

end
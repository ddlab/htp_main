function  T = htp_rest_power_plots( T, titlestr, path_level1, chanlocs, savefile, labels, maxscale)
% scale = relative (i.e. base on max) or fixed (standard values)
% allocate space
dat = zeros(size(table2array(T),1),size(table2array(T),2));

% create data array for plotting
for i = 1:size(table2array(T),2)
    
    dat(:,i) = double(table2array(T(:,i)));
    
end
% remove first column of channel names
dat(:,1) = [];

        switch maxscale
            case 'relative'
                titlestr = titlestr.pow_topo_rel;
            case 'fixed'
                titlestr = titlestr.pow_topo_fix;
        end

[~,fn,~]=fileparts(savefile);
        fn=strsplit(fn,'_');
        fn=fn{1};
 
 figure('pos',[100 100 600 200]), clf

 %set(gcf, 'Interpreter', 'none')
annotation('textbox', [0 0.9 1 0.1], ...
    'String', titlestr, ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center')
 annotation('textbox', [0.05 0.15 0.1 0.1], ...
    'String', ' \muV^2', ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center', ...
    'VerticalAlignment', 'bottom')

for i = 1:size(dat,2)
     
        minx = min(abs(dat(:,i)));
        maxx = max(abs(dat(:,i)));
        
        switch maxscale
            case 'relative'
                maxx = htp_topo_rel_scale(maxx);
            case 'fixed'
                maxx = htp_topo_fixed_scale(labels{i});
        end
        L=strsplit(labels{i},' [');
        L=strrep(L{1},'_',' ');
        subplot(1,6,i);
        %dat(:,i) = double( delta_rel );
        
                [h grid_or_val plotrad_or_grid, xmesh, ymesh] = topoplot(dat(:,i), ... 
            chanlocs, 'whitebk','on', 'maplimits', [ 0 maxx ], 'headrad' ,0.25, ... 
            'electrodes','on', 'hcolor','none', 'maplimits','maxmin',  'conv' ,'on' );
%         
%         [h grid_or_val plotrad_or_grid, xmesh, ymesh] = topoplot(dat(:,i), ... 
%             chanlocs, 'whitebk','on', 'maplimits', [ 0 maxx ],'electrodes','on');
        title(L);
        cbar('horiz',0,[0 maxx],2); %*max(abs(dat)));  
        %title( sprintf('%s',labels{i}));
        

end


saveas(gcf, savefile);
%close gcf;


end
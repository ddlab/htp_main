function [pxx_sum pxx_mean] = powband( band, pxx,f, Fs, NFFT) 
% POWBAND Calculates sum and average power within a frequency band
%
% INPUTS    : 
%   band    : freq. bands of interest vector [low high] 
%   pxx     : power spectrum by channel
%   f       : vector of available freq
%   Fs      : sampling rate
%   NFFT    : number of points
% (optional inputs)
%
%
% OUTPUTS   :
%  pxx_sum  : cumulative power per channel
%  pxx_mean : average power per channel
% (optional outputs)
% 
% NOTES
%
%   (1) Created on 4/7/2018
% written by E. Pedapati
    
    % define lower bound of freq band
    lo = band(1,1);
    
    % define upper edge of freq band
    hi = band(1,2);

    % find indexs in freq array
    Idx = find(f > lo & f < hi);
    
    % define number of channels in power array
    nchans = length(pxx(:,1));
    pxx_sum = zeros(nchans,1);
    pxx_mean = zeros(nchans,1);
    
    for chanid = 1:nchans
    
    extralo = pxx(chanid,Idx(1)-1)*((f(Idx(1)) - lo)/(Fs/NFFT));
    extrahi = pxx(chanid,Idx(end))*((hi-f(Idx(end)))/(Fs/NFFT));
    pxx_sum(chanid,1)   = sum(pxx(chanid,Idx))+extralo+extrahi;
    pxx_mean(chanid,1)  = sum(pxx(chanid,Idx)) / (length(pxx_sum(chanid,1)));
    
    end
    
    end
function scale = htp_topo_fixed_scale( band )

if ~isempty(strfind(band,'Delta')) || ~isempty(strfind(band,'delta'))
    %contains(band,'delta','IgnoreCase',true)
    scale = 0.5;
else
    scale = 0.25;
    
end

end
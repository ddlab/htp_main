function o = edf2eeglab( o )
% uses an EEGLAB gui independent mechanism to load EDF file to EEGlab
% format.
% Dependency: edfread.m (matlab file exchange)
% https://www.mathworks.com/matlabcentral/fileexchange/31900-edfread

% read edf file header + data stream

rawFullFile = fullfile(o.pathdb.raw, o.subj_subfolder, o.filename.raw);

[hdr, data] = edfread(rawFullFile);

EEG = o.EEG;

%EEG = pop_biosig( rawFullFile );

% assign edf header variables to eeglab structure
EEG.setname     = o.subj_basename;
EEG.filename    = o.filename.raw;
EEG.group       = o.subj_subfolder;
EEG.subject     = o.subj_basename;

EEG.xmin        = 0;
EEG.comments    = [hdr.patientID '/' hdr.recordID];

EEG.pnts        = hdr.frequency(1);
EEG.srate       = size(data,2) / hdr.records * hdr.duration;
EEG.trials      = hdr.records;
EEG.pnts        = hdr.duration * EEG.srate;

EEG.chanlocs = struct('labels', cellstr(char(hdr.label)));
EEG.nbchan = hdr.ns;

EEG.data = data(:,:);

load('mealabels_new.mat');

chanDiff = length(chanlocs) - EEG.nbchan;

for i = 1:chanDiff
    chanlocs(end) = [];
end

EEG.chanlocs = chanlocs;


clear chanlocs;
EEG.chaninfo.filename = 'mealabels_new.mat';

if o.proc_deleteMissingChans == true
    
    EEG.pnts = EEG.trials * EEG.pnts;
    EEG.trials = 1;
    EEG = eeg_checkset(EEG);
    
else
    
    EEG = eeg_checkset(EEG);
    
end

o.EEG = EEG;

% add chanlocs


end

%eeglab redraw

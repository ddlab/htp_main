function [filelist, out] = getFileList( filter, strpath )
% gets file list of study related data files
% needs config dummy class object
v = version;
if isstruct(filter)
    filter = filter.filter;
    
end
% retrieves study specific files for processing
tmpfolders = {};
tmpfiles = {};
tmpsfolders = {};

% get folder names
d           = dir(strpath);
isub        = [d(:).isdir];
subfolder   = {d(isub).name};
subfolder( ismember( subfolder,{'.','..'}) ) = [];

cellcat = @(x,y) [x y];

% if no subdirectories, make subfolder empty string.
if isempty(subfolder), subfolder = {''}; end

for i = 1:length(subfolder)
    
    % create list of filename (could pre-process here with filter)
    rawFileList = dir([ strpath char(subfolder{i}) filesep() filter]);
    
    [sfolders{1:length(rawFileList)}] = deal(subfolder{i});
    
    if strcmp(v,'9.0.0.370719 (R2016a)')
        tmpfolders = cellcat(tmpfolders, subfolder{i});
    else
        tmpfolders = cellcat(tmpfolders, {rawFileList(:).folder});
    end
    
    tmpfiles =  cellcat(tmpfiles, {rawFileList(:).name});
    tmpsfolders = cellcat(tmpsfolders, sfolders);
    
    sfolders = {};
    rawFileList = {};
    
    
end

tmppath = cell(1,length(tmpsfolders));
[tmppath{:}] = deal(strpath);

try
   
tmppath = cellfun(@(c,d) [c d], tmppath, tmpsfolders,  'uni', false);
tmpfull = cellfun(@(c,d) [c filesep() d], tmppath, tmpfiles, 'uni', false);
tmpsubfolder = cellfun(@(c,d) c, tmpsfolders, 'uni', false);
tmpname = cellfun(@(d) d, tmpfiles, 'uni', false);

%subfoldernames = cellfun(@(x) split(x,'\'), tmpsubfolder, 'uni',false);

filelist.full = tmpfull;
filelist.path = tmpsubfolder;
filelist.name = tmpname;
filelist.subfolder = tmpsfolders;

catch
    disp('** CHECK IF CONFIGURATION FILE IF PATH CORRECT, FILE EXT, OR IF FILES ARE IN DIRECTORY');

    filelist.full = {'No File'};
    filelist.path = {strpath};
    filelist.name = {'No File'};
    filelist.subfolder = {'No subfolders'};
    
end


out = '';

end


function clearWarnings()

warning('off','MATLAB:lang:cannotClearExecutingFunction');
warning('off', 'MATLAB:MKDIR:DirectoryExists');
warning('off', 'MATLAB:MKDIR:EmptyDirectoryName');


return;
end
function result = checkEegPlugin( strplugin, strpath )
% tests to see if EEGlab plugin is present in the plugin directory.

% get folder names
d           = dir(strpath);
isub        = [d(:).isdir];
subfolder   = {d(isub).name};
subfolder( ismember( subfolder,{'.','..'}) ) = [];

subfolder = lower(subfolder);
strplugin = lower(strplugin);
strfind(subfolder, strplugin);

results  = strfind(subfolder, strplugin);
results = cell2mat(results);

if any(results) == 1
    
    result = 1;
    
else
    
    result = 0;
    
end


end


function GetSize(this) 
   props = properties(this); 
   totSize = 0; 
   
   for ii=1:length(props) 
      currentProperty = getfield(this, char(props(ii))); 
      s = whos('currentProperty'); 
      fprintf('%s \t sz: %d\n', props{ii}, s.bytes);
     
      totSize = totSize + s.bytes; 
   end
  
   fprintf(1, '%d bytes\n', totSize); 
end
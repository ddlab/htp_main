function eeglabOn = checkEegLab(  )

figHandles = get(groot, 'Children');
eegLabOn = strfind([figHandles(:).Name],'EEGLAB');

end


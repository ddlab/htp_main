function T =  parica(subj_basename,study, path, fn, workingdir, path_postica, path_icaweights, path_figs,  subfolder, testmode)

% Parallel Processing version of ICA script
% Can also be run as a regular for loop
[ALLEEG, EEG, CURRENTSET, ALLCOM] = eeglab;

% Load single dataset
EEG = pop_loadset(path);

% calculate independent rank
try rank = EEG.etc.rank; catch rank = EEG.nbchan; end

% run binica
if testmode == true
    tic;
        disp('======================================');
        disp(['PROCESSING ICA : ' fn]);
        disp('======================================');
        cd(path_icaweights);

        EEG = pop_runica(EEG,'icatype','binica', 'extended',1,'interupt','on','pca', rank);
        %cd(path_scripts);
    toc;
end

% Store current dataset
[ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
EEG = eeg_checkset( EEG );

% Define output directory
workingdir = char(strcat(path_postica,subfolder));

% Will create subfolder if it does not exist
if 7~=exist(workingdir,'dir')
    status = mkdir(workingdir);
end

% Check set
EEG = eeg_checkset( EEG );

% Define output
%     'ProcState', 'SubID', 'Group', 'Filename', 'Paradigm',  ...
%     'RemoveComponents', 'TLength', 'Tchans','IndRank', 'Trials',  ...
%     'TEpoch', 'NoPnts','sRate', 'ref', 'Comments'}); 

%disp(i)

C = strsplit(EEG.filename,'_');

postICAfname = strcat(C{1},'_postica.set');


% Create Post-ICA Table
    T.ProcState = {'P'}; % Pending artifiact removal
    T.SubID = regexp(subj_basename, '\d++', 'match' );
    T.Filename = {postICAfname};
    T.Paradigm = {study};
    T.Group = {subfolder};
    T.TLength = {EEG.xmax * EEG.trials};
    T.Tchans = {EEG.nbchan};
    T.IndRank = {rank};
    T.RemoveComponents = {''};
    T.Trials = {EEG.trials};
    T.TEpoch = {[EEG.xmin EEG.xmax]};
    T.NoPnts = {EEG.pnts};
    T.sRate = {EEG.srate};
    T.ref = {EEG.ref};
    T.Comments = {''};

T = struct2table(T);

% Save datafile to PostICA directory
EEG = pop_saveset( EEG, 'filename',postICAfname,'filepath', workingdir );

% Clear dataset to prepare for next
STUDY = []; CURRENTSTUDY = 0; ALLEEG = []; EEG=[]; CURRENTSET=[];

end

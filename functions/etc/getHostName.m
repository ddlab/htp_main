function hname = getHostName()

    [~, hname] = system('hostname');
    hname = strtrim(hname);
    
end
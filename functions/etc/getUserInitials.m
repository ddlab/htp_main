function userstr = getUserInitials()
  cprintf('blue', 'Enter initials to continue or Ctrl-C to end');  
  prompt = '\nInitials: ';
  cprintf('_blue', prompt);
  
  marker = '';
  
  userstr = input(marker,'s');
  if isempty(userstr)
      userstr = 'Not Entered';
  end
  
  cprintf('_blue', '\n');



end



% HIGH THROUGHPUT PIPELINE (HTP) 
% VERSION 2.0 (10/2018)
% AUTHOR: E. PEDAPATI, E. RUSSO
% CONTACT: ernest.pedapati@cchmc.org
% TITLE: Stage 5 Subject Analysis (Level1)
% FILE: htp_stage5.m
%
% NOTES: 

% setup environment
clear all; close all;

global current_stage, current_stage = 5; global user;
cprintf('blue','\nStarting Stage 5: Subject-Level Analysis (Level1)');
try htp_config; clearWarnings; catch, disp('Error in Configuration Files'); end

if current_stage == 0, return; end

% selecting objects by CSV
stage_last = 'postcomps'; stage_next = 'level1';
[csvfile, matfile, pathdb] = getStageCSV(stage_last, basePath);
objStageStatus = find(selectObjects(stage_last, csvfile)); % current stage
objStageStatus_completed = find(selectObjects(stage_next, csvfile)); % completed

% Load subject objects
load(matfile, 'sub'); 

arrayfun(@(sub) sub.setCsv( csvfile ), sub, 'UniformOutput',false );
arrayfun(@(sub) sub.setMat( matfile ), sub, 'UniformOutput',false );

for i = 1:length(sub), sub(i).setUser( user ); end
for i = 1:length(sub), sub(i).updatePaths( basePath ); end


cprintf('blue','\nCurrent Data Directory: %s\n\n', basePath');

prev_files = 0; skip_files = 0; errorchk = 0; 

% specify single subject or array or ALL {sub.subj_basename}
stage45.specifySubject = {sub.subj_basename}; % 'D0918';

% vectorization of subject array
for i = 1: length(sub)
    
    s = sub(i);
    
    % special code to run a single subject
    if contains(s.subj_basename, stage45.specifySubject )
        
        if ismember(i, objStageStatus) % processes only files specified by the spreadsheet
            
            % load postcomps dataset and place in EEG subject structure
            s.loadDataset( 'postcomps' );
            % Error accessing 'C:\Data\SATdata\S04_POSTCOMP\batch1\D0003_rest_postcomp.set'
            % due to batch .mat
            
            % dynamic component removal using CSV file
            s.compRemove;
            
            % power analysis
            %s.setFreqTable(); % requires input
            %s.getPntsTable;
           % s.generateStoreRoom;
           % s.bandAverageTrials;
           % s.visualization;
            
            %s.conn;
            %s.pac;
            
            %s.destoryStoreRoom;
            
            
            % End Analysis methods
            
            s.proc_state = 'level1';
            
            %results = magnitude_eeg( s.EEG.data );
            
            
        else  % if object is not at correct stage, push through only as saved
            
            if ismember(i, objStageStatus_completed)
                s.proc_state = 'level1';
                s.outputRow(stage_next);
                prev_files = prev_files + 1;
            else
                s.outputRow('error');
                skip_files = skip_files + 1;
            end
        end
    end
    sub(i) = s;
    i
end
